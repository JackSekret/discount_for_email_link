class ChangeLinkFromDiscount < ActiveRecord::Migration[5.1]
  def change
  	remove_column :discounts, :link
  	add_column :discounts, :links, :string, array: true, default: []
  end
end
