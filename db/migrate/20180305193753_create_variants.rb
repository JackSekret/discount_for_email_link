class CreateVariants < ActiveRecord::Migration[5.1]
  def change
    create_table :variants, id: :bigserial do |t|
	  t.string :title, null: false
	  t.string :image
	  t.belongs_to :product, limit: 8, index: true
      t.timestamps
    end
  end
end
