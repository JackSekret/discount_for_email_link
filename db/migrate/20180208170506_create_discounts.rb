class CreateDiscounts < ActiveRecord::Migration[5.1]
  def change
    create_table :discounts do |t|
       t.boolean :status
    	 t.integer :shop_id, index: true, null: false
    	 t.string :title, null: false
    	 t.string :code, null: false, index: true
    	 t.integer :discount_type, null: false
    	 t.integer :value
    	 t.integer :applies_to, null: false
    	 t.belongs_to :product, limit: 8, index: true
    	 t.belongs_to :collection, limit: 8, index: true
    	 t.string :link
    	 
      t.timestamps
    end
  end
end
