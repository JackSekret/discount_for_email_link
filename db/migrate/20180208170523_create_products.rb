class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products, id: :bigserial do |t|
	    t.string :title, null: false
	    t.string :image
      t.timestamps
    end
  end
end
