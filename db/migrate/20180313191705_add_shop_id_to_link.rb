class AddShopIdToLink < ActiveRecord::Migration[5.1]
  def change
  	add_column :links, :shop_id, :integer
  	add_column :links, :status, :boolean, default: true
  end
end
