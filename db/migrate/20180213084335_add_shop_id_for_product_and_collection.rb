class AddShopIdForProductAndCollection < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :shop_id, :integer
    add_column :collections, :shop_id, :integer
  end
end
