class CreateStatistics < ActiveRecord::Migration[5.1]
  def change
    create_table :statistics do |t|
      t.integer :link_id
      t.integer :shop_id
      t.integer :status, default: 0
      t.string :discount
      t.string :cart_token
      t.decimal :total, default: 0

      t.timestamps
    end
  end
end
