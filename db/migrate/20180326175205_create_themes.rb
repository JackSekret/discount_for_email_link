class CreateThemes < ActiveRecord::Migration[5.1]
  def change
    create_table :themes do |t|
      t.string :title, null: false
      t.json :data, null: false, default: {}
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
