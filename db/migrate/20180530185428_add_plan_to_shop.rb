class AddPlanToShop < ActiveRecord::Migration[5.1]
  def self.up
    add_column :shops, :plan, :integer, default: 0
  end

  def self.down
    remove_column :shops, :plan, :integer, default: 0
  end
end
