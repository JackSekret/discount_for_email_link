class AddInstallationStatusToShops < ActiveRecord::Migration[5.1]
  def change
  	add_column :shops, :installation_status, :integer, default: 0  	
  end
end
