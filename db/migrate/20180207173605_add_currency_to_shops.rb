class AddCurrencyToShops < ActiveRecord::Migration[5.1]
  def change
    add_column :shops, :money_format, :string
    add_column :shops, :currency, :string
  end
end
