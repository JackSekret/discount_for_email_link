class UpdateAppliesTo < ActiveRecord::Migration[5.1]
  def change
  	change_column :discounts, :applies_to, :integer, :null => true
  end
end
