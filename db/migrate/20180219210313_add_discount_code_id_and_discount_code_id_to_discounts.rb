class AddDiscountCodeIdAndDiscountCodeIdToDiscounts < ActiveRecord::Migration[5.1]
  def change
  	add_column :discounts, :price_rule_id, :integer, limit: 8
  	add_column :discounts, :discount_code_id, :integer, limit: 8
  end
end
