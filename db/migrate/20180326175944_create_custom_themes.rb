class CreateCustomThemes < ActiveRecord::Migration[5.1]
  def change
    create_table :custom_themes do |t|
      t.integer :shop_id
      t.string :title, null: false
      t.json :data, null: false, default: {}
      t.integer :status, default: 0
      t.integer :link_id
      t.timestamps
    end
  end
end
