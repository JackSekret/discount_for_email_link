class AddRecurringChargesToShops < ActiveRecord::Migration[5.1]
  def change
  		add_column :shops, :demo, :boolean, default: false
  		add_column :shops, :trial_period_end_at, :datetime
  		add_column :shops, :recurring_status, :integer, default: 0
  end
end
