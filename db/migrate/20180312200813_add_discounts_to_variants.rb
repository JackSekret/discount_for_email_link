class AddDiscountsToVariants < ActiveRecord::Migration[5.1]
  def change
  	add_column :variants, :discounts, :string, array: true, default: []
  end
end
