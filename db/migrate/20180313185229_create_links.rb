class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|      
      t.string :title, null: false
      t.integer :link_type, null: false, default: 0
      t.string :token
      t.string :link_to
      t.json :discount
      t.json :collection
      t.json :product
      t.json :variant
      t.timestamps
    end
    add_index :links, :token
  end
end
