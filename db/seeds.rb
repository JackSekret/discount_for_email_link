# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
admin = AdminUser.find_or_initialize_by(email: 'admin@admin.com')
admin.password = 'G7pp4KMnz2QDe98I'
admin.password_confirmation = 'G7pp4KMnz2QDe98I'
admin.save
theme = Theme.find_or_initialize_by(title: "Default Theme")
theme.data =
{
	:text=>
	{
		:value=>"Congrats!",
		:size=>28,
		:family=>"Dancing Script",
		:order=>0,
		:state=>true,
    :color=>"#cce700",
    :rows=>1,
    :prev=>"Congrats!"
	},
	:secondary=>
	{
		:value=>"Your discount will be automatically applied at checkout.",
		:order=>1,
		:size=>16,
		:family=>"Source Sans Pro",
		:state=>false,
		:color=>"#dd3044",
		:weight=>"bold",
		:style=>"italic",
		:decoration=>"underline"
	},
	:thirdly=>
	{
		:value=>"test",
		:order=>2,
		:size=>16,
		:family=>"Source Sans Pro",
		:state=>false,
		:color=>"#dd3044",
		:weight=>"bold",
		:style=>"italic",
		:decoration=>"underline"
	},
	:code_text=>
	{
		:value=>"Your code is ",
		:size=>28,
		:family=>"Kaushan Script",
		:order=>3,
		:state=>true,
		:color=>"#34a4e4"
	},
	:timer=>
	{
    :countdown=>3600000,
    :timestamp=>Date.tomorrow.to_time.to_i * 1000,
		:value=>60,
		:size=>28,
		:family=>"Josefin Sans",
		:order=>4,
		:state=>false,
		:color=>"#1f9d00",
		:day=>"00",
		:hour=>"01",
		:min=>"00",
    :sec=>"00",
    :type=>"cc",
		:states=>
		{
			:day=>false,
			:hour=>true
		}
	},
	:theme=>
	{
    :color=> '#232323',
    :position=>
    {
      :bottom=> 1,
      :right=> 0,
      :value=> 99,
      :y=> "bottom",
      :prev=>
      {
        :side=> "right",
        :top=> "72.79411764705883px"
      }
    }
  },
  :image=>
  {
    :order=>5,
    :state=>false,
    :position=>"background",
    :size=>"small"
  },
	:close=>
	{
		:color=>"#b9b8b8"
	},
	:border=>
	{
		:color=>"#232323",
		:size=>1,
		:state=>false
  },
  :corner=>0
}
theme.save
