export const ROUTES = {
    base: 'statistics',
    routes: [
        [ /statistics/,           'Statistics',   'statistics' ],
        [ /links$/,               'Links',        'links' ],
        [ /links\/\d+$/,          'Link',         'link' ],
        [ /links\/\d+\/edit/,     'LinkEdit',     'link' ],
        [ /links\/\d+\/theme/,    'Theme',        'theme' ],
        [ /links\/new/,           'LinkEdit',     'link' ],
        [ /themes$/,              'Themes',       'themes' ],
        [ /themes\/\d+$/,         'Theme',        'theme' ],
        [ /themes\/new/,          'Theme',        'theme' ],
        [ /discounts$/,           'Discounts',    'discounts' ],
        [ /discounts\/\d+$/,      'Discount',     'discount' ],
        [ /discounts\/\d+\/edit/, 'DiscountEdit', 'discount' ],
        [ /discounts\/new/,       'DiscountEdit', 'discount' ],
        [ /settings/,             'Settings',     'settings' ]
    ]
};
