/*eslint-disable*/
export const API = {
    host: `${location.origin}/api/v1`,

    statistics:  { path: () => 'statistics',                              methods: ['GET'] },
    shop:        { path: () => 'shop',                                    methods: ['GET'] },
    links:       { path: () => 'links',                                   methods: ['GET'] },
    link: {
        create:  { path: () => 'links',                                   methods: ['PUT'] },
        get:     { path: id => `links/${id}`,                             methods: ['GET'] },
        edit:    { path: id => `links/${id ? id : ''}`,                   methods: ['PATCH'] },
        delete:  { path: id => `links/${id ? id : ''}`,                   methods: ['DELETE'] },
        active:  { path: id => `links/${id ? `${id}/` : ''}activity`,     methods: ['POST'] },
        theme:   { path: id => `links/${id}/theme/change`,                methods: ['POST'] },
    },
    linkTheme: {
        create:  { path: id => `links/${id}/theme/change`, methods: ['POST'] },
        get:     { path: id => `links/${id}/theme/`, methods: ['GET'] },
        edit:    { path: id => `links/${id}/theme`, methods: ['PATCH'] },
    },
    themes:      { path: () => 'themes',                                  methods: ['GET'] },
    theme: {
        create:  { path: () => 'themes',                                  methods: ['PUT'] },
        get:     { path: id => `themes/${id}`,                            methods: ['GET'] },
        edit:    { path: id => `themes/${id ? id : ''}`,                  methods: ['PATCH'] },
        delete:  { path: id => `themes/${id ? id : ''}`,                  methods: ['DELETE'] },
    },
    image:       { path: () => 'image/upload',                                  methods: ['POST'] },
    priceRules:  { path: id => `price_rules?page=${id}`,                  methods: ['GET'] },
    discounts:   { path: () => 'discounts',                               methods: ['GET'] },
    discount: {
        create:  { path: () => 'discounts',                               methods: ['PUT'] },
        get:     { path: id => `discounts/${id}`,                         methods: ['GET'] },
        edit:    { path: id => `discounts/${id ? id : ''}`,               methods: ['PATCH'] },
        delete:  { path: id => `discounts/${id ? id : ''}`,               methods: ['DELETE'] },
        active:  { path: id => `discounts/${id ? `${id}/` : ''}activity`, methods: ['POST'] }
    }
};
