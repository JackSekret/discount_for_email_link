import 'vendor';
import './styles/forms';
import './styles';
import './icons';

import WebFont             from 'webfontloader';
import { Components as C } from 'components';
import { utils }           from 'utils';

class Main {
    constructor() {
        this.init();
    }

    init() {
        this.getFonts();
        this.render();
        this.listenEvents();
    }

    getFonts() {
        WebFont.load({
            google: { families: utils.fonts },
            classes: false
        });
    }

    render() {
        new C.Header();
    }

    listenEvents() {
        document.addEventListener('navigate', () => utils.emitter.unsubscribe());
    }
}

new Main();
