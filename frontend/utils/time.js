export function time(sec, unit, server = false) {
    const units = {
        'minutes': 60,
        'hours': 3600,
        'days': 86400
    };

    return server ? +sec * units[unit] : +sec / units[unit];
}
