export function overlay(state, type) {
    let selector = type ? `[data-overlay="${type}"]` : '[data-overlay="light"]';
    let overlay = document.querySelector(selector);
    overlay.classList.toggle('hidden', !state);
}
