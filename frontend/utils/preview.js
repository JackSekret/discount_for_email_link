import { utils } from 'utils';

export class Preview {
    elems;
    editor = {};
    preview = {};

    constructor(elems, reset) {
        reset ? this.reset(elems) : this.init(elems);
    }

    init(elems) {
        this.elems = elems;
        this.queryElems();
        this.listenEvents();
    }

    updateView(target, key, value) {
        this.preview[key].forEach(elem => {
            if (elem.dataset.id && elem.dataset.id === target.dataset.id) {
                elem.innerHTML = value;
                elem.classList.add('focus');
            } else if (!elem.dataset.id) {
                elem.innerHTML = value;
                elem.classList.add('focus');
            }
        });

        if (this.editor[key].length > 1) {
            this.editor[key].forEach(elem => {
                if (!elem.dataset.id) elem.value = value;
            });
        }
    }

    setEditorFocus(target, key) {
        this.editor[key].forEach(elem => {
            if (elem.dataset.id && elem.dataset.id === target.dataset.id) {
                elem.focus();
            } else if (!elem.dataset.id) {
                elem.focus();
            }
        });
    }

    setPreviewFocus(target, key) {
        this.preview[key].forEach(elem => {
            if (elem.dataset.id && elem.dataset.id === target.dataset.id) {
                elem.classList.add('focus');
            } else if (!elem.dataset.id) {
                elem.classList.add('focus');
            }
        });
    }

    removePreviewFocus(target, key) {
        this.preview[key].forEach(elem => {
            if (elem.dataset.id && elem.dataset.id === target.dataset.id) {
                elem.classList.remove('focus');
            } else if (!elem.dataset.id) {
                elem.classList.remove('focus');
            }
        });
    }

    queryElems() {
        this.elems.forEach(key => {
            this.editor[key] = Array.from(utils.elems(`[data-${key}=edit]`));
            this.preview[key] = Array.from(utils.elems(`[data-${key}=prev]`));
        });
    }

    reset(elems) {
        setTimeout(() => {
            this.elems = elems;
            this.queryElems();

            for (let key in this.preview) {
                this.preview[key].forEach(elem => {
                    if (elem.dataset.id) {
                        let el = this.editor[key].filter(item => elem.dataset.id === item.dataset.id)[0];
                        elem.innerHTML = el && el.value;
                    } else if (!elem.dataset.id) {
                        elem.innerHTML = this.editor[key].map(item => item.value)[0];
                    }
                });
            }
        });
    }

    listenEvents() {
        for (let key in this.editor) {
            this.editor[key].forEach(el => el.addEventListener('input', (e) => this.updateView(e.target, key, el.value)));
        }

        for (let key in this.editor) {
            this.editor[key].forEach(el => el.addEventListener('focus', (e) => this.setPreviewFocus(e.target, key)));
        }

        for (let key in this.editor) {
            this.editor[key].forEach(el => el.addEventListener('focusout', (e) => this.removePreviewFocus(e.target, key)));
        }

        for (let key in this.preview) {
            this.preview[key].forEach(el => el.addEventListener('click', (e) => this.setEditorFocus(e.target, key)));
        }
    }
}
