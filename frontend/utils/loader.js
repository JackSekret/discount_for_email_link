export function loader(state) {
    let loader = document.querySelector('[data-loader]');
    loader.classList.toggle('hidden', !state);
}
