const lowerCase = ['a', 'an', 'the', 'and', 'or', 'nor', 'of', 'for', 'from', 'to'];

export function titleCase(str) {
    return str
        .toLowerCase()
        .split(' ')
        .map((word, i, arr) =>
            i === 0 || i === arr.length - 1 || !lowerCase.includes(word)
                ? word.replace(word[0], word[0].toUpperCase())
                : word)
        .join(' ');
}
