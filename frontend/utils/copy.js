export function copy(input) {
    input.select();
    input.blur();

    if (document.queryCommandSupported('copy')) {
        document.execCommand('copy');
    } else {
        window.getSelection().toString();
    }

    ShopifyApp.flashNotice('Copied to clipboard');
}
