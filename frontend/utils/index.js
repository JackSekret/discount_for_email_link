import { Render }       from './render';
import { Tabs }         from './tabs';
import { Preview }      from './preview';
import { CharCounter }  from './char-counter';
import { AppRouter }    from './router';
import { Http }         from './http';
import { loader }       from './loader';
import { el, elems }    from './elem';
import { time }         from './time';
import { terms }        from './terms';
import { titleCase }    from './titlecase';
import { status }       from './status';
import { prepareData }  from './prepare-data';
import { queryElems }   from './query-elems';
import { clear }        from './clear-container';
import { path }         from './path';
import { EventEmitter } from './emitter';
import { copy }         from './copy';
import { overlay }      from './overlay';
import { scroll }       from './scroll';
import { store }        from './store';
import { fonts }        from './fonts';
import { formatDate }   from './format-date';
import { unique }       from './unique';
import { sanitize }     from './sanitize';

export const { forEach, filter, map } = Array.prototype;

export const utils = {
    queryElems: (sels, els)  => { queryElems(sels, els);    return utils; },
    render: (data)           => { new Render(data);         return utils; },
    preview: (data, reset)   => { new Preview(data, reset); return utils; },
    charCounter: data        => { new CharCounter(data);    return utils; },
    tabs: (data)             => { new Tabs(data);           return utils; },
    router: (data)           =>   new AppRouter(data),
    navigate: (data)         =>   utils.router().navigate(data),
    emitter: new EventEmitter(),
    http: new Http(),
    store,
    prepareData,
    loader,
    titleCase,
    status,
    terms,
    time,
    elems,
    el,
    clear,
    path,
    scroll,
    copy,
    overlay,
    fonts,
    formatDate,
    unique,
    sanitize,
    forEach,
    filter,
    map
};

export default utils;
