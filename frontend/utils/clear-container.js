import { utils } from 'utils';

export function clear(selector) {
    let container = selector ? utils.el(`[data-${selector}]`) : utils.el('[data-main-container]');
    container.innerHTML = '';
}
