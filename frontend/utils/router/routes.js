import { Router }          from './vendor';
import { ROUTES }          from 'config';
import { Components as C } from 'components';

export const routes = _this => {
    _this.baseRoute = ROUTES.base;

    ROUTES.routes.forEach((item) => {
        Router.add(item[0], () => _this.prepareRoute(C[item[1]], item[2]));
    });

    return Router;
};
