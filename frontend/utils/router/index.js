import { Router } from './vendor';
import { utils }  from '../index';
import { routes } from './routes';

let count = 0;

export class AppRouter {
    status;
    route;
    timerId;
    timer = false;
    config = { mode: 'history' };

    constructor(status) {
        this.status = status;
        this.init();
    }

    init() {
        Router.config(this.config);
        routes(this).listen();
        this.relocation();
        this.listenEvents();
    }

    prepareRoute(Component, path) {
        utils.loader(true);
        Router.checkElem(path);
        this.activeLink(path.split('/')[0]);
        new Component(this.status);
        // ShopifyApp.Bar.setTitle(path.replace(path[0], path[0].toUpperCase()));
    }

    navigate(link) {
        // if (JSON.parse(localStorage.getItem('apps-accept')) === null) {
        //     Router.navigate('terms-and-conditions');
        //     return;
        // };

        // this.timer = true;
        // this.timerId = setTimeout(() => {
        // this.timer = false;
        // Router.navigate(link);
        // }, 350);
        Router.navigate(link);
    }

    activeLink(str) {
        const path = str || location.pathname;
        const links = Array.from(document.querySelectorAll('[data-link]'));

        links.forEach((link) => {
            let matched = link.href.match(/^(?:[^:]+:\/\/)?[^\/]+\/([^\/]+)/);
            let match = matched && matched[1];

            // link.classList.toggle('active', new RegExp(path).test(match));
            link.classList.toggle('active', (`/${match}` === path || match === path));
        });
    }

    relocation() {
        let link = (location.pathname === '/') ? this.route : location.pathname;

        Router.navigate('/');
        Router.listen();
        Router.navigate(link);
    }

    replaceLink(event) {
        event.preventDefault();
        let target = event.currentTarget;
        // this.timer && clearTimeout(this.timerId);
        let path = target.href.match(/^(?:[^:]+:\/\/)?[^\/]+\/([^\/]+)(\/.+)?/);
        this.navigate(`/${path[1]}${path[2] ? path[2] : ''}`);
    }

    set baseRoute(route) {
        this.route = route;
    }

    listenEvents() {
        count++;
        if (count > 1) return;

        const links = document.querySelectorAll('[data-link]');

        Array.from(links).forEach(elem => {
            elem.addEventListener('click', (event) => {
                this.replaceLink(event);
            });

            elem.addEventListener('keypress', (e) => {
                if (e.type === 'keypress' && e.keyCode !== 13 && e.keyCode !== 32) return;
                this.replaceLink(e);
            });
        });
    }
}
