export const Router = {
    routes: [],
    mode: null,
    root: '/',
    loading: false,
    config: function(options) {
        this.mode = options && options.mode && options.mode === 'history'
            && !!(history.pushState) ? 'history' : 'hash';
        this.root = options && options.root ? '/' + this.clearSlashes(options.root) + '/' : '/';
        return this;
    },
    getFragment: function() {
        let fragment = '';
        if (this.mode === 'history') {
            fragment = this.clearSlashes(decodeURI(location.pathname + location.search));
            fragment = fragment.replace(/\?(.*)$/, '');
            fragment = this.root !== '/' ? fragment.replace(this.root, '') : fragment;
        } else {
            let match = window.location.href.match(/#(.*)$/);
            fragment = match ? match[1] : '';
        }
        return this.clearSlashes(fragment);
    },
    clearSlashes: function(path) {
        return path.toString().replace(/\/$/, '').replace(/^\//, '');
    },
    add: function(re, handler) {
        if (typeof re === 'function') {
            handler = re;
            re = '';
        }
        this.routes.push({
            re: re,
            handler: handler
        });
        return this;
    },
    remove: function(param) {
        for (let i = 0, r = this.routes[i]; i < this.routes.length; i++) {
            if (r.handler === param || r.re.toString() === param.toString()) {
                this.routes.splice(i, 1);
                return this;
            }
        }
        return this;
    },
    flush: function() {
        this.routes = [];
        this.mode = null;
        this.root = '/';
        return this;
    },
    check: function(f) {
        let fragment = f || this.getFragment();
        for (let i = 0; i < this.routes.length; i++) {
            let match = fragment.match(this.routes[i].re);
            if (match) {
                match.shift();
                this.routes[i].handler.apply({}, match);
                return this;
            }
        }
        return this;
    },
    listen: function() {
        let self = this;
        let current = self.getFragment();
        let fn = function() {
            if (current !== self.getFragment()) {
                current = self.getFragment();
                self.check(current);
                self.deleteElem(current);
                self.checkHidenElement();
            }
        };
        clearInterval(this.interval);
        this.interval = setInterval(fn, 50);
        return this;
    },
    navigate: function(path) {
        path = path ? path : '';
        history.pushState(null, null, this.root + this.clearSlashes(path));
        ShopifyApp.pushState(this.root + this.clearSlashes(path));
        document.dispatchEvent(new CustomEvent('navigate'));
        return this;
    },
    checkElem: function(str) {
        let divs = document.querySelectorAll('[data-page]');
        let i = 0;
        Array.prototype.forEach.call(divs, (item) => {
            if (item.dataset.page !== str) {
                item.setAttribute('class', 'hidden');
            } else {
                i++;
                if (i > 1) item.remove();
            }
        });
    },
    checkHidenElement: () => {
        let divs = document.querySelectorAll('[data-page].hidden');
        Array.prototype.forEach.call(divs, (item) => item.remove());
    },
    deleteElem: (str) => {
        let divs = document.querySelectorAll('[data-page]');
        Array.prototype.forEach.call(divs, (item) => {
            if (item.dataset.page !== str) item.remove();
        });
    }
};
