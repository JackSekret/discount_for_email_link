import { utils } from 'utils';

export class CharCounter {
    elems;
    editor = {};
    preview = {};

    constructor(elems) {
        this.init(elems);
    }

    init(elems) {
        this.elems = elems;
        this.queryElems();
        this.listenEvents();
    }

    updateView(key, target, length, maxLength) {
        this.preview[key].forEach(elem => {
            if (elem.dataset.id && elem.dataset.id === target.dataset.id) {
                elem.innerHTML = length + '/' + maxLength;
            } else if (!elem.dataset.id) {
                elem.innerHTML = length + '/' + maxLength;
            }
        });
    }

    queryElems() {
        this.elems.forEach(key => {
            this.editor[key] = utils.elems(`[data-${key}=edit]`);
            this.preview[key] = utils.elems(`[data-${key}=prev]`);
        });
    }

    listenEvents() {
        for (let key in this.editor) {
            this.editor[key].forEach(el => el.addEventListener('input', (e) => {
                this.updateView(key, e.target, e.target.value.length, e.target.maxLength);
            }));
        }
    }
}
