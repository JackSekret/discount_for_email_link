export class EventEmitter {
    events = [];
    name;

    component(c) {
        this.name = typeof c === 'string' ? c : c.name;
    }

    subscribe(e, el, cb) {
        if (!(el instanceof Array || el instanceof NodeList)) el = [el];

        Array.from(el).forEach((item) => {
            if (!item) return;
            item.addEventListener(e, cb);
        });

        this.events.push({
            e,
            el,
            cb,
            name: this.name
        });
    }

    unsubscribe(c) {
        this.events.forEach(item => {
            Array.from(item.el).forEach((el) => {
                if (!el) return;
                if (c) {
                    let eq = typeof c === 'string' ? item.name === c : item.name === c.name;
                    eq && el.removeEventListener(item.e, item.cb);
                } else {
                    el.removeEventListener(item.e, item.cb);
                }
            });
        });

        this.events = [];
    }
}
