import { Router }          from './router/vendor';
import { Components as C } from 'components';
import { utils }           from 'utils';

export class Render {
    data;
    temp;
    html;
    elem;

    constructor(data) {
        this.init(data);
        this.render();
    }

    init(data) {
        if (typeof data.template === 'string') {
            this.temp = this.getTemplate(data.template);
        } else {
            this.temp = data.template;
        }

        this.data = data;
        this.html = this.temp.render(data.params);
        this.elem = document.createElement(data.element || 'div');
        this.elem.innerHTML = this.html;
    }

    render() {
        utils.loader(false);
        this.applySettings();
        this.data.before ? this.prepand() : this.append();
        this.activeRoute();
    }

    applySettings() {
        this.addId();
        this.addClasslist();
        this.addAttributes();
        this.addClassName();
    }

    addId() {
        if (!this.data.id) return;
        this.elem.id = this.data.id;
    }

    addClassName() {
        if (!this.data.className) return;
        this.elem.className = this.data.className;
    }

    addClasslist() {
        if (!this.data.classList) return;
        for (let item of this.data.classList) {
            this.elem.classList.add(item);
        }
    }

    addAttributes() {
        if (!this.data.attribute) return;
        else if (typeof this.data.attribute === 'string') {
            this.data.attribute = [{ name: this.data.attribute }];
        } else if (!this.data.attribute.length) {
            this.data.attribute = [this.data.attribute];
        }

        this.data.attribute.forEach(attr => {
            this.elem.setAttribute(attr.name, attr.value || '');
        });
    }

    append() {
        let container = utils.el('[data-main-container]');

        if (!this.data.container && container) {
            container.appendChild(this.elem);
            return;
        }

        (typeof this.data.container === 'string'
            ? utils.el(this.data.container)
            : this.data.container || document.body).appendChild(this.elem);
    }

    prepand() {
        (this.data.container || document.body)
            .insertBefore(this.elem, typeof this.data.before === 'string'
                ? utils.el(this.data.before)
                : this.data.before);
    }

    activeRoute() {
        if (!this.data.route) return;
        this.elem.setAttribute('data-page',
            typeof this.data.route === 'string'
                ? this.data.route
                : this.data.template
        );
        Router.checkElem(Router.clearSlashes(location.pathname));
    }

    getTemplate(string) {
        let matched, reg, req, key, id;

        matched = string.match(/(.+)(\/.+)?/);
        reg = new RegExp(`${matched[1]}\/${matched[2] ? matched[2] : 'template'}\.njk`);
        req = require.context('templates', true, /\/[^\/]+\.njk$/);
        key = req.keys().filter(key => reg.test(key))[0];
        id = req.resolve(key);

        return __webpack_require__(id);
    }
}

if (module.hot) {

    module.hot.accept();

    module.hot.accept('templates/statistics/template', () => {
        utils.path(C.Statistics) && utils.clear();
        utils.emitter.unsubscribe(C.Statistics);
        new C.Statistics();
    });
    module.hot.accept('templates/links/template', () => {
        utils.path(C.Links) && utils.clear();
        utils.emitter.unsubscribe(C.Links);
        new C.Links();
    });
    module.hot.accept('templates/themes/template', () => {
        utils.path(C.Themes) && utils.clear();
        utils.emitter.unsubscribe(C.Themes);
        new C.Themes();
    });
    module.hot.accept('templates/themes/theme/template', () => {
        utils.path(C.Theme) && utils.clear();
        utils.emitter.unsubscribe(C.Theme);
        new C.Theme();
    });
    module.hot.accept('templates/links/link/template', () => {
        utils.path(C.Link) && utils.clear();
        utils.emitter.unsubscribe(C.Link);
        new C.Link();
    });
    module.hot.accept('templates/links/link/edit/template', () => {
        utils.path(C.LinkEdit) && utils.clear();
        utils.emitter.unsubscribe(C.LinkEdit);
        new C.LinkEdit();
    });
    module.hot.accept('templates/discounts/template', () => {
        utils.path(C.Discounts) && utils.clear();
        utils.emitter.unsubscribe(C.Discounts);
        new C.Discounts();
    });
    module.hot.accept('templates/discounts/discount/template', () => {
        utils.path(C.Discount) && utils.clear();
        utils.emitter.unsubscribe(C.Discount);
        new C.Discount();
    });
    module.hot.accept('templates/discounts/discount/edit/template', () => {
        utils.path(C.DiscountEdit) && utils.clear();
        utils.emitter.unsubscribe(C.DiscountEdit);
        new C.DiscountEdit();
    });
    module.hot.accept('templates/settings/template', () => {
        utils.path(C.Settings) && utils.clear();
        utils.emitter.unsubscribe(C.Settings);
        new C.Settings();
    });
}
