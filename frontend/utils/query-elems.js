import { utils } from 'utils';

export function queryElems(selectors, elems) {
    selectors.forEach(item => {
        let target = utils.elems(`[data-${item}]`);
        let multi = target.length > 1 || (target[0] && target[0].dataset.id);

        if (multi) {
            elems[item] = Array.from(target);
        } else {
            elems[item] = target[0];
        }
    });
}
