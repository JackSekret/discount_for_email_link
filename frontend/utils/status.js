import { utils } from 'utils';

export function status(account) {
    let cancelled = utils.el('[data-account-status-cancelled]');
    let trial = utils.el('[data-account-status-trial]');

    if (account === 'cancelled') {
        cancelled.classList.remove('hidden');
        utils.loader(false);
        return;

    } else if (account === 'trial_period') {
        trial.classList.remove('hidden');
    }

    return true;
}
