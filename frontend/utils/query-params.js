export function queryParams(search) {
    let data = search.replace('?', '').split('&');
    let params = {};

    data.forEach(item => {
        let val = item.split('=')[1];
        let key = val ? item.split('=')[0] : 'discount';

        val = item.split('=')[0];
        params[key] = decodeURIComponent(val);
    });

    return params;
}
