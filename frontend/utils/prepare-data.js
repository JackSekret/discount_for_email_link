export function prepareData(res) {
    const additional = {
        page: JSON.parse(sessionStorage.getItem('fb-info')),
        images: JSON.parse(sessionStorage.getItem('fb-pictures')),
        user: JSON.parse(localStorage.getItem('fb-user-info')),
        isLogged: JSON.parse(localStorage.getItem('isLogged')),
        shop: ShopifyApp.shopOrigin.match(/^https?:\/\/(.+)/)[1]
    };

    res.image = additional.images && additional.images
        .filter(image => image.name === additional.page.fb_page_name)
        .map(image => image.img)[0];

    res.products = Array.isArray(res.products) ? res.products : [res.products];
    res.shop = additional.shop;
    res.user = additional.user;
    res.isLogged = additional.isLogged || false;
    res.page = additional.page;

    return res;
}
