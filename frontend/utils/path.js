/*eslint-disable*/
const PATH = {
    Statistics:   /statistics/,
    Links:        /links$/,
    Link:         /links\/.+$/,
    LinkEdit:     /links\/.+(\/edit)?/,
    Themes:       /themes$/,
    Theme:        /links\/\d+\/theme$/,
    Discounts:    /discounts$/,
    Discount:     /discounts\/.+$/,
    DiscountEdit: /discounts\/.+(\/edit)?/,
    Settings:     /settings/,
}

export function path(component) {
    let reg = PATH[component.name];

    return reg.test(location.pathname);
}
