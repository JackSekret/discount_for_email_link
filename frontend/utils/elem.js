const doc = document;

export function el(selector) {
    return doc.querySelector(selector);
}

export function elems(selector) {
    return doc.querySelectorAll(selector);
}
