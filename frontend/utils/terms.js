import { Router } from './router/vendor';

export function terms(res)  {
    localStorage.setItem('apps-accept', JSON.stringify(res.terms_accepted_at));

    if (!res.terms_accepted_at) {
        Router.navigate('terms-and-conditions');
    }
}
