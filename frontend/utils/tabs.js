// import { utils } from 'utils';

export class Tabs {
    data = [];

    constructor(data = [{}]) {
        if (!data.length) data = [data];

        data.forEach((item) => {
            this.data.push({
                tab: {
                    elems: Array.from(document.querySelectorAll(item.tab || '[data-tab]')),
                    dataset: this.getDataset(item.tab) || 'tab',
                    key: this.getKey(item.tab) || ''
                },
                content: {
                    elems: Array.from(document.querySelectorAll(item.content || '[data-content]')),
                    dataset: this.getDataset(item.content) || 'content',
                    key: this.getKey(item.content) || ''
                },
                active: item.active || 'active'
            });
        });

        this.listenEvents();
    }

    changeActiveButton(target) {
        let isActive;

        this.data.forEach(item => {
            if (this.isActive(item.active, target)) isActive = true;
        });

        if (isActive) return;

        this.data.forEach(item => {
            if (!this.checkKey(item.tab.key, target)) return;

            item.tab.elems.forEach(el => {
                if (el !== target) {
                    el.classList.add(item.active);
                }
            });
        });

        this.checkButton(target);
    }

    checkButton(target) {
        this.data.forEach(item => {
            if (!this.checkKey(item.tab.key, target)) return;

            item.tab.elems.forEach(el => {
                if (el.classList.toggle(item.active)) {
                    this.showPreview(item.tab.key, el.dataset[item.tab.dataset]);
                }
            });
        });
    }

    showPreview(key, index) {
        this.data.forEach(item => {
            if (key !== item.content.key) return;

            item.content.elems.forEach(el => {
                el.classList.toggle('hidden', el.dataset[item.content.dataset] !== index);
            });
        });
    }

    isActive(className, el) {
        return Array.from(el.classList).includes(className);
    }

    checkKey(key, target) {
        return Object.keys(target.dataset).map(item => item.replace(/tab/, '')).includes(key);
    }

    getKey(str) {
        if (!str) return;
        return this.getDataset(str).replace(/(tab|content)/, '');
    }

    getDataset(str) {
        if (!str) return;
        return str
            .match(/\[data-(.+)\]/)[1]
            .toLowerCase()
            .split('-')
            .map((item, i) => item.replace(item[0], i ? item[0].toUpperCase() : item[0]))
            .join('');
    }

    listenEvents() {
        this.data.forEach(item => {
            item.tab.elems.forEach(el => {
                el.addEventListener('click', (e) => this.changeActiveButton(e.currentTarget));
            });
        });
    }
};
