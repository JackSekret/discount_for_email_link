export function scroll(a) {
    let el = document.querySelector('[data-modal-body]');
    let d = document,
        b = el ? el : d.body,
        e = d.documentElement,
        c = 'client' + a;
    a = 'scroll' + a;

    return /CSS/.test(d.compatMode) ? (e[c] < e[a]) : (b[c] < b[a]);
}
