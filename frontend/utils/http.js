import { API } from 'config';
// import { utils } from './index';

export class Http {
    method;
    alias;
    api = this.prepareApi();

    init(path, methods, data) {
        if (methods && !this.checkMethod(methods)) return;
        return this.xhr(this.method, `${this.api.host}/${path}`, this.prepareData(data));
    }

    xhr(method, url, data = null) {
        const shop = ShopifyApp.shopOrigin.match(/^https?:\/\/(.+)/)[1];
        // const json = typeof data === 'string';

        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open(method, url, true);
            xhr.setRequestHeader('Shop-Domain', shop);
            xhr.setRequestHeader('Content-Type', 'application/json');

            xhr.onload = () => {
                let res = xhr.response && JSON.parse(xhr.response);

                if (xhr.status === 200 || xhr.status === 201 || xhr.status === 204) {
                    resolve(res);
                } else {
                    // let message = res.error_message.status[0] || res.error;
                    // let error = message ? utils.titleCase(message.replace(/_/g, ' ')) : 'App Error';
                    reject(new Error(res.error));
                }
            };

            xhr.onerror = () => reject(new Error('Network Error'));
            xhr.send(data);
        });
    }

    prepareApi() {
        const endpoints = Object.keys(API).map(key => {
            if (key === 'host') return;

            return {
                path: (id, alias) => API[key][alias] ? API[key][alias].path(id) : API[key].path(id),
                methods: API[key].methods,
                key
            };
        });

        const obj = {
            host: API.host,
            endpoints: {}
        };

        endpoints.forEach(item => {
            if (!item) return;
            obj.endpoints[item.key] = (data) => {
                return this.init(item.path(data && data.id, this.alias), item.methods, data && data.data);
            };
        });

        return obj;
    }

    prepareData(data) {
        return JSON.stringify(data);

        // let json = data && Object.keys(data).filter(key => key === 'ids' || key === 'json').length;

        // if (json || data && data.data.json) {
        //     delete data.json;
        //     return JSON.stringify(data);
        // }

        // let model = new FormData();

        // for (let key in data) {
        //     if (Array.isArray(data[key])) {
        //         data[key].forEach(item => {
        //             if (!item) return;
        //             model.append(key, item);
        //         });
        //         continue;
        //     }
        //     model.append(key, data[key]);
        // }

        // return model;
    }

    checkMethod(methods) {
        if (methods.includes(this.method)) return true;

        console.error('406 Not Acceptable');
        return;
    }

    get get() {
        this.method = 'GET';
        this.alias = 'get';
        return this.api.endpoints;
    }

    get post() {
        this.method = 'POST';
        return this.api.endpoints;
    }

    get patch() {
        this.method = 'PATCH';
        this.alias = 'edit';
        return this.api.endpoints;
    }

    get put() {
        this.method = 'PUT';
        this.alias = 'create';
        return this.api.endpoints;
    }

    get delete() {
        this.method = 'DELETE';
        this.alias = 'delete';
        return this.api.endpoints;
    }

    get create() {
        this.method = 'PUT';
        this.alias = 'create';
        return this.api.endpoints;
    }

    get edit() {
        this.method = 'PATCH';
        this.alias = 'edit';
        return this.api.endpoints;
    }

    get active() {
        this.method = 'POST';
        this.alias = 'active';
        return this.api.endpoints;
    }
}
