export class Cookie {
        options = {
            expires: 24 * 60 * 60,
            path: '/',
            encodeValue: false
        }

        get(name) {
            let matches = document.cookie.match(new RegExp(
                '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }

        set(name, value, options) {
            options = options || {};
            let exp = options.expires;

            if (typeof exp === 'number' && exp) {
                let d = new Date();
                d.setTime(d.getTime() + exp * 1000);
                exp = options.expires = d;
            }

            if (exp && exp.toUTCString) options.expires = exp.toUTCString();
            value = encodeURIComponent(value);
            let updatedCookie = name + '=' + value;

            for (let propName in options) {
                updatedCookie += '; ' + propName;
                let propValue = options[propName];
                if (propValue !== true) updatedCookie += '=' + propValue;
            }

            document.cookie = updatedCookie;
        }

        remove(name) {
            this.set(name, null, { expires: -1 });
        }
}
