import { utils } from 'utils';
import * as  m   from './modules';

export class Theme {
    match = location.pathname.match(/links\/(\d+)\/theme/);
    id = this.match && this.match[1];
    path = `links/${this.id}/theme`;

    elems = {};
    sizes = [16, 18, 20, 24, 28, 32, 36];
    borders = [1, 2, 3, 4, 5, 8, 10];
    corners = [0, 3, 5, 10, 20, 30, 40];
    images = {
        position: ['background'],
        size: []
    }

    constructor() {
        this.get().then(res => this.init(res));
    }

    async get() {
        return await utils.http.get.linkTheme({ id: this.id });
    }

    async edit(data) {
        return await utils.http.edit.linkTheme(data);
    }

    async image(data) {
        return await utils.http.post.image(data);
    }

    init(res) {
        this.prepare(res);
        this.render(utils.store.theme);
        this::m.Bar.init();
        this::m.Modal.init();
        this::m.Elems.query();
        this::m.DragNDrop.init();
        this::m.ColorPicker.init();
        this::m.Timer.create();
        this::m.Timer.init();
        this::m.Events.listen(Theme.name);
    }

    prepare(res) {
        console.log(res);
        console.log(res.custom_theme.data.text);
        utils.store.theme = res.custom_theme.data;
        utils.store.theme.code = res.discount.code;
        utils.store.theme.fonts = utils.fonts;
        utils.store.theme.sizes = this.sizes;
        utils.store.theme.borders = this.borders;
        utils.store.theme.corners = this.corners;
        utils.store.theme.images = this.images;
    }

    submit() {
        let prevent = this.elems.timer.filter(item => item.focused).length
            || this.hex::utils.filter(item => item.focused).length;
        if (prevent) return;

        let data = {
            theme: utils.store.theme.theme,
            close: utils.store.theme.close,
            text: utils.store.theme.text,
            secondary: utils.store.theme.secondary,
            thirdly: utils.store.theme.thirdly,
            code_text: utils.store.theme.code_text,
            timer: utils.store.theme.timer,
            border: utils.store.theme.border,
            corner: utils.store.theme.corner,
            code: utils.store.theme.code,
            image: utils.store.theme.image

        };

        data.fonts = utils.unique(Object.keys(data).map(key => data[key].family).filter(item => item));

        data = {
            data: { data },
            id: this.id
        };

        this.edit(data).then(() => this.back());
    }

    render(data) {
        utils.render({
            template: 'theme',
            route: this.path,
            className: 'theme',
            params: { data }
        });
    }

    back() {
        utils.navigate(`links/${this.id}`);
    }
}
