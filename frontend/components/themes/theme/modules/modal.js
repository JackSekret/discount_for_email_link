import { utils } from 'utils';

export class Modal {
    static init() {
        if (localStorage.getItem('sdl-help')) return;
        utils.overlay(true, 'hard');

        let config = {
            src: `${location.origin}/theme-info`,
            title: 'How to change the order',
            width: 'large',
            height: 320,
            buttons: {
                primary: {
                    label: 'Don\'t show this again',
                    callback: () => ShopifyApp.Modal.close(true)
                },
                secondary: {
                    label: 'Show this next time',
                    callback: () => ShopifyApp.Modal.close(false)
                }
            }
        };

        ShopifyApp.Modal.open(config, (res) => {
            utils.overlay(false, 'hard');
            if (!res) return;
            localStorage.setItem('sdl-help', 'true');
        });
    }
}
