import { utils } from 'utils';

export class Popup {
    static appearance(e) {
        let side = e.target.value;

        this.elems.rangeValue.style.left = side === 'left' ? 27 + 'px' : '';
        this.elems.rangeValue.style.right = side === 'right' ? 27 + 'px' : '';

        utils.store.theme.theme.position[side] = 0;
        delete utils.store.theme.theme.position[side === 'left' ? 'right' : 'left'];

        // let radio = e.target.closest('label');
        // let axisY = +this.elems.popupPosition.value;
        // axisY <= 50 && (axisY += 5);

        // let postions = e.target.value.split(',');
        // let vt = postions[0] === '100' ? 'bottom' : 'top';
        // let hz = postions[1] === '100' ? 'right' : 'left';
        // let values = [ 'top', 'bottom', 'left', 'right' ];
        // values.forEach(item => delete utils.store.theme.theme[item]);

        // utils.store.theme.theme.appearance = e.target.value;
        // utils.store.theme.theme[vt] = `${vt === 'bottom' ? 0 : postions[0]}%`;
        // utils.store.theme.theme[hz] = `${hz === 'right' ? 0 : postions[1]}%`;
    }

    static positionChange(e) {
        let position = +e.target.value;
        let radio = this.elems.appearanceRadio.filter(item => item.checked)[0];
        let labels = this.elems.appearanceRadio.map(item => item.closest('label'));
        let side = radio.value;
        let axisY = position <= 50 ? position : 100 - position;
        let positionName = position <= 50 ? 'top' : 'bottom';
        let prev = position / 136 * 100 + 'px';

        this.elems.rangeValue.innerHTML = position;
        labels.forEach(item => item.style.top = prev);
        this.elems.rangeValue.style.top = prev;

        utils.store.theme.theme.position = {
            [positionName]: axisY,
            [side]: 0,
            value: position,
            y: positionName,
            prev: {
                top: prev,
                side
            }
        };
        delete utils.store.theme.theme.position[positionName === 'top' ? 'bottom' : 'top'];
    }

    static borderSize(e) {
        let el = e.target.dataset.el;
        let size = e.target.dataset.size;
        let button = this.elems.toggleDropdown.filter(item => item.dataset.toggledropdown === el)[0];
        let target = this.elems.prev.filter(item => item.dataset.prev === 'theme')[0];
        let dropdown = e.target.closest('[data-dropdown]');
        let li = Array.from(dropdown.querySelectorAll('[data-el]'));

        li.forEach((item) => item.classList.remove('selected'));
        e.target.classList.add('selected');
        dropdown.classList.remove('visible');
        button.innerHTML = `${size}px`;

        target.style.borderWidth = `${size}px`;
        utils.store.theme.border.size = size;
    }

    static corners(e) {
        let el = e.target.dataset.el;
        let size = e.target.dataset.size;
        let button = this.elems.toggleDropdown.filter(item => item.dataset.toggledropdown === el)[0];
        let target = this.elems.prev.filter(item => item.dataset.prev === 'theme')[0];
        let dropdown = e.target.closest('[data-dropdown]');
        let li = Array.from(dropdown.querySelectorAll('[data-el]'));

        li.forEach((item) => item.classList.remove('selected'));
        e.target.classList.add('selected');
        dropdown.classList.remove('visible');
        button.innerHTML = `${size}px`;

        target.style.borderRadius = `${size}px`;
        utils.store.theme.corner = size;
    }
}
