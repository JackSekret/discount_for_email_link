import { utils } from 'utils';
// import * as m from './index';

export class Wysiwyg {
    static check(e) {
        let value = window.getSelection().toString();
        value = value ? value : this.elems.text.value;

        let bold = this.elems.wysiwyg.innerHTML.match(new RegExp(`<b>.*${value}.*</b>`));
        let underline = this.elems.wysiwyg.innerHTML.match(new RegExp(`<u>.*${value}.*</u>`));
        let italic = this.elems.wysiwyg.innerHTML.match(new RegExp(`<i>.*${value}.*</i>`));

        e.target.closest('[data-name]').querySelector('[data-style="underline"]').classList.toggle('active', !!underline);
        e.target.closest('[data-name]').querySelector('[data-style="italic"]').classList.toggle('active', !!italic);
        e.target.closest('[data-name]').querySelector('[data-style="bold"]').classList.toggle('active', !!bold);
    }
    static analyse(type) {
        let value = window.getSelection().toString();
        value = value ? value : this.elems.text.value;
        let tags = {
            fontWeight: 'b',
            textDecoration: 'u',
            fontStyle: 'i'
        };


        let replacer = value => `<${tags[type]}>${value}</${tags[type]}>`;
        let values = value.split(' ');
        let save = this.elems.wysiwyg.innerHTML;
        let saveArr = save.split(' ');

        let tagsArr = saveArr.filter(item => /^</.test(item));
        console.log(tagsArr);
        let newArr = values.map(item => {
            tagsArr.forEach(el => {
                if (item === '') return item;
                if (new RegExp(`${item}`).test(el) && !/^<.+>$/.test(item)) item = el;
            });

            return item;
        });

        console.log(newArr);

        type && values.forEach(item => {
            // console.log(item);
            let m = save.match(new RegExp(`(<${tags[type]}>)([<>bui\/]*${item}[<>bui\/]*)(</${tags[type]}>)`));
            save = m ? save.replace(new RegExp(`(${m[1]})(${m[2]})(${m[3]})`), '$2') : save.replace(item, replacer);
        });



        this.elems.wysiwyg.innerHTML = utils.sanitize(type ? save : newArr.join(' '));

        return utils.sanitize(type ? save : newArr.join(' '));

    };
}
