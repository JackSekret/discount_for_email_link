import { utils } from 'utils';
// import * as m from './index';

export class FontChange {
    static active() {
        // this::m.Wysiwyg.check(e);
    }
    static style(e) {
        // this.elems.textPrev.innerHTML = area.value;
        // utils.store.theme.text.value = utils.sanitize(area.value);

        let el = e.target.dataset.el;
        let type = e.target.dataset.type;
        let style = e.target.dataset.style;
        let isActive = /active/.test(e.target.className);
        let save = e.target.dataset.save;

        e.target.classList.toggle('active', !isActive);
        this.elems[`${el}Prev`].style[type] = isActive ? `${save === 'decoration' ? 'none' : 'normal'}` : style;
        utils.store.theme[el][save] = isActive ? '' : style;
        // utils.store.theme[el][save] = '';
        // let prev = this::m.Wysiwyg.analyse(type);

        // this.elems[`${el}Prev`].innerHTML = m.Text.breakLine(prev);
        // utils.store.theme.text.prev = m.Text.breakLine(prev);
    }

    static size(e) {
        let el = e.target.dataset.el;
        let size = e.target.dataset.fontsize;
        let button = this.elems.toggleDropdown.filter(
            item => item.dataset.toggledropdown === `${el}Size`
        )[0];
        let dropdown = e.target.closest('[data-dropdown]');
        let li = Array.from(dropdown.querySelectorAll('[data-el]'));

        li.forEach((item) => item.classList.remove('selected'));
        e.target.classList.add('selected');
        dropdown.classList.remove('visible');
        button.innerHTML = `${size}px`;

        this.elems[`${el}Prev`].style.fontSize = `${size}px`;
        utils.store.theme[el].size = size;
    }

    static family(e) {
        let el = e.target.dataset.el;
        let family = e.target.dataset.fontfamily;

        let button = this.elems.toggleDropdown.filter(
            item => item.dataset.toggledropdown === `${el}Family`
        )[0];
        let dropdown = e.target.closest('[data-dropdown]');
        let li = Array.from(dropdown.querySelectorAll('[data-el]'));

        li.forEach((item) => item.classList.remove('selected'));
        e.target.classList.add('selected');
        dropdown.classList.remove('visible');
        button.innerHTML = family;
        button.style.fontFamily = family;

        this.elems[`${el}Prev`].style.fontFamily = family;
        utils.store.theme[el].family = family;
    }
}
