import { utils } from 'utils';
import datepicker from 'js-datepicker/datepicker';

export class Timer {
    static init() {
        utils.store.theme.timer.type === 'to' && (this.interval = setInterval(() => this::Timer.calcDate(), 1000));
        this.elems.timer.forEach(item => (item.onfocus = () => (item.focused = true)));
        this.elems.timer.forEach(item => (item.onblur = () => (item.focused = false)));
    }

    static create() {
        let options = {
            position: 'bl',
            customDays: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            minDate: new Date(new Date().setDate(new Date().getDate() + 1)),
            onSelect: instance => {
                utils.store.theme.timer.timestamp = +instance.dateSelected;
                this::Timer.toggle('to');
            },

            formatter(el, date) {
                el.value = utils.formatDate(date);
            }
        };
        datepicker(this.elems.datepicker, options);
    }

    static type(e) {
        let type = e.target.value;
        utils.store.theme.timer.type = type;

        if (type === 'cc') {
            clearInterval(this.interval);
            this::Timer.calc();
            let date = Timer.calcCountdown();

            utils.store.theme.timer.day = date.day;
            utils.store.theme.timer.hour = date.hour;
            utils.store.theme.timer.min = date.min;
            utils.store.theme.timer.sec = date.sec;

            this.elems.timer.filter(item => item.dataset.timer === 'day')[0].value = date.day;
            this.elems.timer.filter(item => item.dataset.timer === 'hour')[0].value = date.hour;
            this.elems.timer.filter(item => item.dataset.timer === 'min')[0].value = date.min;

            this.elems.timepicker.value = date.day + ':' + date.hour + ':' + date.min + ':' + date.sec;
            console.log(utils.store.theme.timer);
        } else {
            if (!utils.store.theme.timer.timestamp) utils.store.theme.timer.timestamp = +new Date(new Date().setDate(new Date().getDate() + 1));
            this.interval = setInterval(() => this::Timer.calcDate(), 1000);
        }

        // this.elems.timepicker.classList.toggle('hidden', type === 'to');
        // this.elems.datepicker.classList.toggle('hidden', type === 'cc');
        this.elems['picker-wrap'].forEach(item => item.classList.toggle('hidden', item.dataset.pickerWrap !== type));
        this::Timer.toggle(type);
    }

    static toggle(type) {
        let date = type === 'to' ? this::Timer.calcDate(true) : this::Timer.calcCountdown();
        this::Timer.save(date);
        this::Timer.togglePrev();
        this::Timer.updatePrev(date);
    }

    static save(date) {
        let keys = ['day', 'hour', 'min', 'sec'];
        keys.forEach(key => {
            utils.store.theme.timer[key] = date[key];
        });
    }

    static togglePrev() {
        let prev = {
            day: this.elems['timer-prev'].filter(item => item.dataset.timerPrev === 'day')[0].parentElement,
            hour: this.elems['timer-prev'].filter(item => item.dataset.timerPrev === 'hour')[0].parentElement
        };
        let day = +utils.store.theme.timer.day;
        let hour = +utils.store.theme.timer.hour;

        prev.day.classList.toggle('hidden', +day === 0);
        prev.hour.classList.toggle('hidden', +hour === 0 && +day === 0);
    }

    static change(e) {
        let type = e.target.dataset.timer;
        let value = +e.target.value;
        let prev = this.elems['timer-prev'].filter(item => item.dataset.timerPrev === type)[0];
        let input = this.elems.timepicker;

        utils.store.theme.timer[type] = value;
        this::Timer.calc();
        this::Timer.togglePrev(type);
        let date = this::Timer.calcCountdown();

        prev.innerHTML = date[type];
        e.target.value = date[type];
        input.value = date.day + ':' + date.hour + ':' + date.min + ':' + date.sec;

        this::Timer.calc();
    }

    static calc() {
        utils.store.theme.timer.countdown =
            ((utils.store.theme.timer.day * 24 * 60 * 60) +
            (utils.store.theme.timer.hour * 60 * 60) +
            (utils.store.theme.timer.min * 60)) * 1000;
    }

    static calcDate(res) {
        let timestamp = utils.store.theme.timer.timestamp;
        let diff = (timestamp - +new Date()) / 1000;

        let date = {};
        date.day = Math.floor(diff / 60 / 60 / 24);
        diff -= date.day * 60 *60 * 24;

        date.hour = Math.floor(diff / 60 / 60);
        diff -= date.hour * 60 *60;

        date.min = Math.floor(diff / 60);
        diff -= date.min * 60;

        date.sec = Math.floor(diff);

        utils.store.theme.timer.states = {
            day: date.day !== 0,
            hour: date.hour !== 0
        };

        date.day = date.day < 10 ? '0' + date.day : date.day;
        date.hour = date.hour < 10 ? '0' + date.hour : date.hour;
        date.min = date.min < 10 ? '0' + date.min : date.min;
        date.sec = date.sec < 10 ? '0' + date.sec : date.sec;

        utils.store.theme.timer.day = date.day;
        utils.store.theme.timer.hour = date.hour;
        utils.store.theme.timer.min = date.min;
        utils.store.theme.timer.sec = date.sec;

        if (res) return date;
        this::Timer.updatePrev(date);
    }

    static calcCountdown() {
        var sec = utils.store.theme.timer.countdown / 1000;

        var date = {};
        date.day = Math.floor(sec / 60 / 60 / 24);
        sec -= date.day * 60 *60 * 24;

        date.hour = Math.floor(sec / 60 / 60);
        sec -= date.hour * 60 *60;

        date.min = Math.floor(sec / 60);
        sec -= date.min * 60;

        date.sec = Math.floor(sec);

        utils.store.theme.timer.states = {
            day: date.day !== 0,
            hour: date.hour !== 0
        };

        date.day = date.day < 10 ? '0' + date.day : date.day;
        date.hour = date.hour < 10 ? '0' + date.hour : date.hour;
        date.min = date.min < 10 ? '0' + date.min : date.min;
        date.sec = date.sec < 10 ? '0' + date.sec : date.sec;

        utils.store.theme.timer.day = date.day;
        utils.store.theme.timer.hour = date.hour;
        utils.store.theme.timer.min = date.min;
        utils.store.theme.timer.sec = date.sec;

        return date;
    }

    static updatePrev(date) {
        this.elems['timer-prev'].forEach(item => {
            let num = date[item.dataset.timerPrev];
            item.innerHTML = num;
        });
    }

    static showTimepicker() {
        this.elems['timepicker-popup'].classList.remove('hidden');
    }

    static hideTimepicker(e) {
        let popup = e.target === this.elems['timepicker-popup'] || this.elems['timepicker-popup'] === e.target.closest('[data-timepicker-popup]');

        if (e.target.dataset.timepicker !== undefined) return;
        if (popup) return;
        this.elems['timepicker-popup'].classList.add('hidden');
    }
}
