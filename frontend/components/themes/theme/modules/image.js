import { utils } from 'utils';

export class Image {
    static onUpload(e) {
        console.log(e);
        let files = e.target.files;

        let img = {};
        let reader = new FileReader();

        reader.readAsDataURL(files[0]);
        reader.onloadend = () => {
            img.src = reader.result;
            img.name = files[0].name;
            img.size = files[0].size;
            img.success = false;
            img.progress = 0;

            console.log(this.elems.imagePrev);
            console.log(img);

            this.elems.imagePrev.src = img.src;

            utils.store.theme.image.src = img.src;
            utils.store.theme.image.title = img.name;
            utils.store.theme.image.rate = img.size;
        };
    }

    static send() {
        this.image({ data: { image: utils.store.theme.image.src } });
    }

    static positionChange(e) {
        console.log(e);
        let el = e.target.dataset.el;
        let position = e.target.dataset.imageposition;
        let button = this.elems.toggleDropdown.filter(item => item.dataset.toggledropdown === el)[0];
        // let target = this.elems.prev.filter(item => item.dataset.prev === 'image')[0];
        let prev = this.elems.prev.filter(item => item.dataset.prev === 'theme')[0];
        let dropdown = e.target.closest('[data-dropdown]');
        let li = Array.from(dropdown.querySelectorAll('[data-el]'));

        li.forEach((item) => item.classList.remove('selected'));
        e.target.classList.add('selected');
        dropdown.classList.remove('visible');
        button.innerHTML = position;

        utils.store.theme.image.position = position;

        if (position === 'background') {
            prev.style.backgroundImage = `url(${utils.store.theme.image.src})`;
            prev.style.backgroundSize = 'cover';
            prev.style.backgroundRepeat = 'no-repeat';
        }
    }
}
