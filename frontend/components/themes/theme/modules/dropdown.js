import { utils } from 'utils';

export class Dropdown {
    static toggle(e) {
        let type = e.target.dataset.toggledropdown;
        let el = this.elems.dropdown.filter(
            item => item.dataset.dropdown === type
        )[0];

        let isActive = /visible/.test(el.className);
        el.classList.toggle('visible', !isActive);

        utils.emitter.subscribe('click', document, e => {
            if (e.target.dataset.toggledropdown === type) return;
            if (type === 'appearance' && e.target.closest('[data-dropdown="appearance"]')) return;
            el.classList.remove('visible', isActive);
        });
    }
}
