import { utils } from 'utils';

export class BlockState {
    static init(e) {
        let type = e.target.dataset.check;
        let state = e.target.checked;
        let prev = this.elems.prev.filter(item => item.dataset.prev === type)[0];
        let label = e.target.closest('label');
        let overlay = this.elems.overlay.filter(item => item.dataset.overlay === type)[0];
        // let sep;

        if (type === 'image' && !state) {
            utils.store.theme.image.position = '';
        }
        if (type === 'theme') {
            prev.style.border = !state ? 'none' : `${utils.store.theme.border.size}px solid ${utils.store.theme.border.color}`;
            utils.store.theme.border.state = state;
        }
        // else if (type === 'hour' || type === 'day') {
        //     prev = this.elems['timer-prev'].filter(item => item.dataset.timerPrev === type)[0].parentElement;
        //     // sep = this.elems['timer-sep'].filter(item => item.dataset.timerSep === type)[0];
        //     prev.classList.toggle('hidden', !state);
        //     // sep.classList.toggle('hidden', !state);
        //     if (!utils.store.theme.timer.states) utils.store.theme.timer.states = {};
        //     utils.store.theme.timer.states[type] = state;

        //     if (type === 'day' && !state) {
        //         prev = this.elems['timer-prev'].filter(item => item.dataset.timerPrev === 'hour')[0].parentElement;
        //         // sep = this.elems['timer-sep'].filter(item => item.dataset.timerSep === 'hour')[0];
        //         // this.elems.check.filter(item => item.dataset.check === 'hour')[0].checked = false;
        //         prev.classList.toggle('hidden', state);
        //         // sep.classList.toggle('hidden', state);
        //         utils.store.theme.timer.states.hour = false;
        //     } else if (type === 'hour' && !state && utils.store.theme.timer.states.day) {
        //         prev = this.elems['timer-prev'].filter(item => item.dataset.timerPrev === 'day')[0].parentElement;
        //         // sep = this.elems['timer-sep'].filter(item => item.dataset.timerSep === 'day')[0];
        //         this.elems.check.filter(item => item.dataset.check === 'day')[0].checked = false;
        //         prev.classList.add('hidden');
        //         // sep.classList.add('hidden');
        //         utils.store.theme.timer.states.day = false;
        //     } else if (type === 'day' && !utils.store.theme.timer.states.hour) {
        //         prev = this.elems['timer-prev'].filter(item => item.dataset.timerPrev === 'hour')[0].parentElement;
        //         // sep = this.elems['timer-sep'].filter(item => item.dataset.timerSep === 'hour')[0];
        //         this.elems.check.filter(item => item.dataset.check === 'hour')[0].checked = true;
        //         prev.classList.remove('hidden');
        //         // sep.classList.remove('hidden');
        //         utils.store.theme.timer.states.hour = true;
        //     }
        //     Timer.calc();
        // }
        else {
            prev.classList.toggle('hidden', !state);
            overlay.classList.toggle('hidden', state);
            label.title = state ? 'Hide block' : 'Show block';
            utils.store.theme[type].state = state;
        }
    }
}
