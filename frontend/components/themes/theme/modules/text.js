import { utils } from 'utils';
// import * as m from './index';

export class Text {
    static primary(e) {
        // let prev = this::m.Wysiwyg.analyse();
        // this.elems.wysiwyg.innerHTML = prev;
        this.elems.textPrev.innerHTML = Text.breakLine(utils.sanitize(e.target.value));
        utils.store.theme.text.prev = Text.breakLine(utils.sanitize(e.target.value));
        utils.store.theme.text.rows = Text.rows(e.target.value).amount;
        utils.store.theme.text.value = utils.sanitize(e.target.value);
    }

    static secondary(e) {
        this.elems.secondaryPrev.innerHTML = utils.sanitize(e.target.value);
        utils.store.theme.secondary.value = utils.sanitize(e.target.value);
    }

    static thirdly(e) {
        this.elems.thirdlyPrev.innerHTML = utils.sanitize(e.target.value);
        utils.store.theme.thirdly.value = utils.sanitize(e.target.value);
    }

    static code(e) {
        this.elems['code_text-value'].innerHTML = utils.sanitize(e.target.value);
        utils.store.theme.code_text.value = utils.sanitize(e.target.value);
    }

    static area(e) {
        let popup = this.elems.prev.filter(item => item.dataset.prev === 'theme')[0];
        let area = this.elems.text;
        let popupHeight = popup.offsetHeight;
        let areaHeight = area.offsetHeight;
        let rows;

        if (e.keyCode === 13) {

            if (areaHeight === 175 || popupHeight === 482) return;

            rows = Text.rows(area.value).amount;
            utils.store.theme.text.rows = rows;
            utils.store.theme.text.prev = Text.breakLine(area.value);

            area.style.height = `${40 + rows * 15}px`;
            this.elems.textPrev.innerHTML = Text.breakLine(area.value);

        } else if (e.keyCode === 8) {
            rows = Text.rows(area.value);
            utils.store.theme.text.rows = rows.amount;
            let line = Text.line(area);

            let cof;
            if (rows === 1) cof = rows.amount;
            else if (line === rows.amount) cof = rows.amount;
            else cof = rows.amount + 1;

            area.style.height = `${25 + cof * 15}px`;
        } else if (e.keyCode === 46) {
            rows = Text.rows(area.value);
            utils.store.theme.text.rows = rows.amount;

            area.style.height = `${25 + rows.amount * 15}px`;
        }
    }

    static breakLine(str) {
        let rows = Text.rows(str);
        return rows.new.map(item => item + '<br>').concat(rows.same).join('');
    }

    static rows(str) {
        return {
            new: str.match(/(.+)\n/g) || [],
            same: str.match(/(.+)$/g) || [],
            get amount() {
                return this.new.concat(this.same).length;
            },
            get amounts() {
                return this.new.map(item => item.replace(/\n/, '')).concat(this.same).map(item => item.length);
            }
        };
    }

    static line(area) {
        let rows = Text.rows(area.value);
        let res = area.textLength;
        let last = area.selectionEnd;
        let line;

        rows.amounts.reverse().forEach((item, i, arr) => {
            if (res === last) line = arr.length - i;
            res -= item + 1;
        });

        return line ? (rows.same.length ? line : line + 1) : 1;
    }
}
