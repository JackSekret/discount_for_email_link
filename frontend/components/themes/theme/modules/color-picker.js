import { utils } from 'utils';
import 'a-color-picker';

export class ColorPicker {
    static init() {
        this::ColorPicker.create(
            'text',
            '[data-colorPicker="text"]',
            this.elems.textPrev
        );
        this::ColorPicker.create(
            'secondary',
            '[data-colorPicker="secondary"]',
            this.elems.secondaryPrev
        );
        this::ColorPicker.create(
            'thirdly',
            '[data-colorPicker="thirdly"]',
            this.elems.thirdlyPrev
        );
        this::ColorPicker.create(
            'code_text',
            '[data-colorPicker="code_text"]',
            this.elems.code_textPrev
        );
        this::ColorPicker.create(
            'timer',
            '[data-colorPicker="timer"]',
            this.elems.timerPrev
        );
        // this::ColorPicker.create(
        //     'image',
        //     '[data-colorPicker="image"]',
        //     this.elems.imagePrev
        // );
        this::ColorPicker.create(
            'theme',
            '[data-colorPicker="theme"]',
            this.elems.prev.filter(item => item.dataset.prev === 'theme')[0],
            'backgroundColor'
        );
        this::ColorPicker.create(
            'close',
            '[data-colorPicker="close"]',
            this.elems.prev.filter(item => item.dataset.prev === 'close')[0],
            'fill'
        );
        this::ColorPicker.create(
            'border',
            '[data-colorPicker="border"]',
            this.elems.prev.filter(item => item.dataset.prev === 'theme')[0],
            'borderColor'
        );
    }

    static create(type, attachTo, prev, bg) {
        console.log(this.elems);
        let target = this.elems.color.filter(
            item => item.dataset.color === type
        )[0];

        (AColorPicker.createPicker(
            {
                showRGB: false,
                showHSL: false,
                showAlpha: true,
                color: utils.store.theme[type].color,
                attachTo
            }
        ).onchange = picker => {
            target.value = picker.color;
            if (bg) prev.style[bg]= picker.color;
            else prev.style.color = picker.color;
            if (!utils.store.theme[type]) utils.store.theme[type] = {};
            utils.store.theme[type].color = picker.color;
        });

        utils.emitter.subscribe('click', document, e => {
            if (
                new RegExp(`^${type}`, 'i').test(e.target.dataset.colorPicker) ||
                e.target.closest(attachTo)
            ) return;

            let el = document.querySelector(attachTo);
            el && el.classList.add('hidden');
        });

        this.hex = document.querySelectorAll('input[name="RGBHEX"]');
        this.hex::utils.forEach(item => (item.onfocus = () => (item.focused = true)));
        this.hex::utils.forEach(item => (item.onblur = () => (item.focused = false)));
    }

    static show(e) {
        let type = e.currentTarget.dataset.color;
        let picker = this.elems.colorPicker.filter(
            item => item.dataset.colorpicker === type.toLowerCase()
        )[0];
        picker.classList.toggle('hidden');
    }
}
