import { utils } from 'utils';
import * as  m   from './index';

export class Events {
    static listen(name) {
        utils.emitter.component(name);
        utils.emitter.subscribe('submit', this.elems.form, e => { e.preventDefault(); this.submit(e); });
        utils.emitter.subscribe('click', this.elems.goBack, () => this.back());
        utils.emitter.subscribe('click', this.elems.toggleDropdown, e => this::m.Dropdown.toggle(e));
        utils.emitter.subscribe('change', this.elems.appearanceRadio, e => this::m.Popup.appearance(e));
        utils.emitter.subscribe('click', this.elems.border, e => this::m.Popup.borderSize(e));
        utils.emitter.subscribe('click', this.elems.corner, e => this::m.Popup.corners(e));
        utils.emitter.subscribe('click', this.elems.color, e => { e.preventDefault(); this::m.ColorPicker.show(e); });
        utils.emitter.subscribe('input', this.elems.text, e => this::m.Text.primary(e));
        utils.emitter.subscribe('keyup', this.elems.text, e => this::m.Text.area(e));
        utils.emitter.subscribe('input', this.elems.secondary, e => this::m.Text.secondary(e));
        utils.emitter.subscribe('input', this.elems.thirdly, e => this::m.Text.thirdly(e));
        utils.emitter.subscribe('input', this.elems.codeText, e => this::m.Text.code(e));
        utils.emitter.subscribe('input', this.elems.timer, e => this::m.Timer.change(e));
        utils.emitter.subscribe('change', this.elems.timerType, e => this::m.Timer.type(e));
        utils.emitter.subscribe('focus', this.elems.timepicker, e => this::m.Timer.showTimepicker(e));
        utils.emitter.subscribe('click', document, e => this::m.Timer.hideTimepicker(e));
        // utils.emitter.subscribe('focusout', this.elems.timepicker, e => this::m.Timer.hideTimepicker(e));
        utils.emitter.subscribe('click', this.elems.fontStyle, e => this::m.FontChange.style(e));
        utils.emitter.subscribe('click', this.elems.fontSize, e => this::m.FontChange.size(e));
        utils.emitter.subscribe('click', this.elems.fontFamily, e => this::m.FontChange.family(e));
        // utils.emitter.subscribe('select', this.elems.text, e => this::m.FontChange.active(e));
        utils.emitter.subscribe('change', this.elems.check, e => this::m.BlockState.init(e));
        utils.emitter.subscribe('input', this.elems.popupPosition, e => this::m.Popup.positionChange(e));
        utils.emitter.subscribe('change', this.elems.image, e => this::m.Image.onUpload(e));
        utils.emitter.subscribe('click', this.elems.imagePosition, e => this::m.Image.positionChange(e));
        utils.emitter.subscribe('click', this.elems.send, e => this::m.Image.send(e));
    }
}
