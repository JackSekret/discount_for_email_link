import { utils } from 'utils';
import { Swappable } from '@shopify/draggable';

export class DragNDrop {
    static init() {
        const isPrevented = (element, classesToPrevent) => {
            let currentElem = element;
            let isParent = false;

            while (currentElem) {
                const hasClass = Array.from(currentElem.classList).some(cls =>
                    classesToPrevent.includes(cls)
                );
                if (hasClass) {
                    isParent = true;
                    currentElem = undefined;
                } else {
                    currentElem = currentElem.parentElement;
                }
            }

            return isParent;
        };
        const containers = document.querySelectorAll('[data-form]');
        const swappable = new Swappable(containers, {
            draggable: '.draggable',
            delay: 200,
            mirror: { constrainDimensions: true }
        });

        swappable.on('drag:start', e => {
            const currentTarget = e.originalEvent.target;
            this.elems.dropdown.forEach(item => item.classList.remove('visible'));
            this.elems.colorPicker.forEach(item =>item.classList.add('hidden'));
            if (
                isPrevented(currentTarget, [
                    'button-defalt',
                    'select',
                    'control',
                    'form__input',
                    'color-picker'
                ])
            ) {
                if (currentTarget.tagName === 'SELECT') currentTarget.click();
                e.cancel();
            }
        });

        swappable.on('drag:stop', () => {
            setTimeout(() => {
                let elems = Array.from(
                    document.querySelectorAll('[data-draggable]')
                );
                let prevElems = Array.from(
                    document.querySelectorAll('[data-drag-prev]')
                );

                elems.forEach((item, i) => (item.dataset.position = i));
                this.elems.timerType.forEach(item => item.value === utils.store.theme.timer.type && (item.checked = true));

                let datepicker =  document.querySelector('.qs-datepicker');
                let parent = datepicker.closest('[data-position]');
                let position = parent.dataset.position;
                datepicker.className = datepicker.className.replace(/mode-\d/, '');
                datepicker.classList.add(`mode-${position}`);

                this.elems.dropdown.forEach(item => {
                    let dropType = item.dataset.dropdown;
                    if (/appearance|border|corner/.test(dropType)) return;
                    let type = item.dataset.type;
                    let parent = elems.filter(item => item.dataset.name === type)[0];
                    let position = parent.dataset.position;

                    item.className = item.className.replace(/mode-\d/, '');
                    item.classList.add(`mode-${position}`);
                });

                this.elems.colorPicker.forEach(item => {
                    let type = item.dataset.colorpicker;
                    if (type === 'theme' || type === 'close' || type === 'border') return;
                    let parent = elems.filter(item => item.dataset.name === type)[0];

                    let position = parent.dataset.position;
                    item.className = item.className.replace(/mode-\d/, '');
                    item.classList.add(`mode-${position}`);
                });

                prevElems.forEach((item) => {
                    let type = item.dataset.dragPrev;
                    let order = (item.style.order = elems.filter(
                        item => item.dataset.name === type
                    )[0].dataset.position);
                    utils.store.theme[type].order = order;
                });
            }, 250);
        });
    }
}
