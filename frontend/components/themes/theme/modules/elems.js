import { utils } from 'utils';

export class Elems {
    static selectors = {
        base: [
            'goBack',
            'form',
            'text',
            'textPrev',
            'codeText',
            'code_textPrev',
            'timer',
            'timerPrev',
            'secondary',
            'secondaryPrev',
            'thirdly',
            'thirdlyPrev',
            'colorPicker',
            'color',
            'fontStyle',
            'fontSize',
            'fontFamily',
            'textSection',
            'selectSize',
            'selectFamily',
            'dropdown',
            'toggleDropdown',
            'position',
            'check',
            'prev',
            'theme',
            'appearanceRadio',
            'timer-value',
            'code_text-value',
            'border',
            'corner',
            'submit',
            'timer-prev',
            'timer-sep',
            'timerType',
            'datepicker',
            'timepicker',
            'timepicker-popup',
            'overlay',
            'picker-wrap',
            'popupPosition',
            'rangeValue',
            'wysiwyg',
            'image',
            'imagePrev',
            'imagePosition',
            'send'
        ]
    };

    static query() {
        utils.queryElems(Elems.selectors.base, this.elems);
    }
}
