export class Bar {
    static init() {
        let title ='Edit theme';

        let config = {
            title,
            buttons: {
                primary: {
                    label: 'Save',
                    callback: () => this.elems.submit.click()
                },
                secondary: [{
                    label: 'Cancel',
                    callback: () => this.back()
                }]
            }
        };

        ShopifyApp.Bar.initialize(config);
    }
}
