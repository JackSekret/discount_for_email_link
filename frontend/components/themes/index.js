import { utils } from 'utils';

export class Themes {
    elems = {};
    selectors = { base: [] };

    constructor() {
        this.init();
    }

    async getData() {
        return await utils.http.get.themes();
    }

    init() {
        this.getData().then(res => {
            console.log(res);
            this.render(res);
            this.listenEvents();
            ShopifyApp.Bar.initialize({});
        });
    }

    render(data) {
        utils.render({
            template: 'themes',
            route: 'themes',
            className: 'themes',
            params: { data }
        }).queryElems(this.selectors.base, this.elems);
    }

    listenEvents() {}
}
