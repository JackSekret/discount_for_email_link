import { utils } from 'utils';

export class Statistics {
    elems = {};
    selectors = {
        base: [
            // 'table',
            // 'cards',
            // 'empty',
            'create'
        ]
    };

    constructor() {
        this.get().then(res => this.init(res));
    }

    async get() {
        return await utils.http.get.statistics();
    }

    init(res) {
        console.log(res);
        this.initBar();
        this.prepare(res);
        this.render(utils.store.statistics);
        this.listenEvents();
    }

    prepare(res) {
        utils.store.statistics = { ...res };
        utils.store.statistics.items = [ ...res.statistics ];
        utils.store.statistics.views = this.views;
        utils.store.statistics.orders = this.orders;
    }

    initBar() {
        ShopifyApp.Bar.initialize({ title: 'Statistics' });
    }

    get views() {
        if (!utils.store.statistics.items.length) return 0;
        return utils.store.statistics.items.map(item => item.views).reduce((sum, current) => sum + current);
    }

    get orders() {
        if (!utils.store.statistics.items.length) return 0;
        return utils.store.statistics.items.map(item => item.created_orders).reduce((sum, current) => sum + current);
    }

    render(data) {
        utils.render({
            template: 'statistics',
            route: 'statistics',
            className: 'statistics',
            params: { data }
        }).queryElems(this.selectors.base, this.elems);
    }

    listenEvents() {
        utils.emitter.subscribe('click', this.elems.create, () => utils.navigate('links/new'));
    }
}
