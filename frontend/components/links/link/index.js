import { utils }   from 'utils';
import { helpers } from './helpers';

export class Link {
    match = location.pathname.match(/links\/(\d+)/);
    id = this.match && this.match[1];
    path = this.id ? this.id : 'new';
    method = this.id ? 'edit' : 'create';
    changed = false;
    currency;
    data;
    links;

    elems = {};
    selectors = {
        base: [
            'goBack',
            'value',
            'productPicker',
            'collectionPicker',
            'appllies_to',
            'reset',
            'submit',
            'delete',
            'deleteDiscount',
            'status',
            'hiddenStatus',
            'statusLabel',
            'code',
            'generate',
            'appllies_toSection',
            'select',
            'icon',
            'inputValue',
            'promote',
            'dropdown',
            'dropdownClose',
            'copy',
            'linkType',
            'variantTitle',
            'input',
            'statusColor',
            'variant',
            'variantLink',
            'variants',
            'edit',
            'editTheme',
            'toggleVariants',
            'checkVariant',
            'checkProduct',
            'checkProductLabel',
            'variant_item',
            'cart',
            'checkout',
            'allVariants',
            'timer-prev'
        ]
    };

    constructor() {
        this.init();
    }

    async getData() {
        return {
            data: this.id && await utils.http.get.link({ id: this.id }),
            shop: !this.currency && await utils.http.get.shop()
        };
    }

    async editData(data, method) {
        return await utils.http[method ? method : this.method].link(data);
    }

    init() {
        this.currency = localStorage.getItem('shop-currency');
        let origin = ShopifyApp.shopOrigin;

        this.getData().then(res => {
            console.log(res);
            if (!this.currency) {
                this.currency = res.shop.currency;
                localStorage.setItem('shop-currency', this.currency);
            }

            this.data = res.data;
            this.data.origin = origin;
            res.data.currency = this.currency;
            res.data.isView = true;
            utils.store.theme = { ...res.data.custom_theme.data };

            this.initBar(res.data.status);
            this.render(res.data);
            this.listenEvents();
        });
    }

    initBar(status) {
        let title = 'View link';
        let text = status ? 'Disable' : 'Enable';

        let config = {
            title,
            buttons: {
                secondary: [
                    {
                        style: 'primary',
                        label: 'Edit',
                        callback: () => utils.navigate(`links/${this.id}/edit`)
                    },
                    {
                        label: text,
                        callback: () => this.onStatusChange(!status)
                    }
                ],
                primary: {
                    label: 'Delete',
                    style: 'danger',
                    callback: () => this.onDeleteDiscount()
                }
            }
        };

        ShopifyApp.Bar.initialize(config);
    }

    onStatusChange(state) {
        let action = state ? 'enable' : 'disable';
        let config = helpers.confirm[action](this.data.title);
        let data = {
            id: this.id,
            data: {
                id: this.id,
                is_active: state
            }
        };

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) return;

            this.editData(data, 'active').then(() => {
                ShopifyApp.flashNotice(`Link has been ${state ? 'enabled' : 'disabled'}`);
                this.elems.statusColor.classList.toggle('active', state);
                this.elems.statusColor.classList.toggle('expired', !state);
                this.elems.statusColor.innerHTML = state ? 'Active' : 'Expired';
                this.initBar(state);
            });
        });
    }

    onLinkTypeChange(e, type) {
        type = type || e.target.dataset.linktype;
        let id, title;
        if (this.elems.variant) {
            if (!Array.isArray(this.elems.variant)) this.elems.variant = [this.elems.variant];
            id = this.elems.variant.filter(item => item.checked).map(item => item.value)[0];
            title = this.elems.variant.filter(item => item.checked).map(item => item.dataset.title)[0];
        }
        let discount = `${this.data.origin}/discount/${this.data.code}`;
        let product = this.data.links[0].replace(/.+\/products\//, '');
        let links = {
            product: `${discount}?redirect=%2Fproducts%2F${product}`,
            variant: `${discount}?redirect=%2Fproducts%2F${product}&variant=${id}`,
            home: discount,
            cart: this.data.product && `${discount}?redirect=%2Fcart%2Fadd?id=${id}`,
            checkout: this.data.product && `${discount}?redirect=%2Fcart%2F${id}:1`
        };

        this.elems.input.value = links[type];
        this.elems.variantLink.classList.toggle('hidden', !id);
        this.elems.variantTitle.innerHTML = title;
    }

    onCopy(e) {
        let target = e.target;
        let parent = target.closest('[data-copyLink]');
        let input = parent.querySelector('[data-input]');

        utils.copy(input);
    }

    onCheck(e) {
        e.stopPropagation();
        this.onLinkTypeChange(e, 'variant');
        this.elems.cart.classList.remove('hidden');
        this.elems.checkout.classList.remove('hidden');
    }

    onVariantCheck(e) {
        if (e.target.dataset.radioRing === '' || e.target.dataset.linktype === 'product') return;
        let radio = e.currentTarget.querySelector('[data-linktype]');
        this.elems.cart.classList.remove('hidden');
        this.elems.checkout.classList.remove('hidden');
        radio.checked = true;
        this.onLinkTypeChange(e, 'variant');
    }

    onProductCheck(e) {
        if (!/hidden/.test(this.elems.toggleVariants[0].className)) return;
        this.elems.allVariants.checked = true;
        this.elems.cart.classList.add('hidden');
        this.elems.checkout.classList.add('hidden');
        this.elems.linkType.forEach(item => {
            if (item.dataset.linktype === 'product') item.checked = true;
        });
        this.onLinkTypeChange(e, 'product');
    }

    toggleVariants() {
        let isVisible = !/hidden/.test(this.elems.variants.className);
        this.elems.variants.classList.toggle('hidden', isVisible);
        this.elems.toggleVariants[0].classList.toggle('hidden', !isVisible);
        this.elems.toggleVariants[1].classList.toggle('hidden', isVisible);
        this.elems.checkProduct.classList.toggle('hidden', isVisible);
    }

    calcDate() {
        let timestamp = utils.store.theme.timer.timestamp;
        let diff = (timestamp - +new Date()) / 1000;

        let date = {};
        date.day = Math.floor(diff / 60 / 60 / 24);
        diff -= date.day * 60 *60 * 24;
        date.hour = Math.floor(diff / 60 / 60);
        diff -= date.hour * 60 *60;
        date.min = Math.floor(diff / 60);
        diff -= date.min * 60;
        date.sec = Math.floor(diff);

        this.updatePrev(date);
    }

    updatePrev(date) {
        this.elems['timer-prev'].forEach(item => {
            let num = date[item.dataset.timerPrev];
            item.innerHTML = `${num < 10 ? '0' + num : num}`;
        });
    }

    onDeleteDiscount() {
        let data = {
            id: this.id,
            data: { id: this.id }
        };
        let config = helpers.confirm.delete(this.data.title);

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) return;

            this.method = 'delete';
            this.editData(data).then(() => utils.navigate('/discounts'));
        });
    }

    render(data) {
        utils.render({
            template: 'link',
            route: `links/${this.path}`,
            params: { data }
        })
            .queryElems(this.selectors.base, this.elems);

        utils.store.theme.timer.type === 'to' && setInterval(() => this.calcDate(), 1000);
    }

    listenEvents() {
        utils.emitter.component(Link);
        utils.emitter.subscribe('click', this.elems.deleteDiscount, () => this.onDeleteDiscount());
        utils.emitter.subscribe('click', this.elems.goBack, () => utils.navigate('/links'));
        utils.emitter.subscribe('click', this.elems.copy, (e) => this.onCopy(e));
        utils.emitter.subscribe('change', this.elems.linkType, (e) => this.onLinkTypeChange(e));
        utils.emitter.subscribe('change', this.elems.checkVariant, (e) => this.onCheck(e));
        utils.emitter.subscribe('change', this.elems.allVariants, (e) => this.onProductCheck(e));
        utils.emitter.subscribe('click', this.elems.checkProductLabel, (e) => this.onProductCheck(e));
        utils.emitter.subscribe('click', this.elems.toggleVariants, () => this.toggleVariants());
        utils.emitter.subscribe('click', this.elems.variant_item, (e) => this.onVariantCheck(e));
        utils.emitter.subscribe('click', this.elems.edit, () => utils.navigate(`/links/${this.id}/edit`));
        utils.emitter.subscribe('click', this.elems.editTheme, () => utils.navigate(`/links/${this.id}/theme`));
    }
}
