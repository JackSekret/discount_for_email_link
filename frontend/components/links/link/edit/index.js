import { utils }   from 'utils';
import { helpers } from '../helpers';
import * as  m from './modules';

export class LinkEdit {
    match = location.pathname.match(/links\/(\d+)/);
    id = this.match && this.match[1];
    path = this.id ? `${this.id}/edit` : 'new';
    method = this.id ? 'edit' : 'create';

    changed = false;
    discounts;
    currency;
    data;
    links;
    token;
    type;
    modal;
    loading = false;
    page = 1;
    allPriceRules;
    variantsCount;
    savedType;
    beforeSubmitType;
    isShow = true;
    name = LinkEdit.name;
    elems = {};

    constructor() {
        this.init();
    }

    async get() {
        return {
            data: this.id && await utils.http.get.link({ id: this.id }),
            shop: !this.currency && await utils.http.get.shop()
        };
    }

    async edit(data, method) {
        return await utils.http[method ? method : this.method].link(data);
    }

    init() {
        this::m.Bar.init();

        this.get().then(res => {
            this.prepare(res);
            this::m.Events.listen(LinkEdit.name);
        });
    }

    prepare(res) {
        console.log(res);
        this.currency = localStorage.getItem('shop-currency');
        let origin = ShopifyApp.shopOrigin;

        if (!this.currency) {
            this.currency = res.shop.currency;
            localStorage.setItem('shop-currency', this.currency);
        }

        if (this.id) {
            this.data = res.data;
            res.data.currency = this.currency;
            res.data.edit = true;
            res.item = res.data.variant;
            this.token = res.data.token;
            this.savedType = res.data.link_type;
            utils.store.link = { ...res.data };
            this.type = utils.store.link.link_type;
            this.render(res.data, res.data.variant, true);
        } else {
            this.getToken();

            this.data = {
                link_type: 'homepage',
                currency: this.currency,
                link_to: `${origin}/?${this.token}`,
                url: origin,
                new: true,
                status: true,
                token: this.token,
                discounts: res.priceRules
            };

            utils.store.link = { ...this.data };
            this.render(this.data);
        }
    }

    getToken() {
        this.token = Math.random().toString(36).slice(7);
    }

    onLinkTypeChange(type, id) {
        let origin = ShopifyApp.shopOrigin;
        let handle = utils.store.link.handle;
        let discount = `${origin}/discount/${utils.store.link.discount.code}`;

        let links = {
            homepage: discount,
            product: `${discount}?redirect=%2Fproducts%2F${handle}`,
            collection: `${discount}?redirect=%2Fcollection%2F${handle}`,
            variant: `${discount}?redirect=%2Fproducts%2F${handle}&variant=${id}`,
            cart: `${discount}?redirect=%2Fcart%2Fadd?id=${id}`,
            checkout: `${origin}/cart/${id}:1?discount=${utils.store.link.discount.code}`
        };

        utils.store.link.url = links[type];
        utils.store.link.link_type = type;
    }

    onCopy(e) {
        let target = e.target;
        let parent = target.closest('[data-copyLink]');
        let input = parent.querySelector('[data-input]');

        utils.copy(input);
    }

    onAppliesToChange(e) {
        if (!utils.store.link.discount) {
            e.preventDefault();
            let config = {
                title: 'Warning',
                message: 'First select the discount',
                okButton: 'Ok'
            };
            ShopifyApp.Modal.alert(config);
            return;
        };

        this.type = this.elems.link_type.filter(item => item.checked).map(item => item.value)[0];
        let isProduct = this.type !== 'collection' && this.type !== 'homepage';
        this.elems.productPicker.classList.toggle('hidden', !isProduct);
        this.elems.collectionPicker.classList.toggle('hidden', this.type !== 'collection');

        if (this.isShow) {
            isProduct && this.elems.picker[0].click();
            this.type === 'collection' && this.elems.picker[1].click();
        }

        if (this.type === 'homepage') {
            this.beforeSubmitType = this.type;
            this.savedType = this.type;
            this.elems.productPicker.classList.add('hidden');
            this.elems.collectionPicker.classList.add('hidden');
            this.elems.edit.forEach(item => item.classList.add('hidden'));
            this.onLinkTypeChange('homepage');
        } else this.beforeSubmitType = this.type;
    }

    onCheck(e, variant) {
        if (!this.elems.check) return;
        if (variant && e.currentTarget.dataset.variant_item === '') {
            let radio = e.currentTarget.querySelector('[data-check]');
            let id = +radio.value;
            radio.checked = true;

            utils.store.link.variant = utils.store.link._variants.items
                .filter(item => item.id === id)
                .map(item => {
                    let image = item.image_id && utils.store.link._variants.images.filter(img => img.id === item.image_id)[0];

                    return {
                        id: item.id,
                        title: `${utils.store.link._variants.title}&nbsp;&bull;&nbsp;${item.title}`,
                        image: image ? image.src : utils.store.link._variants.src
                    };
                })[0];

            this.onLinkTypeChange(this.type, id);
        }
    }

    toggleVariants(e) {
        let product = e.target.closest('[data-template]');
        let variants = product.querySelector('[data-variants]');
        let isVisible = !/hidden/.test(variants.className);
        e.target.innerHTML = isVisible ? `Show variants (${this.variantsCount})` : 'Hide';

        variants.classList.toggle('hidden', isVisible);
    }

    onCancel(path) {
        let isChanged;
        let elements = Array.from(this.elems.form.elements);

        elements.forEach((item) => {
            if (item.defaultValue === undefined || /status/.test(item.name)) return;

            if (/applies/.test(item.name)) {
                if (item.checked !== item.defaultChecked) isChanged = true;
            } else {
                if (!item.name) return;
                if (item.value !== item.defaultValue) isChanged = true;
            }
        });

        if (!isChanged) {
            utils.navigate(path);
            m.Discounts.remove();
            return;
        }

        let config = helpers.confirm.cancel();

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) return;
            utils.navigate(path);
            m.Discounts.remove();
        });
    }

    onReset() {
        // location.reload()
        // this.init();
        // if (this.id) this.elems.discount_type.value = 'percentage';
        // if (this.id) this.elems.appllies_to[0].checked = true;

        // setTimeout(() => {
        //     this.onDiscountTypeChange();
        //     this.onAppliesToChange();
        // });
    }

    onSubmit(e) {
        let elems = e.target.elements;
        let title = utils.sanitize(elems::utils.filter(item => item.name === 'title')[0].value);

        let data = { title };
        let keys = [
            'id',
            'url',
            'token',
            'status',
            'link_type',
            'link_to',
            'product',
            'collection',
            'variant',
            'discount'
        ];
        let types = {
            product: /product/,
            collection: /collection/,
            variant: /variant|cart|checkout/
        };

        keys.forEach(key => {
            if (types[key] && !types[key].test(this.type)) return;
            data[key] = utils.store.link[key];
        });

        data = this.id ? { data, id: this.id } : { data };

        console.log(data);

        // // utils.loader(true);
        // // utils.overlay(true);
        // // ShopifyApp.flashNotice(this.id ? 'Updating...' : 'Creating...');

        this.edit(data).then(() => {
            // utils.overlay(false);
            utils.navigate(`${this.id ? `links/${this.id}` : 'themes'}`);
            // utils.navigate(`/links/${res.id}`)
            // ShopifyApp.flashNotice(this.id ? 'Link was updated' : 'Your link was created');
            m.Discounts.remove();
        });
    }

    render(data, item, edit) {
        utils.render({
            template: 'link/edit',
            route: `links/${this.path}`,
            params: {
                data,
                item,
                edit
            }
        });

        this::m.Elems.query();
        this.elems.titleInput.focus();
        this.asyncRender(data);
    }

    asyncRender(data) {
        this::m.Discounts.get(this.page).then(res => {
            let template = require('../helpers/discount-modal/template.njk');
            let html = template.render({
                data: res,
                currency: this.currency,
                link: data,
                new: data.new
            });

            document.body.insertAdjacentHTML('beforeend', html);
            this.discounts = res;
            this::m.Events.async();
        });
    }
}
