import { utils } from 'utils';
import * as  m   from './index';

export class Discounts {
    static async get(id) {
        return await utils.http.get.priceRules({ id });
    }

    static check(e) {
        e.currentTarget.querySelector('input').checked = true;
    }

    static select(e) {
        let ids = e.target.closest('[data-custom-modal]').querySelectorAll('input');
        let id = ids::utils.filter((item) => item.checked)[0].value;

        let discount = this.discounts.filter(item => item.id === +id)[0];
        this.elems.select.innerHTML = require('../../helpers/discount').render({
            item: discount,
            currency: this.currency
        });

        utils.store.link.discount = {
            price_rule_id: discount.id,
            code: discount.title,
            type: discount.target_type === 'line_item' ? discount.value_type : 'free_shipping',
            value: discount.value
        };

        let link = utils.store.link;
        let type = link.link_type;
        let key;

        if (/variant|cart|checkout/.test(type)) key = 'variant';
        else if (/product|collection/.test(type)) key = type;
        this.onLinkTypeChange(type, key && link[key].id);
        link.custom_theme && (link.custom_theme.data.code = discount.title);

        this::m.Bar.init();
        Discounts.hide(e);
    }

    static show() {
        this.modal = document.querySelector('[data-custom-modal]');
        let overlay = document.querySelector('[data-lock-page]');
        if (!this.modal) return;

        this.modal.classList.add('visible');
        overlay.classList.remove('hidden');
        ShopifyApp.Bar.initialize({});
    }

    static hide(e) {
        e.target.closest('[data-custom-modal]').classList.remove('visible');
        document.querySelector('[data-lock-page]').classList.add('hidden');
        this::m.Bar.init();
    }

    static remove() {
        document.querySelector('[data-custom-modal]').remove();
        document.querySelector('[data-lock-page]').remove();
    }

    static checkScrolling(ctx) {
        if (this.allPriceRules) return;
        let modal = this.modal.querySelector('[data-modal-body]');
        let pageHeight = modal.scrollHeight;
        let clientHeight = modal.clientHeight;
        let scrollTop = modal.pageYOffset || modal.scrollTop;
        let needLoad = pageHeight - (scrollTop + clientHeight) < 750;
        if (needLoad) {
            ctx::Discounts.load(this.data);
        }
    }

    static load(data) {
        if (this.loading) return;
        this.loading = true;
        this.page++;

        Discounts.get(this.page).then(res => {
            console.log(res);
            this.loading = false;
            this.allPriceRules = !res.length;
            let ul = document.querySelector('[data-product-variants]');

            res.forEach(item => {
                let template = require('../../helpers/discount-modal/item.njk');
                let html = template.render({
                    item,
                    currency: this.currency,
                    link: data,
                    new: data.new
                });
                let li = document.createElement('li');
                li.innerHTML = html;
                li.className = 'discount-item';
                li.dataset.discountItem = '';
                ul.appendChild(li);
            });
        });
    }
}
