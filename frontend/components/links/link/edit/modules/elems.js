import { utils } from 'utils';

export class Elems {
    static selectors = {
        base: [
            'form',
            'goBack',
            'goBackToDiscount',
            'picker',
            'discount_type',
            'value',
            'productPicker',
            'collectionPicker',
            'link_type',
            'reset',
            'submit',
            'delete',
            'deleteDiscount',
            'status',
            'hiddenStatus',
            'statusLabel',
            'code',
            'generate',
            'appllies_toSection',
            'select',
            'icon',
            'inputValue',
            // 'promote',
            'dropdown',
            'dropdownClose',
            'copy',
            'linkType',
            'input',
            'statusColor',
            'variants',
            'checkAll',
            'checkAllEl',
            'check',
            'toggleVariants',
            'variant_item',
            'deleteVariant',
            'titleInput',
            'modalCancel',
            'edit'
        ]
    };

    static query() {
        utils.queryElems(Elems.selectors.base, this.elems);
    }
}
