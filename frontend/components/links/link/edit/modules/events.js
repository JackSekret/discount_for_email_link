import { utils } from 'utils';
import * as  m   from './index';

export class Events {
    static listen(name) {
        utils.emitter.component(name);
        utils.emitter.subscribe('submit', this.elems.form, (e) => { e.preventDefault(); this.onSubmit(e); });
        utils.emitter.subscribe('reset', this.elems.form, (e) => this.onReset(e));
        // utils.emitter.subscribe('change', this.elems.status, (e) => this.onStatusChange(e));
        utils.emitter.subscribe('click', this.elems.picker, (e) => this::m.Picker.show(e.currentTarget, e.currentTarget.dataset.picker));
        utils.emitter.subscribe('click', this.elems.link_type, (e) => this.onAppliesToChange(e));
        // utils.emitter.subscribe('click', this.elems.delete, (e) => this.onDelete(e));
        // utils.emitter.subscribe('click', this.elems.deleteVariant, (e) => this.onDeleteVariant(e));
        // utils.emitter.subscribe('click', this.elems.deleteDiscount, () => this.onDeleteDiscount());
        utils.emitter.subscribe('click', this.elems.goBack, () => this.onCancel('/links'));
        utils.emitter.subscribe('click', this.elems.goBackToDiscount, () => this.onCancel(`/links/${this.id}`));
        // utils.emitter.subscribe('click', this.elems.promote, () => this.onPromote());
        // utils.emitter.subscribe('click', this.elems.dropdownClose, () => this.onPromote());
        utils.emitter.subscribe('click', this.elems.copy, (e) => this.onCopy(e));
        utils.emitter.subscribe('change', this.elems.check, (e) => this.onCheck(e));
        utils.emitter.subscribe('click', this.elems.toggleVariants, (e) => this.toggleVariants(e));
        utils.emitter.subscribe('click', this.elems.variant_item, (e) => this.onCheck(e, true));
        utils.emitter.subscribe('click', this.elems.select, (e) => this::m.Discounts.show(e));
    }

    static async() {
        utils.emitter.subscribe('click', document.querySelectorAll('[data-modal-cancel]'), (e) => this::m.Discounts.hide(e));
        utils.emitter.subscribe('click', document.querySelectorAll('[data-discount-item]'), (e) => this::m.Discounts.check(e));
        utils.emitter.subscribe('click', document.querySelectorAll('[data-modal-success]'), (e) => this::m.Discounts.select(e));
        utils.emitter.subscribe('scroll', document.querySelector('[data-modal-body]'), () => this::m.Discounts.checkScrolling(this));
    }
}
