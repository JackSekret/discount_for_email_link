import { utils } from 'utils';
import * as  m   from './index';

let self;

export class Picker {
    static show(elem, type) {
        self = this;
        elem.disabled = true;
        utils.overlay(true);
        this.elems.productPicker.classList.add('hidden');
        this.elems.collectionPicker.classList.add('hidden');
        ShopifyApp.Modal[type]({ 'selectMultiple': false }, Picker.callback);
    }

    static callback(success, data) {
        utils.overlay(false);

        if (!success) {
            self.elems.picker.forEach(item => item.disabled = false);
            self.elems.link_type.forEach(item => {
                if (!self.savedType) self.savedType = 'homepage';
                if (item.value === self.savedType) {
                    self.isShow = false;
                    item.click();
                    self.isShow = true;
                }
            });
            return;
        };
        // console.log(data);

        let type = data.collections ? 'collection' : 'product';
        utils.store.link.handle = data[`${type}s`][0].handle;
        data[`${type}s`].forEach(item => self::Picker.handle(item, type, self.type));
        self::m.Elems.query();
        utils.emitter.unsubscribe(self.name);
        self.elems.picker.forEach(item => item.disabled = false);
        self.elems[`${type}Picker`].classList.remove('hidden');
        self.savedType = self.beforeSubmitType;
        type === 'product' && data.products[0].variants && (self.variantsCount = data.products[0].variants.length);
        self.elems.edit.forEach(item => item.classList.toggle('hidden', self.type !== item.dataset.edit));
        self::m.Events.listen(self.name);
        self::m.Events.async();
        utils.store.link.link_type = self.savedType;

        if (data.errors) ShopifyApp.flashError(data.errors);
    }

    static handle(data, type, linkType) {
        // let link = document.querySelector('[data-discountLink]') as any;
        // link.value = `${ShopifyApp.shopOrigin}/${type}s/${data.handle}`;

        let empty = 'https://cdn.shopify.com/s/assets/admin/' +
        'no-image-large-1cfae84eca4ba66892099dcd26e604f5801fdadb3693bc9977f476aa160931ac.gif';
        let src = data.image ? data.image.src : empty;

        let productContainer = document.querySelector('[data-product]');
        let collectionContainer = document.querySelector('[data-collection]');

        let productTmpl = require('../../helpers/product');
        let collectionTempl = require('../../helpers/collection');
        let variantTempl = require('../../helpers/variant');

        (type === 'product' ? productContainer : collectionContainer).innerHTML = '';

        let item, variants;
        item = {
            id: data.id,
            image: src,
            title: data.title,
            variants: data.variants
        };
        let div = document.createElement('div');
        let template = type === 'product' ? productTmpl : collectionTempl;
        let container = type === 'product' ? productContainer : collectionContainer;

        let renderData = {
            id: data.id,
            [type]: item,
            link_type: type,
            linkType,
            isVariants: data.variants && data.variants.length > 1
        };
        div.innerHTML = template.render({ data: renderData });
        container.appendChild(div);

        if (/product|collection/.test(linkType)) {
            utils.store.link[linkType] = {
                id: item.id,
                title: item.title,
                image: item.image
            };

            this.onLinkTypeChange(linkType, utils.store.link[linkType].id);
        }

        if (data.variants) {
            utils.store.link._variants = {
                items: data.variants,
                images: data.images,
                title: item.title,
                src
            };

            let variantsContainer = div.querySelector('[data-variants]');
            let variantsControl = div.querySelector('[data-toggleVariants]');

            variantsContainer.innerHTML = '';
            linkType !== 'product' && variantsContainer.classList.remove('hidden');

            variants = data.variants.map((variant, i) => {
                let image = variant.image_id && data.images.filter(img => img.id === variant.image_id)[0];

                item = {
                    id: variant.id,
                    title: variant.title,
                    image: image ? image.src : src,
                    new: true,
                    checked: !i,
                    type: linkType
                };

                let li = document.createElement('li');
                li.innerHTML = variantTempl.render({ item });
                li.dataset.variant_item = '';
                // if (!i) li.querySelector('[data-check]').setAttribute('chrcked');
                variantsContainer.appendChild(li);

                return item;
            });

            if (/variant|cart|checkout/.test(linkType)) {
                utils.store.link.variant = {
                    id: variants[0].id,
                    title: `${utils.store.link._variants.title}&nbsp;&bull;&nbsp;${variants[0].title}`,
                    image: variants[0].image
                };
                this.onLinkTypeChange(linkType, utils.store.link.variant.id);
            }

            variants.length === 1 && variantsContainer.classList.add('hidden');
            variantsControl.classList.toggle('hidden', variants.length === 1);
        }
    }
}
