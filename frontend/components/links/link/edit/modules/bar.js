export class Bar {
    static init() {
        let title = this.id ? 'Edit link' : 'Create link';
        let text = this.id ? 'Discard' : 'Cancel';
        let reset =   () => this.onCancel('/links');

        let config = {
            title,
            buttons: {
                primary: {
                    label: 'Save',
                    callback: () => this.elems.submit.click()
                },
                secondary: [{
                    label: text,
                    callback: () => reset()
                }]
            }
        };

        ShopifyApp.Bar.initialize(config);
    }
}
