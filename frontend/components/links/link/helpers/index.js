import { confirm } from './confirm';
import { icons }   from './icons';

export const helpers = {
    confirm,
    icons
};
