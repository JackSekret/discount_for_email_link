export const confirm = {
    enable: (title) => {
        return {
            title: `Enable ${title}?`,
            message: `Are you sure you want to enable ${title}? This link will become active now and will have no end date.`,
            okButton: 'Enable',
            cancelButton: 'Cancel'
        };
    },
    disable: (title) => {
        return {
            title: `Disable ${title}?`,
            message: `Are you sure you want to disable ${title}? This link will expire now.`,
            okButton: 'Disable',
            cancelButton: 'Cancel'
        };
    },
    cancel: () => {
        return {
            title: 'You have unsaved changes on this page',
            message: 'If you leave this page, all unsaved changes will be lost. Are you sure you want to leave this page?',
            okButton: 'Leave page',
            cancelButton: 'Cancel',
            style: 'danger'
        };
    },
    delete: (title) => {
        return {
            title: `Delete ${title}?`,
            message: `Are you sure you want to delete the link ${title}? This action cannot be reversed.`,
            okButton: 'Delete link',
            cancelButton: 'Cancel',
            style: 'danger'
        };
    }
};
