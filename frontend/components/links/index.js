import { utils }   from 'utils';
import { helpers } from './link/helpers';
import { icons }   from './link/helpers';

export class Links {
    amount;
    elems = {};
    selectors = {
        base: [
            'create',
            'table',
            'empty',
            'status',
            'check',
            'checkAll',
            'onCheckInfo',
            'info',
            'thead',
            'toggleDropdown',
            'dropdown',
            'action',
            'edit',
            'view',
            'discount-item',
            'controls'
        ]
    };

    constructor() {
        this.init();
    }

    async getData() {
        return await utils.http.get.links();
    }

    async editData(method, data) {
        return await utils.http[method].link(data);
    }

    init() {
        this.getData().then(res => {
            console.log(res);
            this.render(res, res.currency);
            this.checkAmount(res.links.length);
            this.initBar();
            this.listenEvents();
            localStorage.setItem('shop-currency', res.currency);
        });
    }

    initBar() {
        let config = {
            title: 'Links',
            buttons: {
                primary: {
                    label: 'Create link',
                    callback: () => utils.navigate('/links/new')
                }
            }
        };

        ShopifyApp.Bar.initialize(this.amount ? config : { title: 'Links' });
    }

    checkAmount(amount) {
        if (amount) this.amount = amount;

        this.elems.table.classList.toggle('hidden', !this.amount);
        this.elems.empty.classList.toggle('hidden', this.amount);
        this.initBar();
    }

    onCheck(e) {
        let row = e.target.closest('[data-discount-item]');
        let amount = this.elems.check.filter(item => item.checked).length;
        let text = amount > 1 ? 'links' : 'link';

        this.elems.checkAll.checked = amount === this.amount;
        this.elems.info.innerHTML = `${amount} ${text} selected`;
        this.elems.onCheckInfo.classList.toggle('hidden', !amount);
        this.elems.thead.forEach(item => item.classList.toggle('invisible', amount));
        row.classList.toggle('selected', e.target.checked);
        this.elems.controls.forEach(item => item.classList.toggle('invisible', amount));
    }

    onCheckAll(e, state) {
        state = state || e.currentTarget.checked;
        let text = this.amount > 1 ? 'links' : 'link';

        this.elems.info.innerHTML = `${this.amount} ${text} selected`;
        this.elems.onCheckInfo.classList.toggle('hidden', !state);
        this.elems.check.forEach(item => item.checked = state);
        this.elems.thead.forEach(item => item.classList.toggle('invisible', state));
        this.elems.controls.forEach(item => item.classList.toggle('invisible', state));
        this.elems['discount-item'].forEach(item => item.classList.toggle('selected', state));
        this.elems.dropdown.classList.toggle('visible', !state);
    }

    toggleDropdown(isVisible) {
        isVisible = /visible/.test(this.elems.dropdown.className);
        this.elems.dropdown.classList.toggle('visible', !isVisible);
    }

    onAction(e) {
        let action = e.target.dataset.action || e.currentTarget.dataset.action;
        if (action === 'status') action = e.target.checked ? 'enable' : 'disable';

        let inputs = this.elems.check.filter(item => item.checked);
        inputs = inputs.length ? inputs : [e.currentTarget];

        let elems = inputs.map(item => item.closest('[data-discount-item]'));
        let ids = inputs.map(item => +item.dataset.id) || [+e.target.dataset.id];
        let amount = inputs.length;

        this.toggleDropdown(true);
        this.confirm(action, elems, ids, amount, e.target);
    }

    enable(data, elems, amount) {
        data.data.is_active = true;

        this.editData('active', data).then(() => {
            let text = amount && amount > 1 ? 'links' : 'discount';
            ShopifyApp.flashNotice(`Updated ${amount || 1} ${text}.`);

            elems.forEach(elem => {
                let action = elem.querySelector('[data-statuslabel]');
                let label = elem.querySelector('[data-status]');
                let check = elem.querySelector('[data-action="status"]');
                label.classList.add('active');
                label.innerHTML = 'Active';
                action.innerHTML = `${icons.disable}`;
                check.checked = true;
            });
        });
    }

    disable(data, elems, amount) {
        data.data.is_active = false;

        this.editData('active', data).then(() => {
            let text = amount && amount > 1 ? 'links' : 'discount';
            ShopifyApp.flashNotice(`Disabled ${amount || 1} ${text}.`);

            elems.forEach(elem => {
                let action = elem.querySelector('[data-statuslabel]');
                let label = elem.querySelector('[data-status]');
                let check = elem.querySelector('[data-action="status"]');
                label.classList.remove('active');
                label.innerHTML = 'Expired';
                action.innerHTML = `${icons.enable}`;
                check.checked = false;
            });
        });
    }

    delete(data, elems, amount) {
        this.editData('delete', data).then(() => {
            let text = amount && amount > 1 ? 'links' : 'discount';
            ShopifyApp.flashNotice(`Deleted ${amount || 1} ${text}.`);
            elems.forEach(elem => {
                elem.remove();
                this.amount--;
            });
            this.checkAmount();
            this.onCheckAll(event, false);
            this.elems.onCheckInfo.classList.add('hidden');
            this.elems.thead.forEach(item => item.classList.remove('invisible'));
            this.elems.controls.forEach(item => item.classList.remove('invisible'));
        });
    }

    confirm(action, elems, ids, amount, target) {
        let config = helpers.confirm[action](amount);
        let cb = () => this[action]({ data: { ids } }, elems, amount);

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) {
                if (target.dataset.action === 'status') target.checked = !target.checked;
                return;
            };
            cb();
        });
    }

    render(data, currency) {
        utils.render({
            template: 'links',
            route: 'links',
            className: data.links.length ? 'discounts' : 'container-empty',
            params: {
                data,
                currency
            }
        }).queryElems(this.selectors.base, this.elems);
    }

    listenEvents() {
        utils.emitter.component(Links);
        utils.emitter.subscribe('click', this.elems.create, () => utils.navigate('/links/new'));
        utils.emitter.subscribe('click', this.elems.edit, (e) => utils.navigate(`links/${e.currentTarget.dataset.id}/edit`));
        utils.emitter.subscribe('click', this.elems.view, (e) => utils.navigate(`links/${e.currentTarget.dataset.id}`));
        utils.emitter.subscribe('click', this.elems.check, (e) => this.onCheck(e));
        utils.emitter.subscribe('click', this.elems.checkAll, (e) => this.onCheckAll(e));
        utils.emitter.subscribe('click', this.elems.toggleDropdown, (e) => this.toggleDropdown(e));
        utils.emitter.subscribe('click', this.elems.action, (e) => this.onAction(e));
    }
}
