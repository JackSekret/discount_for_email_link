import { Components as C } from 'components';
import { utils } from 'utils';

export class Header {
    elems = {};
    selectors = { base: ['nav', 'open'] };

    constructor() {
        this.init();
        // this.getData().then(res => this.init(res));
    }

    async getData() {
        // return {
        //     settings: await utils.http.get.settings()
        // };
    }

    init() {
        utils.loader(true);
        this.render();
        this.listenEvents();
        utils.router('active');
    }

    toggleNav(e) {
        let isVisible = /visible/.test(this.elems.nav.className);
        let target = e.target.dataset.nav === '';

        if (!isVisible && !target) return;
        this.elems.nav.classList.toggle('visible', !isVisible);
        this.elems.open.classList.toggle('opened', !isVisible);
    }

    openNav() {
        let isVisible = /visible/.test(this.elems.nav.className);
        setTimeout(() => {
            this.elems.nav.classList.toggle('visible', !isVisible);
            this.elems.open.classList.toggle('opened', !isVisible);
        });
    }

    render() {
        utils.render({
            template: 'header',
            container: 'body',
            element: 'header',
            classList: ['main-header'],
            attribute: 'data-header'
        }).queryElems(this.selectors.base, this.elems);

        new C.Container();
        new C.Footer();
    }

    listenEvents() {
        this.elems.nav.addEventListener('click', (e) => this.toggleNav(e));
        this.elems.open.addEventListener('click', (e) => this.openNav(e));
    }
}
