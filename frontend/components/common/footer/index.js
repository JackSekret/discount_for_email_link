import { utils } from 'utils';

export class Footer {
    constructor() {
        this.render();
    }

    render() {
        utils.render({
            template: 'footer',
            container: 'body',
            element: 'footer',
            className: 'main-footer',
            attribute: 'data-footer'
        });
    }
}
