import { utils } from 'utils';

export class Container {
    constructor() {
        this.render();
    }

    render() {
        utils.render({
            template: 'container',
            container: 'body',
            element: 'main',
            className: 'main-container',
            attribute: 'data-main-container'
        });
    }
}
