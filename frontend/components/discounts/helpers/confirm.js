export const confirm = {
    enable: () => {
        return {
            title: 'Enable selected discounts',
            message: 'Are you sure you want to enable the selected discounts? These discounts will become active now.',
            okButton: 'Enable',
            cancelButton: 'Cancel'
        };
    },
    disable: () => {
        return {
            title: 'Disable selected discounts',
            message: 'Are you sure you want to disable the selected discounts? These discounts will expire now.',
            okButton: 'Disable',
            cancelButton: 'Cancel'
        };
    },
    delete: (amount) => {
        let text = amount > 1 ? 'discounts' : 'discount';
        return {
            title: `Delete ${amount} ${text}?`,
            message: 'Deleted discounts cannot be recovered. Do you still want to continue?',
            okButton: 'Delete',
            cancelButton: 'Cancel',
            style: 'danger'
        };
    }
};
