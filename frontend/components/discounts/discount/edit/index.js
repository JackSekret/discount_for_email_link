import { utils }   from 'utils';
import { helpers } from '../helpers';

let self;

export class DiscountEdit {
    match = location.pathname.match(/discounts\/(\d+)/);
    id = this.match && this.match[1];
    path = this.id ? `${this.id}/edit` : 'new';
    method = this.id ? 'edit' : 'create';
    changed = false;
    currency;
    data;
    links;

    elems = {};
    selectors = {
        base: [
            'form',
            'goBack',
            'goBackToDiscount',
            'picker',
            'discount_type',
            'value',
            'productPicker',
            'collectionPicker',
            'appllies_to',
            'reset',
            'submit',
            'delete',
            'deleteDiscount',
            'status',
            'hiddenStatus',
            'statusLabel',
            'code',
            'generate',
            'appllies_toSection',
            'select',
            'icon',
            'inputValue',
            'promote',
            'dropdown',
            'dropdownClose',
            'copy',
            'linkType',
            'input',
            'statusColor',
            'variants',
            'checkAll',
            'checkAllEl',
            'check',
            'toggleVariants',
            'variant_item',
            'deleteVariant'
        ]
    };

    constructor() {
        this.init();
    }

    async getData() {
        return {
            data: this.id && await utils.http.get.discount({ id: this.id }),
            shop: !this.currency && await utils.http.get.shop()
        };
    }

    async editData(data, method) {
        return await utils.http[method ? method : this.method].discount(data);
    }

    init() {
        this.currency = localStorage.getItem('shop-currency');
        let origin = ShopifyApp.shopOrigin;

        this.getData().then(res => {
            if (!this.currency) {
                this.currency = res.shop.currency;
                localStorage.setItem('shop-currency', this.currency);
            }

            if (this.id) {
                // cart = `${origin}/cart/${res.data.product.id}:1`
                this.data = res.data;
                this.data.origin = origin;
                res.data.currency = this.currency;
                res.data.isVariants = res.data.product && !!res.data.product.variants.length;
                res.data.edit = true;
                // res.data.links = {};
                // res.data.links.cart = cart;
                this.render(res.data);
            } else {
                let data = {
                    applies_to: 'cart',
                    discount_type: 'percentage',
                    currency: this.currency,
                    link: origin,
                    new: true
                };
                this.render(data);
            }

            this.listenEvents();
        });

        this.initBar();
        self = this;
    }

    initBar() {
        let title = this.id ? 'Edit discount' : 'Create discount';
        let text = this.id ? 'Discard' : 'Cancel';
        let reset = this.id ? () => this.elems.reset.click() : () => this.onCancel('/discounts');

        let config = {
            title,
            buttons: {
                primary: {
                    label: 'Save',
                    callback: () => this.elems.submit.click()
                },
                secondary: [{
                    label: text,
                    callback: () => reset()
                }]
            }
        };

        ShopifyApp.Bar.initialize(config);
    }

    onStatusChange(e) {
        let state = e.target.checked;
        let action = state ? 'enable' : 'disable';
        let config = helpers.confirm[action](this.data.title);
        let data = {
            id: this.id,
            data: {
                id: this.id,
                is_active: state
            }
        };

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) {
                e.target.checked = !e.target.checked;
                return;
            };

            this.editData(data, 'active').then(() => {
                ShopifyApp.flashNotice(`Discount has been ${state ? 'enabled' : 'disabled'}`);
                this.elems.statusLabel.innerHTML = state ? `${helpers.icons.disable} Disable` : `${helpers.icons.enable} Enable`;
                this.elems.statusColor.classList.toggle('active', state);
                this.elems.statusColor.classList.toggle('expired', !state);
                this.elems.statusColor.innerHTML = state ? 'Active' : 'Expired';
                this.elems.hiddenStatus.checked = state;
            });
        });
    }

    onPromote() {
        let elem = this.elems.dropdown;
        let isOpen = !/hidden/.test(elem.className);

        this.elems.dropdown.classList.toggle('hidden', isOpen);
    }

    onLinkTypeChange(e) {
        let type = e.target.dataset.linktype;
        let discount = `${this.data.origin}/discount/${this.data.code}`;
        let product = this.data.link.replace(/.+\/products\//, '');
        let links = {
            product: `${discount}?redirect=%2Fproducts%2F${product}`,
            home: discount,
            cart: `${discount}?redirect=%2Fcart%2Fadd?id=${this.data.product.id}`,
            checkout: `${discount}?redirect=%2Fcart%2F${this.data.product.id}:1`
        };

        this.elems.input.value = links[type];
    }

    onCopy(e) {
        let target = e.target;
        let parent = target.closest('[data-copyLink]');
        let input = parent.querySelector('[data-input]');

        utils.copy(input);
    }

    onDiscountTypeChange(e) {
        let type = e && e.target.value;
        let isFreeShipping = e && e.target.value === 'free_shipping'
            || !e && this.elems.discount_type.value === 'free_shipping';

        this.elems.value.classList.toggle('hidden', !!isFreeShipping);
        this.elems.appllies_toSection.classList.toggle('hidden', !!isFreeShipping);
        this.elems.select.classList.toggle('full-width', !!isFreeShipping);
        this.elems.icon.forEach(item => item.classList.toggle('hidden', item.dataset.icon !== type));
        this.elems.inputValue.classList.toggle('discount-value', type === 'amount');
        this.elems.inputValue.classList.toggle('discount-percentage', type === 'percentage');
        this.elems.inputValue.type = type === 'amount' ? 'text' : 'number';
    }

    onAppliesToChange(e) {
        let value = this.elems.appllies_to.map(item => item.checked && item.value)[0];
        let isProduct = e && e.target.value === 'product' || !e && value === 'product';
        let isCollection = e && e.target.value === 'collection' || !e && value === 'collection';
        // let isVariants = isProduct && this.elems.variants.children.length;
        let isVariant = isProduct && this.elems.variants.children.length === 1;

        this.elems.productPicker.classList.toggle('hidden', !isProduct);
        this.elems.collectionPicker.classList.toggle('hidden', !isCollection);
        this.elems.toggleVariants.forEach(item => item.classList.toggle('hidden', isVariant));
        // this.elems.toggleVariants[1].classList.toggle('hidden', !isVariants);
    }

    onDelete(e) {
        let target = e.currentTarget;
        let template = target.closest('[data-template]');
        let type = template.dataset.template;

        template.remove();
        helpers.removePickerItem(type);
        this.elems.variants.innerHTML = '';
        this.elems.toggleVariants.classList.add('hidden');
    }

    onDeleteVariant(e) {
        e.stopPropagation();
        let target = e.currentTarget;
        let template = target.closest('[data-variant_item]');

        template.remove();

        let empty = this.elems.variants.children.length === 1;
        if (empty) template.querySelector('[data-deletevariant]').classList.add('hidden');

    }

    onCheckAll(e) {
        let state = e.target.checked;
        this.elems.check.forEach(item => item.checked = state);
    }

    onCheck(e, variant) {
        if (!this.elems.check) return;
        if (variant && e.currentTarget.dataset.variant_item === '') {
            let checkbox = e.currentTarget.querySelector('[data-check]');
            checkbox.checked = !checkbox.checked;
        }
        this.elems.checkAll.checked = this.elems.check.every(item => item.checked);
    }

    toggleVariants(e) {
        let product = e.target.closest('[data-template]');
        let variants = product.querySelector('[data-variants]');
        let checkAllEl = product.querySelector('[data-checkAllEl]');
        let isVisible = !/hidden/.test(variants.className);

        variants.classList.toggle('hidden', isVisible);
        !this.id && checkAllEl.classList.toggle('hidden', isVisible);

        // this.elems.toggleVariants.innerHTML = !isVisible ? 'Hide' : 'Show variants';
        // this.elems.toggleVariants[0].classList.toggle('hidden', !isVisible);
        // this.elems.toggleVariants[1].classList.toggle('hidden', isVisible);
        // this.elems.variants.classList.toggle('hidden', isVisible);
        // !this.id && this.elems.checkAllEl.classList.toggle('hidden', isVisible);
        // this.elems.toggleVariants.innerHTML = !isVisible ? 'Hide' : 'Show variants';
        // this.elems.toggleVariants[0].classList.toggle('hidden', !isVisible);
        // this.elems.toggleVariants[1].classList.toggle('hidden', isVisible);
    }

    showPicker(elem, type) {
        elem.disabled = true;
        ShopifyApp.Modal[type]({ 'selectMultiple': true }, this.handlePicker);
    }

    handlePicker(success, data) {
        if (!success) {
            self.elems.picker.forEach(item => item.disabled = false);
            return;
        };

        let type = data.collections ? 'collection' : 'product';
        data[`${type}s`].forEach(item => helpers.handlePicker(item, type));
        utils.queryElems(self.selectors.base, self.elems);
        utils.emitter.unsubscribe(DiscountEdit);
        self.elems.picker.forEach(item => item.disabled = false);
        self.listenEvents();

        if (data.errors) ShopifyApp.flashError(data.errors);
    }

    generateCode() {
        let code = Math.random().toString(36).slice(2).toUpperCase();
        this.elems.code.value = code;
    }

    onDeleteDiscount() {
        let data = {
            id: this.id,
            data: { id: this.id }
        };
        let config = helpers.confirm.delete(this.data.title);

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) return;

            this.method = 'delete';
            this.editData(data).then(() => utils.navigate('/discounts'));
        });
    }

    onCancel(path) {
        let isChanged;
        let elements = Array.from(this.elems.form.elements);

        elements.forEach((item) => {
            if (item.defaultValue === undefined || /status/.test(item.name)) return;

            if (/applies/.test(item.name)) {
                if (item.checked !== item.defaultChecked) isChanged = true;
            } else {
                if (!item.name) return;
                if (item.value !== item.defaultValue) isChanged = true;
            }
        });

        if (!isChanged) {
            utils.navigate(path);
            return;
        }

        let config = helpers.confirm.cancel();

        ShopifyApp.Modal.confirm(config, agree => {
            if (!agree) return;
            utils.navigate(path);
        });
    }

    onReset() {
        if (this.id) this.elems.discount_type.value = 'percentage';
        if (this.id) this.elems.appllies_to[0].checked = true;

        setTimeout(() => {
            this.onDiscountTypeChange();
            this.onAppliesToChange();
        });
    }

    onSubmit(e) {
        let elems = e.target.elements;
        let data = {};

        if (this.id) data.id = this.id;

        for (let elem of elems) {
            if (!elem.name) continue;
            if (elem.disabled) continue;
            if (/applies/.test(elem.name) && !elem.checked) continue;
            if (/applies/.test(elem.name) && elem.value === 'product') data.product = {};
            if (/applies/.test(elem.name) && elem.value === 'collection') data.collection = {};
            if (/variant/.test(elem.name)) continue;
            data[elem.name] = /status/.test(elem.name) ? elem.checked : elem.value;
        }

        if (data.product) {
            data.product.id = data['product[id]'];
            data.product.title = data['product[title]'];
            data.product.image = data['product[image]'];

            if (this.id) {
                data.product.variants = Array.from(this.elems.variants.children).map((item, i) => {
                    let ids = elems['variant[][id]'];
                    let titles = elems['variant[][title]'];
                    let images = elems['variant[][image]'];

                    if (!(ids instanceof NodeList)) {
                        ids = [ids];
                        titles = [titles];
                        images = [images];
                    } else {
                        ids = Array.from(ids);
                        titles = Array.from(titles);
                        images = Array.from(images);
                    }

                    return {
                        id: ids.map((item) => item.value)[i],
                        title: titles.map((item) => item.value)[i],
                        image: images.map((item) => item.value)[i]
                    };
                });
            } else if (elems['variant-check']) {
                let arr = Array.from(elems['variant-check']);
                let checked = arr.filter(item => item.checked).map(item => item.dataset.id);
                data.product.variants = checked.map(id => {
                    let ids = Array.from(elems['variant[][id]']);
                    let titles = Array.from(elems['variant[][title]']);
                    let images = Array.from(elems['variant[][image]']);

                    return {
                        id: ids.filter((item) => item.dataset.id === id)[0].value,
                        title: titles.filter((item) => item.dataset.id === id)[0].value,
                        image: images.filter((item) => item.dataset.id === id)[0].value
                    };
                });
            } else {
                let id = elems['variant[][id]'];
                let title = elems['variant[][title]'];
                let image = elems['variant[][image]'];

                data.product.variants = {
                    id: id.value,
                    title: title.value,
                    image: image.value
                };
            }
        } else if (data.collection) {
            data.collection.id = data['collection[id]'];
            data.collection.title = data['collection[title]'];
            data.collection.image = data['collection[image]'];
        }

        for (let key in data) {
            if (/\[.+\]/.test(key)) delete data[key];
        }

        data.links = [data.links];
        data.json = true;

        if (data.discount_type === 'free_shipping') delete data.applies_to;
        data = this.id ? {
            data,
            id: this.id
        } : { data };

        // utils.loader(true);
        // utils.overlay(true);
        // ShopifyApp.flashNotice(this.id ? 'Updating...' : 'Creating...');
        this.editData(data).then((res) => {
            // utils.overlay(false);
            utils.navigate(`/discounts/${res.id}`);
            ShopifyApp.flashNotice(this.id ? 'Discount was updated' : 'Your discount was created');
        });
    }

    render(data) {
        utils.render({
            template: 'discount/edit',
            route: `discounts/${this.path}`,
            params: { data }
        }).queryElems(this.selectors.base, this.elems);
    }

    listenEvents() {
        utils.emitter.component(DiscountEdit);
        utils.emitter.subscribe('submit', this.elems.form, (e) => { e.preventDefault(); this.onSubmit(e); });
        utils.emitter.subscribe('reset', this.elems.form, (e) => this.onReset(e));
        utils.emitter.subscribe('change', this.elems.status, (e) => this.onStatusChange(e));
        utils.emitter.subscribe('click', this.elems.generate, () => this.generateCode());
        utils.emitter.subscribe('click', this.elems.picker, (e) => this.showPicker(e.currentTarget, e.currentTarget.dataset.picker));
        utils.emitter.subscribe('change', this.elems.discount_type, (e) => this.onDiscountTypeChange(e));
        utils.emitter.subscribe('change', this.elems.appllies_to, (e) => this.onAppliesToChange(e));
        utils.emitter.subscribe('click', this.elems.delete, (e) => this.onDelete(e));
        utils.emitter.subscribe('click', this.elems.deleteVariant, (e) => this.onDeleteVariant(e));
        utils.emitter.subscribe('click', this.elems.deleteDiscount, () => this.onDeleteDiscount());
        utils.emitter.subscribe('click', this.elems.goBack, () => this.onCancel('/discounts'));
        utils.emitter.subscribe('click', this.elems.goBackToDiscount, () => this.onCancel(`/discounts/${this.id}`));
        utils.emitter.subscribe('click', this.elems.promote, () => this.onPromote());
        utils.emitter.subscribe('click', this.elems.dropdownClose, () => this.onPromote());
        utils.emitter.subscribe('click', this.elems.copy, (e) => this.onCopy(e));
        utils.emitter.subscribe('change', this.elems.linkType, (e) => this.onLinkTypeChange(e));
        utils.emitter.subscribe('change', this.elems.checkAll, (e) => this.onCheckAll(e));
        utils.emitter.subscribe('change', this.elems.check, (e) => this.onCheck(e));
        utils.emitter.subscribe('click', this.elems.toggleVariants, (e) => this.toggleVariants(e));
        utils.emitter.subscribe('click', this.elems.variant_item, (e) => this.onCheck(e, true));
    }
}
