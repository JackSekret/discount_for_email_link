export const icons = {
    enable: `
        <svg class="nav-icon status-icon enable" width="24" height="24">
            <use xlink:href="#enable" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
        </svg>`,
    disable: `
        <svg class="nav-icon status-icon disable" width="24" height="24">
            <use xlink:href="#disable" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
        </svg>`
};
