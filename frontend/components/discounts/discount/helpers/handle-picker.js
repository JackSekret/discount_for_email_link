export function handlePicker(data, type) {
    let link = document.querySelector('[data-discountLink]');
    link.value = `${ShopifyApp.shopOrigin}/${type}s/${data.handle}`;

    let empty = 'https://cdn.shopify.com/s/assets/admin/' +
    'no-image-large-1cfae84eca4ba66892099dcd26e604f5801fdadb3693bc9977f476aa160931ac.gif';
    let src = data.image ? data.image.src : empty;

    let variantsControl = document.querySelector('[data-toggleVariants]');
    let productContainer = document.querySelector('[data-product]');
    let collectionContainer = document.querySelector('[data-collection]');

    let productTmpl = require('./product');
    let collectionTempl = require('./collection');
    let variantTempl = require('./variant');

    let item, variants;
    item = {
        id: data.id,
        image: src,
        title: data.title,
        variants: data.variants
    };
    let div = document.createElement('div');
    let template = type === 'product' ? productTmpl : collectionTempl;
    let container = type === 'product' ? productContainer : collectionContainer;

    let renderData = {
        id: data.id,
        [type]: item,
        applies_to: type,
        isVariants: data.variants && data.variants.length > 1
    };
    div.innerHTML = template.render({ data: renderData });
    container.appendChild(div);

    if (data.variants) {
        let variantsContainer = div.querySelector('[data-variants]');
        variantsContainer.innerHTML = '';

        variants = data.variants.map(variant => {
            let image = variant.image_id && data.images.filter(img => img.id === variant.image_id)[0];

            item = {
                id: variant.id,
                title: variant.title,
                image: image ? image.src : src,
                new: true
            };

            let li = document.createElement('li');
            li.innerHTML = variantTempl.render({ item });
            li.dataset.variant_item = '';
            variantsContainer.appendChild(li);

            return item;
        });

        variantsControl.classList.toggle('hidden', variants.length === 1);
    }
}
