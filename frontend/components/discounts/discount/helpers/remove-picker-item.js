export function removePickerItem(type) {
    const inputs = document.querySelectorAll(`input[name^="${type}"]`);

    Array.from(inputs).forEach(input => {
        if (/id/.test(input.name)) input.value = '';
        if (/title/.test(input.name)) input.value = '';
        if (/image/.test(input.name)) input.value = '';

        // input.disabled = true;
    });
}
