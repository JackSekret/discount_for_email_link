// import { utils }        from 'utils';

import { Header }       from './common/header';
import { Container }    from './common/container';
import { Footer }       from './common/footer';
import { Statistics }   from './statistics';
import { Links }        from './links';
import { Link }         from './links/link';
import { Themes }       from './themes';
import { Theme }        from './themes/theme';
import { LinkEdit }     from './links/link/edit';
import { Discounts }    from './discounts';
import { Discount }     from './discounts/discount';
import { DiscountEdit } from './discounts/discount/edit';
import { Settings }     from './settings';

export const Components = {
    Header,
    Container,
    Footer,
    Statistics,
    Links,
    Link,
    LinkEdit,
    Themes,
    Theme,
    Discounts,
    Discount,
    DiscountEdit,
    Settings
};

// if (module.hot) {
//     module.hot.accept();

//     module.hot.accept('./statistics', () => {
//         Components.Statistics = Statistics;
//         utils.emitter.unsubscribe(Statistics);
//         utils.path(Statistics) && utils.clear();
//         utils.path(Statistics) && new Statistics();
//     });
//     module.hot.accept('./links', () => {
//         Components.Links = Links;
//         utils.emitter.unsubscribe(Links);
//         utils.path(Links) && utils.clear();
//         utils.path(Links) && new Links();
//     });
//     module.hot.accept('./themes', () => {
//         Components.Themes = Themes;
//         utils.emitter.unsubscribe(Themes);
//         utils.path(Themes) && utils.clear();
//         utils.path(Themes) && new Themes();
//     });
//     module.hot.accept('./themes/theme', () => {
//         alert();
//         Components.Theme = Theme;
//         utils.emitter.unsubscribe(Theme);
//         utils.path(Theme) && utils.clear();
//         utils.path(Theme) && new Theme();
//     });
//     module.hot.accept('./discounts', () => {
//         Components.Discounts = Discounts;
//         utils.emitter.unsubscribe(Discounts);
//         utils.path(Discounts) && utils.clear();
//         utils.path(Discounts) && new Discounts();
//     });
//     module.hot.accept('./discounts/discount', () => {
//         Components.Discount = Discount;
//         utils.emitter.unsubscribe(Discount);
//         utils.path(Discount) && utils.clear();
//         utils.path(Discount) && new Discount();
//     });
//     module.hot.accept('./links/link', () => {
//         Components.Link = Link;
//         utils.emitter.unsubscribe(Link);
//         utils.path(Link) && utils.clear();
//         utils.path(Link) && new Link();
//     });
//     module.hot.accept('./links/link/edit', () => {
//         Components.LinkEdit = LinkEdit;
//         utils.emitter.unsubscribe(LinkEdit);
//         utils.path(LinkEdit) && utils.clear();
//         utils.path(LinkEdit) && new LinkEdit();
//     });
//     module.hot.accept('./discounts/discount/edit', () => {
//         Components.DiscountEdit = DiscountEdit;
//         utils.emitter.unsubscribe(DiscountEdit);
//         utils.path(DiscountEdit) && utils.clear();
//         utils.path(DiscountEdit) && new DiscountEdit();
//     });
//     module.hot.accept('./settings', () => {
//         Components.Settings = Settings;
//         utils.emitter.unsubscribe(Settings);
//         utils.path(Settings) && utils.clear();
//         utils.path(Settings) && new Settings();
//     });
// }
