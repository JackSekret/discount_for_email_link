import { utils } from 'utils';

export class Settings {
    constructor() {
        this.init();
    }

    init() {
        this.render();
        this.initBar();
    }

    initBar() {
        ShopifyApp.Bar.initialize({ title: 'Settings' });
    }

    render() {
        utils.render({
            template: 'settings',
            route: 'settings'
        });
    }
}
