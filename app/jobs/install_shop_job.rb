class InstallShopJob < ActiveJob::Base

  queue_as :install_shop

  def perform(params)  	
    InstallShop.new(params[:shop_domain]).install
  end

end
