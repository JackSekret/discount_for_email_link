module ShopifyWebhooks
	class AppUninstalledJob < ActiveJob::Base
	  queue_as :default

	  def perform(args)
	    shop = Shop.find_by_shopify_domain args[:shop_domain]
	    if shop
			  shop.installation_status = :deleted
			  shop.save
	    end
	  end

	end
end
