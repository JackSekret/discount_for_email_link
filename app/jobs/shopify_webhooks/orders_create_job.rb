module ShopifyWebhooks
	class OrdersCreateJob < ActiveJob::Base
	  queue_as :default

	  def perform(args)
	    shop = Shop.find_by_shopify_domain args[:shop_domain]
	    return unless shop
      webhook = args[:webhook]
      discount_code = webhook['discount_codes'].first
      # cart_token = webhook['cart_token']
      if discount_code #&& cart_token 
        statistics = shop.statistics.find_by(
          discount: discount_code['code'],
          # cart_token: params[:cart_token],
        )
        return unless statistics
        statistics.total = webhook['total_price']
        statistics.status = :created_order
        statistics.save
      end  
	  end
	end
end