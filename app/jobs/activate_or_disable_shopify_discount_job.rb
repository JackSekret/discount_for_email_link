class ActivateOrDisableShopifyDiscountJob < ActiveJob::Base

  queue_as :shopify_discount

  def perform(shop_id, discount_ids, is_active)
    shop = Shop.find_by_id shop_id
  	return unless shop
  	shop.activate_session
    method = is_active ? :activate : :disable
  	discount_ids.each do |discount_id|
      PriceRule.new(discount_id).send(method) if discount_id
    end
    ShopifyAPI::Base.clear_session
  end

end