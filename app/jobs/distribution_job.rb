class DistributionJob < ActiveJob::Base
	queue_as :distribution

	def perform(*args)
    options = args.extract_options!.symbolize_keys!
    if options.has_key?(:shop_id)
      shops = ::Shop.where(id: options[:shop_id])
    else
      shops = Shop.all
    end
    shops.each do |shop|
      unless shop.demo?
        if shop.charge_id
        recurring_charge = RecurringApplicationCharge.new(shop)
        recurring_charge.verify
        else
          shop.update(plan: 'free') unless shop.plan == 'free'
        end
      end
    end
  end
end
