class DestroyShopifyDiscountJob < ActiveJob::Base

  queue_as :shopify_discount

  def perform(shop_id, price_rule_ids)
  	shop = Shop.find_by_id shop_id
  	return unless shop
  	shop.activate_session
  	price_rule_ids.each do |price_rule_id|
      (PriceRule.new(nil).delete_shopify_price_rule(price_rule_id) rescue nil) if price_rule_id 
    end
    ShopifyAPI::Base.clear_session
  end

end