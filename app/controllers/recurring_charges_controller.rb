class RecurringChargesController < AuthenticatedController
  # skip_before_action :verify_terms_and_conditions
  layout 'distribution'

  def change_plan
    plan = params[:plan]
    recurring_application_charge = RecurringApplicationCharge.new(@current_shop, plan)
    @redirect_url = recurring_application_charge.charge
    redirect_to root_path unless @current_shop.present? && @redirect_url
  end

  def index
    RecurringApplicationCharge.new(@current_shop).activate!(params) if @current_shop.present?
    redirect_to root_path
  end

end
