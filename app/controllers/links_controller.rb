class LinksController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :verify_and_set_headers

  def theme
    return json_error('Token params not found') unless params[:token]
    @link = @shop.links.find_by(token: params[:token])
    return json_error('Link not found') unless @link && @link.status
    theme = @link.custom_theme
    return json_error('Theme not found') unless theme
    # return json_error('cart_token not found') unless params["\"cart_token\""]
    statistic = @link.statistics.find_or_initialize_by(
      discount: @link.discount["code"],
      # cart_token: params[:cart_token],
      shop: @shop
    )
    statistic.save
    @data = theme.data
    @njk = File.read("#{Rails.root}/app/views/themes/theme.njk")
    render json: {link: @link.to_json, data: @data, njk: @njk}, status: :ok
  end

  def verify_and_set_headers
    origin_domain = request.headers["HTTP_ORIGIN"].to_s.sub(/^https?:\/\//, '')
    @shop = Shop.search_by_domain origin_domain
    @shop ||= Shop.search_by_domain params[:shop_domain]
    return json_error('Shop not found') unless @shop
    headers['Access-Control-Allow-Origin'] = request.headers["HTTP_ORIGIN"]
    headers['Access-Control-Expose-Headers'] = 'ETag'
    headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD'
    headers['Access-Control-Allow-Headers'] = '*,x-requested-with,Content-Type,If-Modified-Since,If-None-Match,Auth-User-Token'
    headers['Access-Control-Max-Age'] = '86400'
    headers['Access-Control-Allow-Credentials'] = 'true'
  end

  def json_error(mess)
    render :json => {status: false, mess: mess}
  end
end
