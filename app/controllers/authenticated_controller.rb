class AuthenticatedController < ShopifyApp::AuthenticatedController
  # before_action :default_locale
  before_action :set_shop
  # before_action :verify_terms_and_conditions, unless: 'flash[:do_not_redirect]'

  private

  def set_shop
    @current_shop = Shop.find_by_shopify_domain @shop_session.url if @shop_session
  end

  # def verify_terms_and_conditions
  #   if current_shop.present? && !current_shop.terms_accepted?
  #     flash[:do_not_redirect] = true
  #     redirect_to accept_terms_and_conditions_path
  #   end
  # end

  # def accept_terms_and_conditions_path
  #   "/terms-and-conditions"
  # end

  # def default_locale
  #   I18n.locale = I18n.default_locale
  # end

  # def active_and_not_initiated_installation?
  #   value = Redis.sidekiq.get("#{current_shop.shopify_domain}/installing")
  #   value.present? && value != 'initiated'
  # end
end
