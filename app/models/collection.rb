# == Schema Information
# Schema version: 20180217112526
#
# Table name: collections
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  shop_id    :integer
#

class Collection < ApplicationRecord
  belongs_to :shop
  has_one :discounts
end
