# == Schema Information
# Schema version: 20180313185229
#
# Table name: discounts
#
#  id               :integer          not null, primary key
#  status           :boolean
#  shop_id          :integer          not null, indexed
#  title            :string           not null
#  code             :string           not null, indexed
#  discount_type    :integer          not null
#  value            :integer
#  applies_to       :integer
#  product_id       :integer          indexed
#  collection_id    :integer          indexed
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  price_rule_id    :integer
#  discount_code_id :integer
#  links            :string           default([]), is an Array
#

class Discount < ApplicationRecord
  belongs_to :shop
  belongs_to :product, required: false
  belongs_to :collection, required: false 

  enum discount_type: [:percentage, :amount, :free_shipping]
  enum applies_to: [:cart, :product, :collection]

  validates :title, :code, :discount_type, :links, presence: true
  validates :status, inclusion: { in: [ true, false ] }
  validates :title, uniqueness: { case_sensitive: false }
  validates :applies_to, presence: true, if: ->(discount){!discount.discount_type.eql?(:free_shipping)
  discount.discount_type.eql?(:free_shipping)}
  validates :value, presence: true, if: ->(discount){discount.percentage? || discount.amount?}
  validates :product, presence: true, if: ->(discount){discount.applies_to && discount.applies_to.eql?(:product)}
  validates :collection, presence: true, if: ->(discount){discount.applies_to && discount.applies_to.eql?(:collection)}

  def find_collection(params)
    if params.present?
      collection = self.shop.collections.find_or_initialize_by(id: params[:id])
      collection.title = params[:title]
      collection.image = params[:image]
      self.collection = collection
    else
      self.collection = nil
    end
  end
  
  def find_product(params)
    if params.present?
      product = self.shop.products.find_or_initialize_by(id: params[:id])
      product.title = params[:title]
      product.image = params[:image]
      self.product = find_variants(product, params[:variants])
    else
      self.product = nil
    end
  end

  def find_variants(product, params)
    product.variants.each do |variant|
      variant.discounts = variant.discounts.select{|i| i != self.id}
    end
    product.variants
    params.each do |shopify_variant|
      variant = product.variants.find_or_initialize_by(id: shopify_variant[:id])
      variant.title = shopify_variant[:title]
      variant.image = shopify_variant[:image]
      variant.discounts.push self.title
      product.variants << variant
    end
    product
  end

  def create_or_update_job
    CreateOrUpdateJob.perform_later(self.id)
  end

end
