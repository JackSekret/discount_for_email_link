# == Schema Information
# Schema version: 20180530185428
#
# Table name: statistics
#
#  id         :integer          not null, primary key
#  link_id    :integer
#  shop_id    :integer
#  status     :integer          default("view")
#  discount   :string
#  cart_token :string
#  total      :decimal(, )      default(0.0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Statistic < ApplicationRecord
  belongs_to :shop
	belongs_to :link

  enum status: [:view, :created_order]
  
end
