# == Schema Information
# Schema version: 20180530185428
#
# Table name: shops
#
#  id                  :integer          not null, primary key
#  shopify_domain      :string           not null, indexed
#  shopify_token       :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  money_format        :string
#  currency            :string
#  domain              :string
#  installation_status :integer          default("pending_installation")
#  demo                :boolean          default(FALSE)
#  recurring_status    :integer          default("pending")
#  charge_id           :string
#  trial_period_end_at :datetime
#  plan                :integer          default("basic")
#

class Shop < ActiveRecord::Base
  include ShopifyApp::SessionStorage
  # include ShopifyAssetable

  has_many :discounts, dependent: :destroy
  has_many :links, dependent: :destroy
  has_many :products, dependent: :destroy
  has_many :collections, dependent: :destroy
  has_many :custom_themes
  has_many :statistics

  enum installation_status: [:pending_installation, :installing, :installed, :failed, :deleted]
  enum plan: [:free, :basic, :standard]


  def self.search_by_domain domain
    self.find_by('shopify_domain = ? OR domain = ?', domain, domain)
  end

  def activate_session
    @activate_session ||= ShopifyAPI::Base.activate_session(self.session)
  end

  def session
    @session ||= ShopifyAPI::Session.new(shopify_domain, shopify_token)
  end

  def sync_shop_params
    activate_session
    self.update_column(:installation_status, Shop.installation_statuses[:installing])
    InstallShop.new(self.id).update_shop_params
    ShopifyAPI::Base.clear_session
  end
end
