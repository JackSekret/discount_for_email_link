# == Schema Information
# Schema version: 20180512085833
#
# Table name: links
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  link_type  :integer          default("homepage"), not null
#  token      :string           indexed
#  link_to    :string
#  discount   :json
#  collection :json
#  product    :json
#  variant    :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  shop_id    :integer
#  status     :boolean          default(TRUE)
#  url        :string
#

class Link < ApplicationRecord
  belongs_to :shop
  has_one :custom_theme, dependent: :destroy
  has_many :statistics

	enum link_type: [:homepage, :collection, :product, :variant, :cart, :checkout]

  validates :status, inclusion: { in: [ true, false ] }
  validates :title, uniqueness: { case_sensitive: false }
  validates :title, :link_type, :token, :link_to, :discount, presence: true
  validates :collection, presence: true, if: ->(link){link.link_type && link.link_type.eql?(:collection)}
  validates :product, presence: true, if: ->(link){link.link_type && link.link_type.eql?(:product)}
  validates :variant, presence: true, if: ->(link){link.link_type && [:variant, :cart, :checkout].include?(link.link_type)}
  validates :url, presence: :true

  before_create :add_theme

  def change_theme(params)
    if params[:type].eql? 'global'
      self.custom_theme.update(data: Theme.find(params[:theme_id]).data)
    else
      self.custom_theme.update(data: self.shop.custom_themes.find(params[:theme_id]).data)
    end
    self.custom_theme
  end

  private
  def add_theme
    themes = shop.custom_themes
    default_theme = themes.present? ? themes.last : Theme.first
  	theme = CustomTheme.new
  	theme.title = self.title
  	theme.data = default_theme.data
  	theme.shop = self.shop
    self.custom_theme = theme
  end
end
