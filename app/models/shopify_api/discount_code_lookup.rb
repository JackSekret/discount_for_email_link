module ShopifyAPI
  class DiscountCodeLookup < ShopifyAPI::DiscountCode
    
    def self.lookup(code)
      side = self.site
      if side
        uri = URI.parse("https://#{site.host}/admin/discount_codes/lookup")
        args = {code:code}
        uri.query = URI.encode_www_form(args)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        request = Net::HTTP::Get.new(uri.request_uri, self.headers.merge({'Accept'=>'application/json','Content-Type'=>'application/json'}))
        response = http.request(request)
        response.code == '303' ? response.body : nil
      else
        nil
      end
    end
  end
end