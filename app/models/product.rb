# == Schema Information
# Schema version: 20180217112526
#
# Table name: products
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  shop_id    :integer
#

class Product < ApplicationRecord
  belongs_to :shop
  has_one :discounts
  has_many :variants
end
