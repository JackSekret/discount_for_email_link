module ShopifyAssetable
  # extend ActiveSupport::Concern
  # def install_themes tasks=[]
  #   if tasks.blank?
  #     tasks = [:snippet_theme, :css, :js, :insert_snippets]
  #   end
  #   theme_roles = %w{main mobile}
  #   activated_themes = ShopifyRequestWrapper.call do
  #     ShopifyAPI::Theme.all.select { |t| theme_roles.include?(t.role) }
  #   end
  #   if activated_themes.present?
  #     activated_themes.each do |current_theme|
  #       add_assets_to_theme(current_theme, tasks)
  #     end
  #   end
  # end
  # def add_assets_to_theme(current_theme, tasks)
  #   if current_theme.present?
  #     # snippet_theme
  #     if tasks.include?(:snippet_theme)
  #       file_names = Dir[ 'app/views/themes/*.liquid' ].select{ |f| File.file? f }.map{ |f| File.basename f }
  #       file_names.each do |theme_name|
  #         snippet_theme = ShopifyRequestWrapper.call do
  #           ShopifyAPI::Asset.find("snippets/#{theme_name}", params: {theme_id: current_theme.id}) rescue nil
  #         end
  #         snippet_theme = ShopifyAPI::Asset.new(theme_id: current_theme.id) if snippet_theme.nil?
  #         snippet_theme.key = "snippets/#{theme_name}"
  #         file = File.open(Rails.root.join("app/views/themes/#{theme_name}"))
  #         snippet_theme.value = file.read.to_s
  #         ShopifyRequestWrapper.call do
  #           snippet_theme.save
  #         end
  #       end
  #     end   
  #   end
  # end
  # def remove_assets_from_theme(current_theme)
  #   if current_theme.present?
  #     # snippet_theme
  #     file_names = Dir[ 'app/views/themes/*.liquid' ].select{ |f| File.file? f }.map{ |f| File.basename f }
  #     file_names.each do |theme_name|
  #       snippet_theme = ShopifyRequestWrapper.call do
  #         ShopifyAPI::Asset.find("snippets/#{theme_name}", params: {theme_id: current_theme.id}) rescue nil
  #       end
  #       unless snippet_theme.nil?
  #         ShopifyRequestWrapper.call do
  #           snippet_theme.destroy
  #         end
  #       end
  #     end  
      
  #   end
  # end
 
  # def update_hash_triggers_metafield
  #   self.update_column(:last_triggers_updated, Time.now)
  #   value =  self.status ? self.triggers.hash_triggers(self).to_json.to_s : {status:false}
  #   metafield = ShopifyAPI::Metafield.new(
  #     namespace: 'cc_triggers_hash',
  #     key: 'triggers_hash',
  #     value_type: 'string',
  #     value: value
  #   )
  #   ShopifyRequestWrapper.call do
  #     ShopifyAPI::Shop.current.add_metafield(metafield)
  #   end
  # end
end