# == Schema Information
# Schema version: 20180326175944
#
# Table name: custom_themes
#
#  id         :integer          not null, primary key
#  shop_id    :integer
#  title      :string           not null
#  data       :json             not null
#  status     :integer          default(0)
#  link_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CustomTheme < ApplicationRecord
  belongs_to :shop
  belongs_to :link
end
