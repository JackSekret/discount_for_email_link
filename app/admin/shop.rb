ActiveAdmin.register Shop do
  permit_params :installation_status, :demo
  actions :all, except: [:new]

  index do
    selectable_column
    id_column
    column :shopify_domain
    column :domain
    column 'Installation Status' do |shop|
      status = shop.installation_status
      case status
      when 'pending_installation'
        status_tag(status, :orange)
      when 'installed'
        status_tag(status, :green)
      else
        status_tag(status, :red)
      end
    end
    column :money_format
    column :currency
    column :created_at
    column :demo
    column :plan
    column :trial_period_end_at
    actions
  end

  filter :shopify_domain,
         as: :string
  filter :domain,
         as: :string
  filter :installation_status,
         as: :select,
         collection: Shop.installation_statuses.map { |v, k| [v.titleize, k] }
  filter :demo
  filter :trial_period_end_at
  filter :plan,
         as: :select,
         collection: Shop.plans.map { |v, k| [v.titleize, k] }
  filter :created_at

  form do |f|
    f.inputs do
      f.input :installation_status
      f.input :demo
    end
    f.actions
  end

end
