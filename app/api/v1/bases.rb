require "grape-swagger"
module V1
  class Bases < Grape::API
  	version 'v1'
  	 before do
		raise_error(406, 'Undefined shop') unless current_shop
	end
	  helpers do
	    def raise_error(code, message, args = {})
	      message = message.join(', ') if message.is_a? Array
	      error!({ error_code: code, error_message: message }.merge(args), code)
	    end

	    def current_shop
	      domain = request.headers['Shop-Domain'].presence || params[:shop_domain] || (request.headers['Referer'] && URI.parse(request.headers['Referer']).host)
	      @current_shop ||= Shop.find_by_shopify_domain(domain)
	    end
	  end
	  mount V1::Ping
	  mount V1::PriceRules
	  mount V1::Shops
	  mount V1::Discounts
	  mount V1::Links
	  mount V1::Themes
	  mount V1::Statistics
    mount V1::Images

  end
end
