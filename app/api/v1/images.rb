module V1
  class Images < Grape::API
    namespace :image do
      desc 'upload image'
      params do
        requires :image, type: String
        optional :filename, type: String
      end
      post :upload do
        Dir.mkdir("public/image/#{current_shop.id}") unless File.exists?("public/image/#{current_shop.id}")
        filename = params[:filename] ? params[:filename] : "#{Time.now.to_i}.png"
        file_path = "public/image/#{current_shop.id}/#{filename}"
        data = params[:image]
        image_data = Base64.decode64(data['data:image/png;base64,'.length .. -1])
        new_file=File.new(file_path, 'wb')
        new_file.write(image_data)
        present image_url: "https://#{Rails.application.secrets.application_url}/image/#{current_shop.id}/#{filename}"
      end
    end
  end
end
