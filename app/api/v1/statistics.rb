module V1
  class Statistics < Grape::API   

    namespace :statistics do
    	desc 'Get all Statistics for the shop'
	    get do
	      statistics = ShopStatistics.new(current_shop).statistics
        present :money_format, current_shop.money_format
        present :currency, current_shop.currency
	      present :statistics, statistics
	    end
    end
  end
end
