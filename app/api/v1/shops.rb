module V1
  class Shops < Grape::API

  	namespace :shops do
      post '/sync' do
       current_shop.sync_shop_params
       present Shop.find(current_shop.id), with: V1::Entities::Shop
      end

      # get '/install_themes' do
      #   current_shop.activate_session
      #   current_shop.install_themes
      #   ShopifyAPI::Base.clear_session
      #   present Shop.find(current_shop.id), with: V1::Entities::Shop
      # end
    end

    get :shop do    	
      present current_shop, with: V1::Entities::Shop
    end
  end
end
