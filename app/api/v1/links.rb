module V1
  class Links < Grape::API
    helpers do
      params :standard_link_params do
        requires :status, type: Grape::API::Boolean
        requires :title, type: String
        requires :link_type, type: String, values:  Link.link_types.keys
        optional :token, type: String
        requires :link_to, type: String
        requires :url, type: String
        requires :discount, type: Hash
        optional :collection, type: Hash
        optional :product, type: Hash
        optional :variant, type: Hash
      end
    end

    namespace :links do
    	desc 'Get all links for the shop'
	    get do
	      links = current_shop.links
        present :money_format, current_shop.money_format
        present :currency, current_shop.currency
	      present :links, links, with: V1::Entities::Link
	    end

	    desc 'Create link'
      params do
        use :standard_link_params
      end
      put do
        link = current_shop.links.new(params)
        if link.save
          present link, with: V1::Entities::Link
        else
        	raise_error(400, link.errors.messages)
        end
      end

      desc 'Destroy links'
      params do
        requires :ids, type: Array[Integer]
      end
      delete do
        links = current_shop.links.where(id: params[:ids]).try(:destroy_all)
        present current_shop.links, with: V1::Entities::Link
      end

      desc 'Activate/Deactivate'
      params do
        requires :ids, type: Array[Integer]
        requires :is_active, type: Boolean
      end
      post '/activity' do
        links = current_shop.links.where(id:params[:ids])
        links.update_all(status: params[:is_active]) if links.present?
        present current_shop.links, with: V1::Entities::Link
      end

      namespace '/:id' do
        desc 'Get link by id'
        params do
          requires :id, type: Integer
        end
        get do
          link = current_shop.links.find_by_id(params[:id])
          return raise_error(400, 'Link not found') unless link.present?
          present link, with: V1::Entities::Link
        end

        desc 'Update link'
        params do
          requires :id, type: Integer
          use :standard_link_params
        end
        patch do
          link = current_shop.links.find_by_id(params[:id])
          return raise_error(400, 'Link not found') unless link.present?
          if link.update(params)
            present link, with: V1::Entities::Link
          else
            raise_error(400, link.errors.messages)
          end
        end

        desc 'Destroy link'
        params do
          requires :id, type: Integer
        end
        delete do
          link = current_shop.links.find_by_id(params[:id]).try(:destroy)
          true
        end

        desc 'Activate/Deactivate'
        params do
          requires :id, type: Integer
          requires :is_active, type: Boolean
        end
        post '/activity' do
          link = current_shop.links.find_by_id(params[:id])
          return raise_error(400, 'Link not found') unless link.present?
          link.update_column(:status, params[:is_active])
          true
        end

        desc 'Get all themes for the shop'
        get '/themes' do
          themes = Theme.all
          custom_themes = current_shop.custom_themes.where.not(link_id: params[:id])
          present :themes, themes, with: V1::Entities::Theme
          present :custom_themes, custom_themes, with: V1::Entities::CustomTheme
        end

        namespace '/theme' do
          desc 'Theme'
          params do
            requires :id, type: Integer
          end
          get '/' do
            link = current_shop.links.find_by_id(params[:id])
            return raise_error(400, 'Link not found') unless link.present?
            present link, with: V1::Entities::Link
          end

          desc 'update theme'
          params do
            requires :id, type: Integer
            requires :data, type: Hash
          end

          patch do
            link = current_shop.links.find_by_id(params[:id])
            theme = link.custom_theme
            return raise_error(400, 'Link not found') unless link.present?
            if theme.update(data: params[:data])
              present link, with: V1::Entities::Link
            else
              raise_error(400, theme.errors.messages)
            end
          end

          desc 'change theme'
          params do
            requires :id, type: Integer
            requires :type, type: String, values: ['global', 'custom']
            requires :theme_id, type: Integer
          end
          post 'change' do
            link = current_shop.links.find_by_id(params[:id])
            return raise_error(400, 'Link not found') unless link.present?
            theme = link.change_theme(params)
            present link, with: V1::Entities::Link
          end
        end
      end
    end
  end
end
