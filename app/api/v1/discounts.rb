module V1
  class Discounts < Grape::API
    helpers do
      params :standard_discount_params do
        requires :status, type: Grape::API::Boolean
        requires :title, type: String
        requires :code, type: String
        requires :discount_type, type: String, values:  Discount.discount_types.keys
        optional :value, type: String
        optional :applies_to, type: String, values:  Discount.applies_tos.keys
        requires :links, type: Array[String]
        optional :product, type: Hash do
          requires :id, type: Integer
          requires :title, type: String
          optional :image, type: String
          group :variants, type:  Array[JSON] do
            requires :id, type: Integer
            requires :title, type: String
            optional :image, type: String
          end
        end
        optional :collection, type: Hash do
          requires :id, type: Integer
          requires :title, type: String
          optional :image, type: String
        end
      end
    end
    
    namespace :discounts do	
    	desc 'Get all discounts for the shop'    	
	    get do
	      discounts = current_shop.discounts.all
        present :money_format, current_shop.money_format
        present :currency, current_shop.currency
	      present :discounts, discounts, with: V1::Entities::Discount
	    end

	    desc 'Create discount'
      params do
        use :standard_discount_params
      end
      put do
        discount = current_shop.discounts.new(params.except(:product, :collection))
        unless params[:discount_type].eql?('free_shipping')
          discount.find_product(params[:product])
          discount.find_collection(params[:collection])
        end
        if discount.save
          CreateOrUpdateShopifyDiscountJob.perform_later(discount.id)
          present discount, with: V1::Entities::Discount
        else
        	raise_error(400, discount.errors.messages)
        end
      end

      desc 'Destroy discounts'
      params do
        requires :ids, type: Array[Integer]
      end
      delete do
        discounts = current_shop.discounts.where(id: params[:ids])
        return raise_error(400, 'Discounts not found') unless discounts.present?
        DestroyShopifyDiscountJob.perform_later(current_shop.id, discounts.map(&:price_rule_id))
        discounts.destroy_all
        present current_shop.discounts, with: V1::Entities::Discount
      end

      desc 'Activate/Deactivate'
      params do
        requires :ids, type: Array[Integer]
        requires :is_active, type: Boolean
      end
      post '/activity' do
        discounts = current_shop.discounts.where(id:params[:ids])
        return raise_error(400, 'Discounts not found') unless discounts.present?
        discounts.update_all(status: params[:is_active])
        ActivateOrDisableShopifyDiscountJob.perform_later(current_shop.id, discounts.map(&:id), params[:is_active])
        present current_shop.discounts, with: V1::Entities::Discount
      end

      namespace '/:id' do
        desc 'Get discount by id'
        params do
          requires :id, type: Integer
        end
        get do
          discount = current_shop.discounts.find_by_id(params[:id])
          return raise_error(400, 'Discount not found') unless discount.present? 
          present discount, with: V1::Entities::Discount
        end

        desc 'Update discount'
        params do
          requires :id, type: Integer
          use :standard_discount_params
        end
        patch do
          discount = current_shop.discounts.find_by_id(params[:id])
          return raise_error(400, 'Discount not found') unless discount.present? 
          discount.find_product(params[:product])
          discount.find_collection(params[:collection])
          if discount.update(params.except(:product, :collection))
            CreateOrUpdateShopifyDiscountJob.perform_later(discount.id)
            present discount, with: V1::Entities::Discount
          else
            raise_error(400, discount.errors.messages)
          end
        end

        desc 'Destroy discount'
        params do
          requires :id, type: Integer
        end
        delete do
          discount = current_shop.discounts.find_by_id(params[:id])
          return raise_error(400, 'Discount not found') unless discount.present?
          price_rule_id = discount.price_rule_id
          if discount.destroy
            DestroyShopifyDiscountJob.perform_later(current_shop.id, [price_rule_id])
            true
          else
            raise_error(400, discount.errors.messages)
          end
        end

        desc 'Activate/Deactivate'
        params do
          requires :id, type: Integer
          requires :is_active, type: Boolean
        end
        post '/activity' do
          discount = current_shop.discounts.find_by_id(params[:id])
          return raise_error(400, 'Discount not found') unless discount.present?
          ActivateOrDisableShopifyDiscountJob.perform_later(current_shop.id, [discount.id], params[:is_active])
          discount.update_column(:status, params[:is_active])
          true
        end
      end
    end    
  end
end
