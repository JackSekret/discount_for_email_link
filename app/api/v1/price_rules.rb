module V1
  class PriceRules < Grape::API
    desc 'Price Rules'
     params do
      optional :page, type: Integer
      optional :per_page, type: Integer
    end
    get :price_rules do
      shop = current_shop
      shop.activate_session
      page = params[:page] || 1
      per_page = params[:per_page] || 30
      price_rules = ShopifyRequestWrapper.call{ShopifyAPI::PriceRule.find(:all, params: { page: page, limit: per_page})} || {}
      ShopifyAPI::Base.clear_session
      price_rules
    end
  end
end