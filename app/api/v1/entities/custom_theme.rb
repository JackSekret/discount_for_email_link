module V1
  module Entities
    class CustomTheme < Grape::Entity
      expose :id
      expose :title
      expose :data
      expose :status
    end
  end
end