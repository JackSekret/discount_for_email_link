module V1
  module Entities
    class Link < Grape::Entity
      expose :id
      expose :status
      expose :title
      expose :link_type
      expose :token
      expose :link_to
      expose :url
      expose :discount
      expose :collection
      expose :product
      expose :variant
      expose :custom_theme, using:  V1::Entities::CustomTheme
      expose :url
    end
  end
end
