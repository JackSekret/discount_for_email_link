module V1
  module Entities
    class Shop < Grape::Entity
      expose :id
      expose :shopify_domain
      expose :domain
      expose :plan
      expose :money_format
      expose :currency
    end
  end
end
