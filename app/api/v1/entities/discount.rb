module V1
  module Entities
    class Discount < Grape::Entity
      expose :id
      expose :status
      expose :title
      expose :code
      expose :discount_type
      expose :value
      expose :applies_to
      expose :links
      expose :product, using:  V1::Entities::Product
      expose :collection, using:  V1::Entities::Collection
    end
  end
end