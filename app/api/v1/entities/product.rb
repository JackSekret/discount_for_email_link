module V1
  module Entities
    class Product < Grape::Entity
      expose :id
      expose :title
      expose :image
      expose :variants, using:  V1::Entities::Variant
    end
  end
end