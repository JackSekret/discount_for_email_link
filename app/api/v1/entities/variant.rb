module V1
  module Entities
    class Variant < Grape::Entity
      expose :id
      expose :title
      expose :image
      expose :discounts
    end
  end
end