module V1
  module Entities
    class Collection < Grape::Entity
      expose :id      
      expose :title
      expose :image      
    end
  end
end 