module V1
  module Entities
    class Theme < Grape::Entity
      expose :id
      expose :title
      expose :data
      expose :status
    end
  end
end