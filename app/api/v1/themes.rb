module V1
  class Themes < Grape::API

  	namespace :themes do
      desc 'Get all themes for the shop'     
      get '/' do
        themes = Theme.all
        custom_themes = current_shop.custom_themes
        present :themes, themes, with: V1::Entities::Theme
        present :custom_themes, custom_themes, with: V1::Entities::CustomTheme
      end

      desc 'Create theme'
      params do
        requires :title, type: String
        requires :data, type: Hash
      end

      put do
        theme = Theme.new(params)     
        if theme.save         
          present theme, with: V1::Entities::Theme
        else
          raise_error(400, theme.errors.messages)
        end
      end

      namespace '/:id' do
        desc 'Get theme by id'
        params do
          requires :id, type: Integer
        end
        get do
          theme = Theme.find(params[:id])
          present theme, with: V1::Entities::Theme
        end

        desc 'Update theme'
        params do
          requires :id, type: Integer
          requires :title, type: String
          requires :data, type: Hash
        end
        patch do
          theme = Theme.find(params[:id])
          if theme.update(params)           
            present theme, with: V1::Entities::Theme
          else
            raise_error(400, theme.errors.messages)
          end
        end

        desc 'Destroy theme'
        params do
          requires :id, type: Integer
        end
        delete do
          Theme.find(params[:id]).destroy
          true
        end

      end
    end

  end
end
