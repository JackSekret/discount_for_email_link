class API < Grape::API
  # prefix 'api'
  format :json
  default_format :json
  content_type   :json, 'application/json; charset=utf-8'
  
  mount V1::Bases

  add_swagger_documentation(
    api_version: "v1",
    hide_documentation_path: true,
    mount_path: "/swagger_doc",
    hide_format: true,       
  )
end