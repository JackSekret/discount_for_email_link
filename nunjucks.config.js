module.exports = function(env) {
    var currency = {
        USD: '$', // US Dollar
        EUR: '€', // Euro
        GBP: '£', // British Pound Sterling
        UAH: '₴', // Ukrainian Hryvnia
        CNY: '¥',
        JPY: '¥', // Japanese Yen
        RUB: '₽',
        CRC: '₡', // Costa Rican Colón
        ILS: '₪', // Israeli New Sheqel
        INR: '₹', // Indian Rupee
        FRF: '₣',
        KRW: '₩', // South Korean Won
        NGN: '₦', // Nigerian Naira
        PHP: '₱', // Philippine Peso
        PLN: 'zł', // Polish Zloty
        PYG: '₲', // Paraguayan Guarani
        THB: '฿', // Thai Baht
        VND: '₫' // Vietnamese Dong
    };
    env.addFilter('currency', function(input) {
        if (!input) return;
        return currency[input];
    });
    env.addFilter('money', function(input, format) {
        if (!input) return;

        return format.replace(/\{\{.+\}\}/, input);
    });
    env.addFilter('fixNaN', function(input) {
        return isNaN(input) ? 0 : input;
    });
    env.addFilter('formatText', function(input) {
        if (!input) return;
        return input.replace(/_/g, ' ');
    });
    env.addFilter('cutomTrim', function(input) {
        if (!input) return;
        return input.replace(/<br>/g, '');
    });
    env.addFilter('rmDefault', function(input) {
        if (!input) return;
        var filtered = input.split('-');
        if (filtered.slice(-1)[0].trim().indexOf('Default') + 1) return filtered[0];
        return input;
    });
    env.addFilter('rmVariant', function(input) {
        if (!input) return;
        var filtered = input.split('-');
        return filtered[0] === 'T' ? filtered[0] + ' - ' + filtered[1] :  filtered[0];
    });
    env.addFilter('addSpace', function(input) {
        if (input) {
            var filtered = input.split(/([a-zA-Z#_]+)/);
            return filtered[0] + ' ' + filtered[1];
        }
        return input;
    });
    env.addFilter('isArray', function(input) {
        if (!input) return;
        return Array.isArray(input);
    });
    env.addFilter('bytesToSize', function(input) {
        if (!input) return;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (input === 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(input) / Math.log(1024)));
        return Math.round(input / Math.pow(1024, i), 2) + ' ' + sizes[i];
    });
    env.addFilter('isObject', function(input) {
        if (!input) return;
        return !Array.isArray(input) && input instanceof Object;
    });
    env.addFilter('addDataToObject', function(input, key, value) {
        if (!input) return;
        input[key] = value;
    });
    env.addFilter('sortObjectToArray', function(input, prop) {
        if (!input) return;
        var arr = [];

        for (var key in input) {
            if (key === 'theme' || key === 'border' || key === 'close') continue;
            input[key].name = key;
            arr[+input[key][prop]] = input[key];
        }

        return arr;
    });
    env.addFilter('time', function(input, unit) {
        if (!input) return;
        var units = {
            'hours': 3600,
            'days': 86400,
            'minutes': 60
        };
        return +input / units[unit];
    });
    env.addFilter('reminderDelay', function(input, delay) {
        if (!input) return;
        var date = +new Date();
        delay = delay ? delay * 1000 : 0;
        return input.replace('{{ delay_time }}', formatAMPM(new Date(date + delay)));
    });
    env.addFilter('newLine', function(input) {
        if (!input) return;
        return input + '\n' + '\n';
    });
    env.addFilter('now', function(input) {
        if (!input) return;
        var date = new Date();
        if (input === 'time') return formatAMPM(date, 0);
    });
    env.addFilter('date', function(input) {
        if (!input) return;
        var date = new Date(input);
        var day = date.getDate();
        var momth = date.getMonth() + 1;
        var year = date.getFullYear();

        return year + '-' + momth + '-' + day;
    });
    env.addFilter('convertTimestamp', function(input, type) {
        if (!input) return;
        var sec = input / 1000;

        var date = {};
        date.day = Math.floor(sec / 60 / 60 / 24);
        sec -= date.day * 60 *60 * 24;

        date.hour = Math.floor(sec / 60 / 60);
        sec -= date.hour * 60 *60;

        date.min = Math.floor(sec / 60);
        sec -= date.min * 60;

        date.sec = Math.floor(sec);

        date.day = date.day < 10 ? '0' + date.day : date.day;
        date.hour = date.hour < 10 ? '0' + date.hour : date.hour;
        date.min = date.min < 10 ? '0' + date.min : date.min;
        date.sec = date.sec < 10 ? '0' + date.sec : date.sec;

        if (type) return date[type];

        return date.day + ':' + date.hour + ':' + date.min + ':' + date.sec;
    });
};
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
