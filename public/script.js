(function () {
    console.log('download');
    var utils = {
        cookie: {
            options: {
                expires: 86400,
                path: "/",
                encodeValue: !1
            },
            get: function(e) {
                var t = document.cookie.match(new RegExp("(?:^|; )" + e.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"));
                return t ? decodeURIComponent(t[1]) : void 0
            },
            set: function(e, t, n) {
                var s = (n = n || {}).expires;
                if ("number" == typeof s && s) {
                    var a = new Date;
                    a.setTime(a.getTime() + 1e3 * s), s = n.expires = a
                }
                s && s.toUTCString && (n.expires = s.toUTCString());
                var r = e + "=" + (t = encodeURIComponent(t));
                for (var o in n) {
                    r += "; " + o;
                    var i = n[o];
                    !0 !== i && (r += "=" + i)
                }
                document.cookie = r
            },
            remove: function(e) {
                utils.cookie.set(e, null, {
                    expires: -1
                })
            }
        },
        queryParams: function(e) {
            var t = {};
            return e.replace("?", "").split("&").forEach(function(e) {
                var n = e.split("=")[1],
                    s = n ? e.split("=")[0] : "token";
                n = e.split("=")[0], t[s] = decodeURIComponent(n)
            }), t
        },
        updateQueryParams: function(e, t, n) {
            var s = new RegExp("([?&])" + t + "=.*?(&|$)", "i"),
                a = -1 !== e.indexOf("?") ? "&" : "?";
            return e.match(s) ? e.replace(s, "$1" + t + "=" + n + "$2") : e + a + t + "=" + n
        },
        removeQueryParams: function(str, sep1, match, sep2) {
            if ((sep1 === '?' || sep1 === '&') && !sep2) str = str.replace(sep1, '');
            return str.replace(match, '');
        },
        template: function(template, data) {
            function Template(template, data) {
                this.pattern = /\{\{\s*.+\s*\}\}/g;
                this.variable = /^\{\{\s*|\s*\}\}$/g;
                this.parts = template.split(this.pattern);
                this.matches = template.match(this.pattern);
                this.stream = this.parts[0];
                this.template = this.parse(data);
                this.render();
            }

            Template.prototype = {
                render: function () {
                    document.body.insertAdjacentHTML('afterbegin', this.template);
                },
                parse: function (obj) {
                    if (!this.matches) return;

                    for (var i = 0; i < this.matches.length; i++) {
                        var match = this.matches[i];
                        var isIf = / if /.test(match);
                        var isPipe = / \| /.test(match);
                        var str = '';

                        if (isIf) {
                            var operands = match.replace(this.variable, '').split('if');
                            var value = operands[0].replace(/'/g, '');
                            var expr = operands[1].split(/!?=+/);
                            if (!expr[1]) expr[1] = '';
                            var el = expr[0].trim().replace(/data\./, '');
                            var verified = /'/.test(expr[1]) ? expr[1].trim().replace(/'/g, '') : expr[1].trim() === 'true';
                            var compared = this.deeps(obj, el) === verified;
                            if (/data/.test(value)) value = this.deeps(obj, value.replace(/data\./, '').trim());
                            str += compared ? value : '';
                        } else {
                            if (isPipe) match = match.replace(/ \| \w+/, '');
                            var el = match.replace(this.variable, '').replace(/data\./, '');
                            str += this.deeps(obj, el);
                        }

                        this.stream += str + this.parts[i + 1];
                    }

                    return this.stream;
                },
                deeps: function (obj, val) {
                    var hs = val.split('.');
                    var deep;

                    for (var i = 0; i < hs.length; i++) {
                        deep = deep ? deep[hs[i]] : obj[hs[i]];
                    }

                    return deep;
                }
            };
            new Template(template, data);
        },
        xhr: function(e, t, n, s, a) {
            var r = new XMLHttpRequest;
            a = a || "application/x-www-form-urlencoded", r.open(e, t, !0), r.setRequestHeader("Content-Type", a), r.onload = function() {
                var e = r.response && JSON.parse(r.response);
                200 === r.status ? n(e) : console.error(e)
            }, r.onerror = function() {
                new Error("Network Error")
            }, r.send(s)
        },
        countdown: {
            interval: null,
            init: function(data) {
                this.data = data;
                this.interval = setInterval(function() {
                    this.prepare();
                    this.calc();
                    this.render();
                    this.clear();
                }.bind(this), 1000);
            },
            prepare: function() {
                if (this.data.type === 'cc') {
                    var loaded = utils.cookie.get('sdl_loaded_at');
                    this.timestamp = +loaded + this.data.countdown;
                } else if (this.data.type === 'to') {
                    this.timestamp = this.data.timestamp;
                }
            },
            calc: function() {
                var diff = (this.timestamp - +new Date()) / 1000;
                this.date = {};
                this.date.day = Math.floor(diff / 60 / 60 / 24);
                diff -= this.date.day * 60 *60 * 24;
                this.date.hour = Math.floor(diff / 60 / 60);
                diff -= this.date.hour * 60 *60;
                this.date.min = Math.floor(diff / 60);
                diff -= this.date.min * 60;
                this.date.sec = Math.floor(diff);

                this.timeout = this.date.day === 0 && this.date.hour === 0 && this.date.min === 0 && this.date.sec === 0;
            },
            render: function() {
                Object.keys(this.date).forEach(function(key) {
                    if (this.date[key] < 10) this.date[key] = '0' + this.date[key];
                }.bind(this));

                document.querySelector('[data-timer]').innerHTML =
                    '<span class="sdl-timer__items ' + (this.data.states.day ? '' : 'hidden') + '">' +
                        '<span>' + this.date.day + '</span>' +
                        '<span class="sdl-theme__separator">:</span>' +
                    '</span>' +
                    '<span class="sdl-timer__item ' + (this.data.states.hour ? '' : 'hidden') + '">' +
                        '<span>' + this.date.hour + '</span>' +
                        '<span class="sdl-theme__separator">:</span>' +
                    '</span>' +
                    '<span class="sdl-timer__item">' +
                        '<span>' + this.date.min + '</span>' +
                        '<span class="sdl-theme__separator">:</span>' +
                    '</span>' +
                    '<span class="sdl-timer__item">' + this.date.sec + '</span>';
            },
            clear: function() {
                if (!this.timeout) return;

                clearInterval(this.interval);
                utils.cookie.remove('discount_code');
                utils.cookie.remove('sdl_token');
                utils.cookie.remove('sdl_loaded_at');
                localStorage.removeItem('sdl_theme_template');
                localStorage.removeItem('sdl_theme_data');
                localStorage.removeItem('sdl_link_data');

                var popup = document.querySelector('[data-sdl-popup]');
                popup.parentNode.removeChild(popup);
            }
        }
    };

    function App() { this.init() }

    App.prototype = {
        init: function() {
            this.checkLocalStorage();
            this.getToken() ? this.getData() : (this.requireFontLoader(), this.getCache());
        },
        getToken: function() {
            var params = utils.queryParams(location.search);
            return this.token = params.token;
        },
        getData: function() {
            var vpn = /sdl42/.test(location.host) ? '01' : '2';
            this.url = 'https://user' + vpn + '.mocstage.com/links/' + this.token + '/theme';

            utils.xhr('GET', '/cart.js', function(res) {
                var data = new FormData();
                data.append('cart_token', res.token);

                utils.xhr('POST', this.url, this.callback.bind(this), data);
            }.bind(this));
        },
        callback: function(res) {
            if (res.mess && !res.status) return;

            this.prepareData(res);
            this.saveData();
            this.redirect();
        },
        getCache: function() {
            if (!this.isLocalStorage) return this.getDataWoCart();

            this.template = localStorage.getItem('sdl_theme_template');
            this.data = JSON.parse(localStorage.getItem('sdl_theme_data'));
            this.link = JSON.parse(localStorage.getItem('sdl_link_data'));

            this.render();
        },
        prepareData: function(res) {
            this.template = '<style>' + res.njk.split(/<style>/)[1];
            this.link = JSON.parse(res.link);
            this.data = res.data;
            this.data.code = this.link.discount.code;
        },
        saveData: function() {
            utils.cookie.set('sdl_token', this.link.token);
            utils.cookie.set('sdl_loaded_at', +new Date());

            if (!this.isLocalStorage) return this.redirect(this.link.url);

            localStorage.setItem('sdl_theme_template', this.template);
            localStorage.setItem('sdl_theme_data', JSON.stringify(this.data));
            localStorage.setItem('sdl_link_data', JSON.stringify(this.link));
        },
        render: function() {
            utils.template(this.template, this.data);
            this.form = document.querySelector('form[action*="cart"]');
            this.popup = document.querySelector('[data-sdl-popup]');
            this.close = this.popup.querySelector('[data-close]');
            this.data.timer.state && utils.countdown.init(this.data.timer);
            this.listenEvents();
        },
        redirect: function() {
            location.replace(this.link.url);
        },
        removeDiscounts: function(e) {
            var inputs = this.form.querySelectorAll('input[name="discount"]');

            Array.prototype.forEach.call(inputs, function(input) {
                input.parentNode.removeChild(input);
            });

            this.form.action = this.form.action
                .replace(location.origin, '')
                .replace(/(\?|&)(discount=[^\&]+(&)?)/, utils.removeQueryParams);
        },
        removePopup: function() {
            this.popup.parentNode.removeChild(this.popup);
            this.removeData();
            clearInterval(utils.countdown.interval);
        },
        removeData: function() {
            clearInterval(utils.countdown.interval);
            utils.cookie.remove('discount_code');
            utils.cookie.remove('sdl_token');
            utils.cookie.remove('sdl_loaded_at');

            if (!this.isLocalStorage) return;

            localStorage.removeItem('sdl_theme_template');
            localStorage.removeItem('sdl_theme_data');
            localStorage.removeItem('sdl_link_data');
        },
        requireFontLoader: function() {
            var script = document.createElement('script');
            script.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
            script.async = true;
            document.head.appendChild(script);

            var interval = setInterval(function() {
                try {
                    this.getFonts();
                    clearInterval(interval);
                } catch (e) {}
            }.bind(this));
        },
        getFonts: function() {
            WebFont.load({
                google: { families: this.data.fonts },
                classes: false
            });
        },
        checkLocalStorage: function() {
            try {
                localStorage.setItem('__test_ls', 1);
                localStorage.removeItem('__test_ls');
                this.isLocalStorage = true;
            } catch (e) { this.isLocalStorage = false }
        },
        getDataWoCart: function() {
            var vpn = /sdl42/.test(location.host) ? '01' : '2';
            this.url = 'https://user' + vpn + '.mocstage.com/links/' + utils.cookie.get('sdl_token') + '/theme';

            utils.xhr('POST', this.url, this.fallback.bind(this));
        },
        fallback: function(res) {
            if (res.mess && !res.status) return;

            this.prepareData(res);
            this.render();
        },
        listenEvents: function() {
            this.close.addEventListener('click', this.removePopup.bind(this));
            if (location.pathname === '/cart') {
                this.form.addEventListener('submit', this.removeData.bind(this));
                this.form.addEventListener('submit', this.removeDiscounts.bind(this));
            }
        }
    };

    new App();
})();
