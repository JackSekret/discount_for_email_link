class RecurringApplicationCharge
  attr_accessor :shop, :plan

  def initialize(shop, plan = :free)
    plan = plan.to_s.to_sym
    @plan = PLANS.keys.include?(plan) ? plan : :basic
    @shop = shop
    self.shop.activate_session
  end

  def test_shop?
    !Rails.env.production? || ShopifyRequestWrapper.call{ShopifyAPI::Shop.current}.try(:plan_name).eql?('staff_business')
  end

  def activate!(params={})
    self.shop.activate_session
    r = ShopifyRequestWrapper.call{ShopifyAPI::RecurringApplicationCharge.find(params[:charge_id]) rescue nil}
    if r&.status == 'accepted'
      ShopifyRequestWrapper.call do
        r.activate
      end
      shop.charge_id = params[:charge_id]
      shop.plan = r.name
      shop.save
    else
      shop.charge_id = nil
      shop.plan = :free
      shop.save
    end
  end

  def charge

    return cancel_recurring_charge if plan == :free
    recurring_charge_params = required_charge_params
    current_recurring_charge = create_recurring_charge(recurring_charge_params)
    current_recurring_charge.try(:confirmation_url)
  end

  def required_charge_params
    charge_activate_url = Rails.application.routes.url_helpers.recurring_charges_url(host: Rails.application.secrets.application_url, protocol: 'https')
    {
      name: plan,
      price: PLANS[plan],
      test: test_shop?,
      return_url: charge_activate_url,
      trial_days: 0,
    }
  end

  def create_recurring_charge(charge_params)
    ShopifyRequestWrapper.call { ShopifyAPI::RecurringApplicationCharge.create(charge_params) }
  end

  def cancel_recurring_charge
    ShopifyRequestWrapper.call { ShopifyAPI::RecurringApplicationCharge.all.select{|r| r.status == 'active'}.first&.cancel }
  end

end
