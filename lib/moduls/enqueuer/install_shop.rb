# frozen_string_literal: true

module Enqueuer
  class InstallShop
    attr_accessor :shop_id

    def initialize(shop_id)
      self.shop_id = shop_id
    end

    def enqueue
      return if job_in_queue || job_scheduled || job_in_retry_set || job_working
      InstallShopJob.set(wait: 5.seconds).perform_later(shop_id)
    end

    private

    def job_in_queue
      Sidekiq::Queue.new('install_shop').detect do |j|
        same_shop_id?(j.item)
      end
    end

    def job_scheduled
      Sidekiq::ScheduledSet.new.detect do |j|
        install_shop?(j) && same_shop_id?(j.item)
      end
    end

    def job_in_retry_set
      Sidekiq::RetrySet.new.detect do |j|
        install_shop?(j) && same_shop_id?(j.item)
      end
    end

    def job_working
      Sidekiq::Workers.new.detect do |_, _, work|
        work['queue'] == 'install_shop' && same_shop_id?(work['payload'])
      end
    end

    def install_shop?(job)
      job.item['wrapped'] == 'InstallShopJob'
    end

    def same_shop_id?(item)
      item['args'].first['arguments'].first == shop_id
    end
  end
end
