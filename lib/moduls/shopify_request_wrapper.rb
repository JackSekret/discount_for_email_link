class ShopifyRequestWrapper

  def self.call
    if block_given?
      begin
        retries ||= 0
        yield
      rescue ActiveResource::ClientError => e
        if e.respond_to?(:response) && e.response.respond_to?(:code) &&  %w{429 430}.include?(e.response.code)
          sleep e.response['retry-after'].present? ? e.response['retry-after'].to_i : 2
          retry
        else
          nil
        end
      rescue EOFError
        sleep 4
        retry
      rescue ActiveResource::ServerError
        sleep 1
        retry if (retries += 1) < 3
      end
    else
      nil
    end
  end

end
