class InstallShop
  attr_accessor :shop

  def initialize(shopify_domain)
    @shop = Shop.find_by_shopify_domain shopify_domain
  end

  def install
    return if shop.nil?
    shop.update_column(:installation_status, Shop.installation_statuses[:installing])
    shop.activate_session    
    p 'Updating shop'
    update_shop_params   
    # shop.install_themes
    ShopifyAPI::Base.clear_session
    shop.update_column(:installation_status, Shop.installation_statuses[:installed])
  end 

  def update_shop_params   
    shopify_shop = ShopifyAPI::Shop.current rescue nil
    if shopify_shop
      shop.money_format = shopify_shop.money_format rescue nil
      shop.currency = shopify_shop.currency rescue nil
      shop.domain = shopify_shop.try(:domain)      
    end
    shop.save
  end 

end
