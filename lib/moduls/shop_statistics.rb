class ShopStatistics
  attr_accessor :shop

  def initialize(shop)
    @shop = shop
  end

  def statistics
    return {} if shop.nil?
    sql = <<-SQL
      SELECT links.id,
             links.title,
             links.updated_at,
             count(statistics.id) as views,
             sum(case when statistics.status = 1 then 1 else 0 end) as created_orders,
             sum(statistics.total) AS total
      FROM links
      JOIN statistics ON statistics.link_id=links.id
      WHERE links.shop_id = #{shop.id}
      GROUP BY links.title, links.updated_at, links.id
      ORDER BY links.updated_at DESC
    SQL
    ActiveRecord::Base.connection.execute(sql)
  end

end

# updated_at
      # ORDER BY links.id