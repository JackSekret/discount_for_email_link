class PriceRule 
  attr_accessor :discount

  def initialize(discount_id)
    @discount = Discount.find_by_id(discount_id)
  end

  def activate     
    pr = ShopifyRequestWrapper.call{ShopifyAPI::PriceRule.find(discount.price_rule_id)}
    pr.ends_at = nil
    ShopifyRequestWrapper.call{pr.save}
  end

  def disable
    pr = ShopifyRequestWrapper.call{ShopifyAPI::PriceRule.find(discount.price_rule_id)}
    pr.ends_at = pr.starts_at
    ShopifyRequestWrapper.call{pr.save}
  end

  def create_or_update_shopify_discount
    discount.shop.activate_session
    find_discount
    if discount.price_rule_id.present?
      price_rule = find_shopify_price_rule
      update_shopify_price_rule(price_rule)
    else
      price_rule = create_shopify_price_rule
    end
    if discount.discount_code_id.present?
      discount_code = find_shopify_discount_code
      update_shopify_discount_code(discount_code)
    else
      create_shopify_discount_code
    end
    ShopifyAPI::Base.clear_session
  end

  def find_discount    
    discount_str = ShopifyAPI::DiscountCodeLookup.lookup(discount.code)
    if discount_str
      price_rule_id = discount_str[/(?<=price_rules\/).+(?=\/discount_codes)/]
      discount_code_id = discount_str[/(?<=discount_codes\/).+(?=\")/]
      discount.update_columns(price_rule_id: price_rule_id, discount_code_id: discount_code_id)        
    end       
  end

  def find_shopify_price_rule
    ShopifyRequestWrapper.call{ShopifyAPI::PriceRule.find(discount.price_rule_id)}
  end

  def update_shopify_price_rule(price_rule)
    ShopifyRequestWrapper.call{price_rule.update_attributes(constructor.except(:starts_at))}
  end

  def create_shopify_price_rule
    price_rule = ShopifyRequestWrapper.call { ShopifyAPI::PriceRule.create(constructor) }
    discount.update_column(:price_rule_id, price_rule.id)
    price_rule
  end

  def delete_shopify_price_rule(price_rule_id)
    ShopifyRequestWrapper.call{ShopifyAPI::PriceRule.delete(price_rule_id)}
  end

  def find_shopify_discount_code
    ShopifyRequestWrapper.call do
      ShopifyAPI::DiscountCode.find discount.discount_code_id, params: { price_rule_id: discount.price_rule_id }
    end
  end

  def update_shopify_discount_code(shopify_discount)
    ShopifyRequestWrapper.call{shopify_discount.update_attributes(code: discount.code, price_rule_id: discount.price_rule_id)}
  end

  def create_shopify_discount_code
    shopify_discount = ShopifyAPI::DiscountCode.new
    shopify_discount.prefix_options[:price_rule_id] = discount.price_rule_id
    shopify_discount.code = discount.code
    ShopifyRequestWrapper.call{shopify_discount.save}
    discount.update_column(:discount_code_id, shopify_discount.id)
  end

  private

  def constructor
    {
      customer_selection: :all,
      target_selection: target_selection,
      title: discount.code,
      starts_at: Time.now.utc
    }.merge(
      send("constructor_#{discount.discount_type}")
    )
  end

  def constructor_percentage
    {
      value_type: :percentage,
      target_type: :line_item,
      allocation_method: :across,
      value: discount.value * -1
    }.merge(
      send("constructor_#{discount.applies_to}")
    )
  end

  def constructor_amount
    {
      value_type: :fixed_amount,
      target_type: :line_item,
      allocation_method: :each,
      value: discount.value * -1
    }.merge(
      send("constructor_#{discount.applies_to}")
    )
  end

  def constructor_free_shipping
    {
      value_type: :percentage,
      target_type: :shipping_line,
      allocation_method: :each,
      value: -100
    }
  end

  def target_selection
    ['product', 'collection'].include?(discount.applies_to) ? :entitled : :all
  end

  def constructor_cart
    {}
  end

  def constructor_collection
    {entitled_collection_ids: [discount.collection.id]}
  end

  def constructor_product
    {entitled_variant_ids: discount.product.variants.map(&:id)}
  end
end
