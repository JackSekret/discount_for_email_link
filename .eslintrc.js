module.exports = {
    root: true,
    parser: "babel-eslint",
    parserOptions: {
        ecmaVersion: 8,
        sourceType: "module"
    },
    env: {
        browser: true,
        es6: true,
    },
    rules: {
        indent: ["error", 4, {
            SwitchCase: 1,
            MemberExpression: 1,
            ArrayExpression: 1,
            ObjectExpression: 1
        }],
        "comma-dangle": ["error", {
            arrays: "never",
            objects: "never",
            imports: "never",
            exports: "never",
            functions: "ignore"
        }],
        "linebreak-style": ["error", "unix"],
        quotes: ["error", "single"],
        semi: ["error", "always"],
        "no-undef": "error",
        "no-unused-vars": [
            "error",
            {
                vars: "local"
            }
        ],
        "no-cond-assign": ["error", "always"],
        "eqeqeq": ["error", "smart"],
        "no-console": "off",
        "block-scoped-var": "error",
        "no-loop-func": "error",
        "no-self-compare": "error",
        "no-unneeded-ternary": "error",
        "no-use-before-define": "off",
        "no-undef": "off",
        "no-new": "off",
        'template-curly-spacing': 'off',
        'object-curly-spacing': ["error", 'always'],
        'object-property-newline': ['off'],
        'key-spacing': ["error", {
            "beforeColon": false
        }],
        "object-curly-newline": ["error", {
            ObjectExpression: {
                multiline: true,
                minProperties: 3
            },
            ObjectPattern: 'never',
            ImportDeclaration: 'never',
            ExportDeclaration: 'never',
        }],
        "no-debugger": process.env.NODE_ENV !== "development" ? 2 : 0
    }
};
