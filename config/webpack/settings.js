const path = require('path');
const utils = require('./utils');

const dist = 'assets';
const host = '0.0.0.0';
const port = 8080;
const served = 'https://localhost:8080';
const publicPath = `https://${host}:${port}/${dist}/`;
const manifest = utils.root('public/packs/manifest.json');
const loadersDir = path.join(__dirname, 'loaders');

module.exports = {
    dist,
    host,
    port,
    served,
    publicPath,
    manifest,
    loadersDir
};
