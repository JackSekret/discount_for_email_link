module.exports = {
    test: /\.js$/,
    include: /frontend/,
    exclude: /node_modules/,
    loader: 'babel-loader'
};
