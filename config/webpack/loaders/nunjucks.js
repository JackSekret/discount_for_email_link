module.exports = {
    test: /\.njk$/,
    loader: 'nunjucks-loader',
    options: {
        config: 'nunjucks.config.js',
        quiet: true
    }
};
