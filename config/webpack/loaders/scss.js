const ExtractTextPlugin = require('extract-text-webpack-plugin');
const DEV = process.env.NODE_ENV === 'development';

module.exports = {
    test: /\.(s?css)$/,
    use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
            {
                loader: 'css-loader',
                options: {
                    minimize: !DEV
                },
            },
            {
                loader: 'postcss-loader',
                options: {
                    sourceMap: true
                },
            },
            'resolve-url-loader',
            {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                },
            },
        ],
    }),
};
