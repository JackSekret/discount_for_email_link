module.exports = {
    test: /\.js$/,
    include: /app\/assets\/frontend\//,
    exclude: /node_modules/,
    loader: 'eslint-loader',
    enforce: 'pre',
    options: {
        formatter: require('eslint-friendly-formatter')
    }
};
