module.exports = {
    test: /\.svg$/,
    use: [
        'svg-sprite-loader',
        {
            loader: 'svgo-loader',
            options: {
                plugins: [{
                        removeViewBox: false
                    },
                    {
                        removeEmptyAttrs: false
                    },
                    {
                        removeDoctype: false
                    }, {
                        removeComments: false
                    }, {
                        cleanupNumericValues: {
                            floatPrecision: 2
                        }
                    }, {
                        convertColors: {
                            names2hex: false,
                            rgb2hex: false
                        }
                    }
                ]
            }
        }
    ]
};
