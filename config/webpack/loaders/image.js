module.exports = {
    test: /\.(gif|png|jpe?g)$/i,
    use: [
        {
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
            },
        },
        {
            loader: 'image-webpack-loader',
            options: {
                mozjpeg: {
                    progressive: true,
                    optimizationLevel: 4,
                    quality: 95,
                },
                pngquant: {
                    quality: '65-90',
                    speed: 4,
                },
            },
        },
    ],
};
