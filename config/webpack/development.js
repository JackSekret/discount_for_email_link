const utils             = require('./utils');
const settings          = require('./settings');
const commonConfig      = require('./common');
const webpack           = require('webpack');
const webpackMerge      = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin    = require('webpack-manifest-plugin');

module.exports = webpackMerge(commonConfig, {
    mode: 'development',

    output: {
        pathinfo: true,
        filename: '[name].js',
        publicPath: `${settings.publicPath}`,
        hotUpdateChunkFilename: 'hot/[id].[hash].hot-update.js',
        hotUpdateMainFilename: 'hot/[hash].hot-update.json'
    },

    devServer: {
        host: settings.host,
        port: settings.port,
        https: true,
        compress: true,
        inline: true,
        // hot: true,
        disableHostCheck: true,
        historyApiFallback: true,
        stats: { colors: true },
        overlay: {
            warnings: false,
            errors: true
        },
        publicPath: settings.publicPath,
        public: settings.served,
        contentBase: utils.root('public/assets'),
        headers: { 'Access-Control-Allow-Origin': '*' }
    },

    stats: { errorDetails: true },

    watchOptions: { aggregateTimeout: 100 },

    devtool: 'cheap-module-source-map',

    plugins: [
        // new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin({ disable: true }),
        new ManifestPlugin({
            fileName: settings.manifest,
            publicPath: settings.publicPath,
            writeToFileEmit: true
        })
    ]
});
