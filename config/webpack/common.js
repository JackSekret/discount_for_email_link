const utils              = require('./utils');
const settings           = require('./settings');
const webpack            = require('webpack');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const { join }           = require('path');
const { sync }           = require('glob');
const ENV                = process.env.NODE_ENV;

module.exports = {
    entry: { app: './frontend/main' },

    resolve: {
        extensions: ['.js', '.css', '.scss', '.njk', '.svg'],
        modules: [
            utils.root('frontend'),
            utils.root('frontend/components'),
            utils.root('app/views/themes'),
            'node_modules'
        ],

        alias: {
            vendor: utils.root('frontend/vendor/index'),
            components: utils.root('frontend/components/index'),
            utils: utils.root('frontend/utils/index'),
            config: utils.root('frontend/config/index'),
            templates: utils.root('frontend/components'),
            theme: utils.root('frontend/components/themes/theme/'),
            link: utils.root('frontend/components/links/link/edit/'),
            themes: utils.root('app/views/themes')
        }
    },

    module: { rules: sync(join(settings.loadersDir, '*.js')).map(loader => require(loader)) },

    optimization: {
        runtimeChunk: { name: 'common' },
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    name: 'common',
                    chunks: 'initial',
                    enforce: true
                }
            }
        }
    },

    plugins: [
        new SpriteLoaderPlugin(),
        new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(ENV) })
    ]
};
