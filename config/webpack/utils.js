const path = require('path');
const _root = path.resolve(__dirname, '../..');
// const notifier = require('node-notifier');
// const packageConfig = require(`${_root}/package.json`);
module.exports = {
    root(args) {
        args = Array.prototype.slice.call(arguments, 0);
        return path.join.apply(path, [_root].concat(args));
    }
    // error() {
    //     return (severity, errors) => {
    //         if (severity !== 'error') return;
    //         const error = errors[0];
    //         const filename = error.file && error.file.split('!').pop();
    //         notifier.notify({
    //             title: packageConfig.name,
    //             message: severity + ': ' + error.name,
    //             subtitle: filename || '',
    //             icon: path.join(__dirname, 'logo.png')
    //         });
    //     }
    // }
};
