const utils = require('./utils');
const commonConfig      = require('./common');
const settings          = require('./settings');
const webpackMerge      = require('webpack-merge');
const ManifestPlugin    = require('webpack-manifest-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = webpackMerge(commonConfig, {
    mode: 'production',

    output: {
        path: utils.root('public/assets'),
        filename: '[name].[hash].js'
    },

    plugins: [
        new ExtractTextPlugin({
            filename: '[name].[hash].css',
            allChunks: true
        }),
        new CompressionPlugin({
            asset: '[path].gz[query]',
            algorithm: 'gzip',
            test: /\.(js|css|html|svg)$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new ManifestPlugin({
            fileName: settings.manifest,
            publicPath: `/${settings.dist}/`,
            writeToFileEmit: true
        })
    ]
});
