Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root :to => 'home#index'
  mount ShopifyApp::Engine, at: '/'
  mount API => '/api'

  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
  username == Rails.application.secrets.sidekiq['login'] && password == Rails.application.secrets.sidekiq['password']
  end unless Rails.env.development?
  mount Sidekiq::Web => '/sidekiq'

  get '/:root_page', to: 'home#index', root_page: /(statistics)|(settings)|((discounts|links)(\/(new|\d+)?(\/((edit|theme)))?)?|themes(\/\d+)?)/

  resources :recurring_charges, only: [:index] do
    collection do
      get :change_plan
    end
  end

   post '/links/:token/theme', to: 'links#theme'

end
