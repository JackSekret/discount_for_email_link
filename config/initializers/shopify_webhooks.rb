WEBHOOKS = [  
  {
    topic: 'app/uninstalled',
    fields: %W(id),
    address: "https://#{Rails.application.secrets.application_url}/webhooks/app_uninstalled"
  },
  {
    topic: 'orders/create',
    fields: %W(id total_price discount_codes cart_token),
    address: "https://#{Rails.application.secrets.application_url}/webhooks/orders_create"
  }
]


ShopifyApp.configure do |config|
  config.webhook_jobs_namespace = 'shopify_webhooks'
  config.webhooks = WEBHOOKS
end

ShopifyApp.configure do |config|
  config.scripttags = [
    {event:'onload', src:"https://#{Rails.application.secrets.application_url}/script.js"}
    # {event:'onload', src: ->(domain) { dynamic_tag_url(domain) } }
  ]
end