require 'sidekiq'

APP_NAME = Rails.application.class.to_s.split("::").first.to_s.parameterize

Sidekiq.configure_server do |config|
  config.redis = { url: Rails.application.secrets.redis['url'], namespace: "#{APP_NAME}_#{Rails.env}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: Rails.application.secrets.redis['url'], namespace: "#{APP_NAME}_#{Rails.env}" }
end
# Sidekiq::Queue['shopify_webhooks'].limit = 10


# schedule_file = "config/schedule.yml"
#
# if File.exists?(schedule_file) && Sidekiq.server?
#   Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
# end
