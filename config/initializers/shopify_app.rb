permissions = [
  'read_themes',
  'write_themes',
  'read_products',
  'read_price_rules',
  'write_price_rules',
  'write_script_tags',
  'read_orders',
]

ShopifyApp.configure do |config|
  config.application_name = "Discount For Email Link"  
  config.api_key = Rails.application.secrets.shopify_app[:api_key]
  config.secret = Rails.application.secrets.shopify_app[:secret] 
  config.scope = permissions.join(',')
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
   config.after_authenticate_job = { job: InstallShopJob }
end
